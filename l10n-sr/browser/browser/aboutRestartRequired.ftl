# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Потребно рестартовање
restart-required-header = Жао нам је. Морамо још једну ствар да урадимо да бисте наставили.
