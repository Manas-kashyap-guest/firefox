# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Помери нагоре
    .accesskey = н
languages-customize-movedown =
    .label = Помери надоле
    .accesskey = л
languages-customize-remove =
    .label = Уклони
    .accesskey = к
languages-customize-select-language =
    .placeholder = Изабери језик за додавање…
languages-customize-add =
    .label = Додај
    .accesskey = ј
messenger-languages-window =
    .title = Подешавања језика { -brand-short-name }-а
    .style = width: 42em
messenger-languages-description = { -brand-short-name } ће приказати први језик као подразумевани и приказаће заменске језике ако је то потребно, по редоследу по којем се појављују.
