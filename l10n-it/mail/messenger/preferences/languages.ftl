# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Sposta sopra
    .accesskey = S
languages-customize-movedown =
    .label = Sposta sotto
    .accesskey = o
languages-customize-remove =
    .label = Elimina
    .accesskey = E
languages-customize-select-language =
    .placeholder = Seleziona una lingua da aggiungere ...
languages-customize-add =
    .label = Aggiungi
    .accesskey = A
messenger-languages-window =
    .title = Impostazioni della lingua per { -brand-short-name }
    .style = width: 42em
messenger-languages-description = { -brand-short-name } utilizzerà la prima lingua come predefinita, mentre le altre lingue verranno utilizzate, se necessario, nell’ordine in cui sono visualizzate.
