# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Liostaichean bacaidh
    .style = width: 55em
blocklist-desc = ’S urrainn dhut fhèin taghadh dè an liosta a chleachdadh { -brand-short-name } gus eileamaidean-lìn a bhacadh a thracaicheas na nì thu air loidhne ma dh’fhaoidte.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Liosta
blocklist-button-cancel =
    .label = Sguir dheth
    .accesskey = S
blocklist-button-ok =
    .label = Sàbhail na h-atharraichean
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Dìon bunasach le Disconnect.me (mholamaid seo).
blocklist-item-moz-std-desc = Bheir seo cead do chuid a thracairean ach an obraich làraichean-lìn mar bu chòir.
blocklist-item-moz-full-name = Dìon daingeann le Disconnect.me
blocklist-item-moz-full-desc = Bacaidh seo tracairean air a bheil daoine eòlach. Dh’fhaoidte nach obraich gach làrach-lìn mar bu chòir.
