# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Jami bulok
    .style = width: 55em
blocklist-desc = Itwero yero jami mene ma { -brand-short-name } bi tic kwede me gengo jami Kakube ma twero lubo kor tic me yeny mamegi.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Jami
blocklist-button-cancel =
    .label = Juki
    .accesskey = J
blocklist-button-ok =
    .label = Gwok alokaloka
    .accesskey = G
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Disconnect.me gwokke ma yot (Kicwako).
blocklist-item-moz-std-desc = Ye lulub kor mogo wek kakube otii maber.
blocklist-item-moz-full-name = Disconnect.me gwokke ma ger.
blocklist-item-moz-full-desc = Geng lulub kor ma ngene. Kakube mogo pe bi tic ma ber.
