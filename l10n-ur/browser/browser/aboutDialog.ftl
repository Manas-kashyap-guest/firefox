# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

aboutDialog-title =
    .title = { -brand-full-name } کے متعلق
releaseNotes-link = نیا کیا ہے
update-checkForUpdatesButton =
    .label = تازہ کاریوں کے لیے پڑتال کریں
    .accesskey = C
update-updateButton =
    .label = { -brand-shorter-name } کی تازہ کاری کرنے کے لیئے دوبارہ شروع کریں
    .accesskey = R
update-checkingForUpdates = تازہ کاریوں کے لیے پڑتال کر رہا ہے…
update-downloading = <img data-l10n-name="icon"/>تازہ کاری ڈاؤن لوڈ کر رہا ہے—<label data-l10n-name="download-status"/>
update-applying = تازہ کاری لگائیں ...
update-failed = تازہ کاری ناکام ہو گئی۔<label data-l10n-name="failed-link">تازہ ترین ورژن ڈاؤن لوڈ کریں</label>
update-failed-main = تازہ کاری ناکام ہو گئی۔<a data-l10n-name="failed-link-main">تازہ ترین ورژن ڈاؤن لوڈ کریں</a>
update-adminDisabled = تازہ کاریاں آپ کے نظام کے منتظم کی طرف سے نااہل شدہ ہیں
update-noUpdatesFound = { -brand-short-name } تازہ ترین ہے
update-otherInstanceHandlingUpdates = { -brand-short-name } ایک اور صورت سے تازہ ہو رہا ہے
update-manual = تازہ کاریاں یہاں دستیاب ہیں  <label data-l10n-name="manual-link"/>
update-unsupported = آپ اس نظام پر مزید تازہ کاریاں نہیں کر سکتے۔<label data-l10n-name="unsupported-link">مزید سیکھیں</label>
update-restarting = دوباره شروع ہو رہا ہے…
channel-description = آپ اس وقت ادھر ہیں<label data-l10n-name="current-channel"></label>چینل کی تازہ کاری کریں
warningDesc-version = { -brand-short-name } تجرباتی ہے اور غیر مستحکم ہو سکتا ہے۔
community-exp = <label data-l10n-name="community-exp-mozillaLink">{ -vendor-short-name }</label>ہے ایک<label data-l10n-name="community-exp-creditsLink">عالمی گروه</label>  آپس میں مل کر مواصلاتی جال کو وصیح، عوامی اور قابل رسائی بناۓ ہوۓ-
community-2 = { -brand-short-name } کے ڈیزائنر ہیں<label data-l10n-name="community-mozillaLink">{ -vendor-short-name }</label>، آ<label data-l10n-name="community-creditsLink">عالمہ گروہ</label>   آپس میں مل کر مواصلاتی جال کو وصیح، عوامی اور قابل رسائی بناۓ ہوۓ-
helpus = مدد کرنا چاہیں گے؟ <label data-l10n-name="helpus-donateLink">ڈونیشن دیں</label> یا <label data-l10n-name="helpus-getInvolvedLink">شامل ہوں!</label>
bottomLinks-license = لائسنس کرنے کی معلومات
bottomLinks-rights = صارف کے حقوق
bottomLinks-privacy = رازداری پالیسی
aboutDialog-architecture-sixtyFourBit = 64-بٹ
aboutDialog-architecture-thirtyTwoBit = 32-بٹ
