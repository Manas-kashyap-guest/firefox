<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "Origen de los datos de ping:">
<!ENTITY aboutTelemetry.showCurrentPingData "Datos acuales de ping">
<!ENTITY aboutTelemetry.showArchivedPingData "Datos archivados de ping">
<!ENTITY aboutTelemetry.showSubsessionData "Mostrar datos de subsesión">
<!ENTITY aboutTelemetry.choosePing "Elegir ping:">
<!ENTITY aboutTelemetry.archivePingType "
Tipo de ping
">
<!ENTITY aboutTelemetry.archivePingHeader "Ping">
<!ENTITY aboutTelemetry.optionGroupToday "
Hoy
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
Ayer
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Más antiguo
">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Datos de Telemetry">
<!ENTITY aboutTelemetry.moreInformations "¿Buscas más información?">
<!ENTITY aboutTelemetry.firefoxDataDoc "La <a>documentación de datos de Firefox</a> contiene guías sobre cómo trabajar con tus herramientas de datos.">
<!ENTITY aboutTelemetry.telemetryClientDoc "La <a>documentación de cliente de Firefox Telemetry</a> incluye definiciones para conceptos, documentación de API y referencias de datos.">
<!ENTITY aboutTelemetry.telemetryDashboard "Los <a>tableros de Telemetry</a> te permiten visualizar los datos que Mozilla recibe a través de Telemetry.">

<!ENTITY aboutTelemetry.telemetryProbeDictionary "El <a>diccionario de sondeos</a> proporciona detalles y descripciones de los sondeos recopilados por Telemetry.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "
Abrir en el visor de JSON
">

<!ENTITY aboutTelemetry.homeSection "Inicio">
<!ENTITY aboutTelemetry.generalDataSection "  Datos generales">
<!ENTITY aboutTelemetry.environmentDataSection "  Entorno de Datos">
<!ENTITY aboutTelemetry.sessionInfoSection "  Información de la sesión">
<!ENTITY aboutTelemetry.scalarsSection "
 Escalares">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  Escalares con llave">
<!ENTITY aboutTelemetry.histogramsSection "  Histogramas">
<!ENTITY aboutTelemetry.keyedHistogramsSection "  Histogramas claves">
<!ENTITY aboutTelemetry.eventsSection "
  
  Eventos">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "  Medidas simples">
<!ENTITY aboutTelemetry.slowSqlSection "  Sentencias SQL lentas">
<!ENTITY aboutTelemetry.addonDetailsSection "  Detalles del complemento">
<!ENTITY aboutTelemetry.capturedStacksSection "
  
  Pilas capturadas">
<!ENTITY aboutTelemetry.lateWritesSection "  Escrituras demoradas">
<!ENTITY aboutTelemetry.rawPayloadSection "
Contenido sin procesar
">
<!ENTITY aboutTelemetry.raw "JSON sin procesar">

<!ENTITY aboutTelemetry.fullSqlWarning "  NOTA: la depuración SQL lenta está activada. Pueden motrarse cadenas completas de SQL pero no se enviarán a Telemetry.">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Obtener nombres de función para pilas">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Mostrar datos de pila en bruto
">
