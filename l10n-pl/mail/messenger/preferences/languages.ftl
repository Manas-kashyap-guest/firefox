# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = W górę
    .accesskey = W

languages-customize-movedown =
    .label = W dół
    .accesskey = d

languages-customize-remove =
    .label = Usuń
    .accesskey = U

languages-customize-select-language =
    .placeholder = Wybierz język do dodania…

languages-customize-add =
    .label = Dodaj
    .accesskey = o

messenger-languages-window =
    .title = Ustawienia języka programu { -brand-short-name }
    .style = width: 40em

messenger-languages-description = { -brand-short-name } będzie wyświetlał pierwszy język jako domyślny, a języki zastępcze w razie potrzeby i w tej kolejności.
