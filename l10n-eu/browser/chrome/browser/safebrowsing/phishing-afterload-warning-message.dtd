<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Joan atzera">
<!ENTITY safeb.palm.seedetails.label "Ikusi xehetasunak">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Hau ez da gune iruzurtia…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "i">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Aholkularitza <a id='advisory_provider'/> zerbitzuak eskainia.">


<!ENTITY safeb.blocked.malwarePage.title2 "Webgune hau bisitatzeak zure ordenagailua kalte lezake">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName;(e)k orri hau blokeatu du zure ordenagailuko informazio pertsonala lapur edo ezaba lezakeen software maltzurra instalatzen saia litekeelako.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> gunea <a id='error_desc_link'>software maltzurra izateagatik salatuta dago</a>. <a id='report_detection'>Atzemate-arazo baten berri eman</a> edo <a id='ignore_warning_link'>arriskua ezikusi</a> eta gune ez-segurura joan zaitezke.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> gunea <a id='error_desc_link'>software maltzurra izateagatik salatuta dago</a>. <a id='report_detection'>Atzemate-arazo baten berri eman</a> dezakezu.">

<!ENTITY safeb.blocked.malwarePage.learnMore "<a id='learn_more_link'>StopBadware.org</a> gunean web eduki kaltegarriari buruzko argibide gehiago dituzu, hala nola birus eta malware-ari buruzko eta ordenagailua babesteko informazioa. <a id='firefox_support'>support.mozilla.org</a> gunean &brandShortName;(r)en phishing eta malware babesari buruzko argibide gehiago dituzu.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Hurrengo webguneak programa kaltegarriak izan litzake">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName;(e)k orri hau blokeatu du zure nabigatzaile-esperientzia kalte lezaketen programak instalatzera bultza baitiezazuke (adibidez, zure hasiera-orria aldatuz edo bisitatzen dituzun guneetan aparteko iragarkiak erakutsiz).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> gunea <a id='error_desc_link'>software kaltegarria izateagatik salatuta dago</a>. <a id='ignore_warning_link'>Arriskua ezikusi</a> eta gune ez-segurura joan zaitezke.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> gunea <a id='error_desc_link'>software kaltegarria izateagatik salatuta dago</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "<a id='learn_more_link'>Eskatu gabeko softwarearen politika</a>n kaltegarria den eta nahi ez den softwareari buruzko argibide gehiago dituzu. <a id='firefox_support'>support.mozilla.org</a> gunean &brandShortName;(r)en phishing eta malware babesari buruzko argibide gehiago dituzu.">


<!ENTITY safeb.blocked.phishingPage.title3 "Gune iruzurtia">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName;(e)k orri hau blokeatu du softwarea instalatzera edo pasahitz edo kreditu-txartelen tankerako informazio pertsonala argitara ematera bultza liezazukeelako.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> gunea <a id='error_desc_link'>iruzurgilea izateagatik salatuta dago</a>. <a id='report_detection'>Atzemate-arazo baten berri eman</a> edo <a id='ignore_warning_link'>arriskua ezikusi</a> eta gune ez-segurura joan zaitezke.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> gunea <a id='error_desc_link'>gune iruzurti gisa salatuta dago</a>. <a id='report_detection'>Atzemate-arazo baten berri eman</a> dezakezu.">

<!ENTITY safeb.blocked.phishingPage.learnMore "<a id='learn_more_link'>www.antiphishing.org</a> gunean phishing-a eta iruzurra egiten duten guneei buruzko argibide gehiago dituzu. <a id='firefox_support'>support.mozilla.org</a> gunean &brandShortName;(r)en phishing eta malware babesari buruzko argibide gehiago dituzu.">


<!ENTITY safeb.blocked.harmfulPage.title "Hurrengo webguneak malwarea izan lezake">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName;(e)k orri hau blokeatu du zure informazio pertsonala lapurtzen edo ezabatzen duten aplikazioak instalatzen saia daitekeelako (adibidez argazkiak, mezuak edo kreditu-txartelak).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> gunea <a id='error_desc_link'>balizko aplikazio kaltegarri bat izateagatik salatuta dago</a>. <a id='ignore_warning_link'>Arriskua ezikusi</a> eta gune ez-segurura joan zaitezke.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> gunea <a id='error_desc_link'>balizko aplikazio kaltegarri bat izateagatik salatuta dago</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "<a id='firefox_support'>support.mozilla.org</a> gunean &brandShortName;(r)en phishing eta malware babesari buruzko argibide gehiago dituzu.">
