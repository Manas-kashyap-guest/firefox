# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Strings used for device manager

devmgr-devlist =
    .label = Các Module và Thiết bị Bảo mật
devmgr-header-details =
    .label = Chi tiết
devmgr-header-value =
    .label = Giá trị
devmgr-button-login =
    .label = Đăng Nhập
    .accesskey = N
devmgr-button-logout =
    .label = Đăng Xuất
    .accesskey = X
devmgr-button-changepw =
    .label = Thay đổi Mật khẩu
    .accesskey = M
devmgr-button-load =
    .label = Nạp
    .accesskey = p
devmgr-button-unload =
    .label = Không Nạp
    .accesskey = K
devmgr-button-enable-fips =
    .label = Bật FIPS
    .accesskey = F
devmgr-button-disable-fips =
    .label = Tắt FIPS
    .accesskey = F

## Strings used for load device

load-device-info = Nhập thông tin cho module bạn muốn thêm.
load-device-modname-default =
    .value = Module PKCS#11 Mới
load-device-browse =
    .label = Duyệt…
    .accesskey = B

## Token Manager

devinfo-status =
    .label = Trạng thái
devinfo-status-disabled =
    .label = Bị Vô Hiệu
devinfo-status-not-present =
    .label = Không Có
devinfo-status-uninitialized =
    .label = Chưa được Nhận diện
devinfo-status-not-logged-in =
    .label = Chưa Đăng Nhập
devinfo-status-logged-in =
    .label = Đã Đăng Nhập
devinfo-status-ready =
    .label = Sẵn sàng
devinfo-desc =
    .label = Mô tả
devinfo-man-id =
    .label = Nhà Sản Xuất
devinfo-hwversion =
    .label = Phiên bản HW
devinfo-fwversion =
    .label = Phiên bản FW
devinfo-modname =
    .label = Mô-đun
devinfo-modpath =
    .label = Đường dẫn
login-failed = Đăng nhập Thất bại
devinfo-label =
    .label = Nhãn
devinfo-serialnum =
    .label = Số Serial
fips-nonempty-password-required = Chế độ FIPS yêu cầu bạn đặt một Mật khẩu Chính cho mỗi thiết bị bảo mật. Vui lòng đặt mật khẩu trước khi bật chế độ FIPS.
unable-to-toggle-fips = Không thể thay đổi chế độ FIPS cho thiết bị bảo mật. Bạn nên thoát và khởi động lại ứng dụng này.
add-module-failure = Không thể thêm module
del-module-warning = Bạn có chắc muốn xóa module bảo mật này không?
del-module-error = Không thể xóa module
