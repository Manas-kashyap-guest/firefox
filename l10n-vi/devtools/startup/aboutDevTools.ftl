# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

head-title = GIới thiệu công cụ nhà phát triển
enable-title = Kích hoạt công cụ phát triển Firefox
enable-learn-more-link = Xem thêm về công cụ nhà phát triển
enable-enable-button = Bật công cụ phát triển
enable-close-button = Đóng thẻ này
welcome-title = Chào mừng đến với công cụ nhà phát triển Firefox!
newsletter-email-placeholder =
    .placeholder = Thư điện tử
newsletter-subscribe-button = Đăng ký
newsletter-thanks-title = Cảm ơn!
footer-title = Phiên bản Firefox cho nhà phát triển
footer-learn-more-link = Xem thêm
features-learn-more = Xem thêm
features-debugger-title = Trình gỡ lỗi
features-network-title = Mạng
features-responsive-title = Chế độ thiết kế tương thích
features-performance-title = Hiệu năng
features-memory-title = Bộ nhớ
