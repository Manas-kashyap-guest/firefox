# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

config-window =
    .title = about:config

## Strings used to display a warning in about:config

# This text should be attention grabbing and playful
config-about-warning-title =
    .value = Phần này có thể làm vô hiệu chế độ bảo hành!
config-about-warning-text = Thay đổi những thiết lập nâng cao này có thể gây hại cho sự ổn định, bảo mật và tính năng của ứng dụng này. Bạn chỉ nên tiếp tục nếu bạn hoàn toàn chắc chắn về những gì mình đang làm.
config-about-warning-button =
    .label = Tôi chấp nhận rủi ro!
config-about-warning-checkbox =
    .label = Hiện cảnh báo này vào lần sau
config-search-prefs =
    .value = Tìm kiếm:
    .accesskey = r
config-focus-search =
    .key = r
config-focus-search-2 =
    .key = f

## These strings are used for column headers

config-pref-column =
    .label = Tên Tùy Chọn
config-lock-column =
    .label = Trạng thái
config-type-column =
    .label = Kiểu
config-value-column =
    .label = Giá trị

## These strings are used for tooltips

config-pref-column-header =
    .tooltip = Nhấn để sắp xếp
config-column-chooser =
    .tooltip = Nhấn để chọn cột hiển thị

## These strings are used for the context menu

config-copy-pref =
    .key = C
    .label = Chép
    .accesskey = C
config-copy-name =
    .label = Chép Tên
    .accesskey = T
config-copy-value =
    .label = Chép Giá Trị
    .accesskey = G
config-modify =
    .label = Sửa đổi
    .accesskey = S
config-toggle =
    .label = Bật-Tắt
    .accesskey = B
config-reset =
    .label = Đặt về mặc định
    .accesskey = M
config-new =
    .label = Mới
    .accesskey = i
config-string =
    .label = Chuỗi kí tự
    .accesskey = C
config-integer =
    .label = Số nguyên
    .accesskey = S
config-boolean =
    .label = Luận lí
    .accesskey = L
config-default = mặc định
config-modified = đã sửa
config-locked = bị khóa
config-property-string = chuỗi kí tự
config-property-int = số nguyên
config-property-bool = luận lí
config-new-prompt = Nhập tên Tùy chọn
config-nan-title = Giá trị không hợp lệ
config-nan-text = Văn bản bạn điền vào không phải là số.
# Variables:
#   $type (String): type of value (boolean, integer or string)
config-new-title = Giá trị mới { $type }
# Variables:
#   $type (String): type of value (boolean, integer or string)
config-modify-title = Nhập giá trị { $type }
