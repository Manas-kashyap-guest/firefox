# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

url-classifier-provider-update-btn = Cập nhật
url-classifier-cache-title = Bộ nhớ đệm
url-classifier-cache-clear-btn = Xóa
url-classifier-debug-title = Gỡ lỗi
url-classifier-enabled = Đã bật
url-classifier-disabled = Đã tắt
url-classifier-updating = đang cập nhật
url-classifier-cannot-update = không thể cập nhật
url-classifier-success = thành công
