# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

password-not-set =
    .value = (chưa đặt)
failed-pw-change = Không thể thay đổi Mật khẩu Chính.
incorrect-pw = Bạn không nhập đúng Mật khẩu Chính hiện tại. Hãy thử lại.
pw-change-ok = Thay đổi Mật khẩu Chính thành công.
pw-empty-warning = Các mật khẩu web và email, dữ liệu biểu mẫu, và các khóa cá nhân của bạn sẽ không được bảo vệ.
pw-erased-ok = Bạn vừa xóa mật khẩu chính của mình.  { pw-empty-warning }
pw-not-wanted = Cảnh báo! Bạn vừa quyết định không dùng Mật khẩu Chính. { pw-empty-warning }
pw-change2empty-in-fips-mode = Bạn đang ở chế độ FIPS. FIPS yêu cầu Mật khẩu Chính không được để trống.
pw-change-success-title = Thay đổi Mật khẩu Thành công
pw-change-failed-title = Thay đổi Mật khẩu Thất bại
pw-remove-button =
    .label = Xóa
set-password =
    .title = Thay đổi mật khẩu chính
set-password-old-password = Mật khẩu hiện tại:
set-password-new-password = Nhập mật khẩu mới:
set-password-reenter-password = Nhập lại mật khẩu:
set-password-meter = Độ an toàn mật khẩu
set-password-meter-loading = Đang tải
master-password-description = Mật khẩu Chính được dùng để bảo vệ các thông tin nhạy cảm khi bạn duyệt web. Nếu tạo một Mật khẩu Chính bạn sẽ được yêu cầu nhập nó trong mỗi phiên làm việc, khi { -brand-short-name } truy vấn các thông tin đã được lưu và bảo vệ bởi mật khẩu.
master-password-warning = Hãy nhớ kĩ Mật khẩu Chính của mình. Nếu quên, bạn sẽ không thể truy cập các thông tin mà nó bảo vệ.
remove-password =
    .title = Xóa Mật Khẩu Chính
remove-info =
    .value = Bạn cần nhập mật khẩu hiện tại của mình để tiếp tục:
remove-warning1 = Mật khẩu Chính của bạn được dùng để bảo vệ các thông tin nhạy cảm như mật khẩu trang web.
remove-warning2 = Nếu bạn xóa Mật khẩu Chính, các thông tin của bạn sẽ không được bảo vệ nếu máy tính của bạn bị tấn công.
remove-password-old-password =
    .value = Mật khẩu hiện tại:
