# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Dùng thử ngay
onboarding-button-label-get-started = Bắt đầu
onboarding-welcome-header = Chào mừng đến với { -brand-short-name }
onboarding-start-browsing-button-label = Bắt đầu duyệt web

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Duyệt web riêng tư
onboarding-private-browsing-text = Duyệt bởi bản thân bạn. Duyệt web riêng tư với Trình chặn nội dung với nhiệm vụ chặn các trình theo dõi trực tuyến theo dõi bạn trên web.
onboarding-screenshots-title = Ảnh chụp màn hình
onboarding-screenshots-text = Tạo, lưu và chia sẻ ảnh chụp màn hình - mà không phải rời khỏi { -brand-short-name }. Chụp một khu vực hoặc toàn bộ trang khi bạn duyệt. Sau đó lưu vào web để dễ dàng truy cập và chia sẻ.
onboarding-addons-title = Tiện ích
onboarding-addons-text = Thêm nhiều tính năng hơn để { -brand-short-name } sẽ làm việc tích cực hơn cho bạn. So sánh giá cả, kiểm tra thời tiết hoặc thể hiện cá nhân hóa của bạn bằng một chủ đề tùy chỉnh.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Duyệt nhanh hơn, thông minh hơn hoặc an toàn hơn với các tiện ích mở rộng như Ghostery, cho phép bạn chặn quảng cáo gây phiền nhiễu.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Đồng bộ
onboarding-fxa-text = Đăng ký tài khoản { -fxaccount-brand-name } để đồng bộ dấu trang, mật khẩu và các thẻ đang mở của bạn khi sử dụng { -brand-short-name } ở mọi nơi.
