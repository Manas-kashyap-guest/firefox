# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Chính sách doanh nghiệp
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Kích hoạt
errors-tab = Lỗi
documentation-tab = Tài liệu
policy-name = Tên của chính sách
policy-value = Giá trị của chính sách
policy-errors = Lỗi chính sách
