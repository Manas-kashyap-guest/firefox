# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Ali
    .accesskey = A
languages-customize-movedown =
    .label = Ader
    .accesskey = d
languages-customize-remove =
    .label = Kkes
    .accesskey = K
languages-customize-select-language =
    .placeholder = Fren tutlayt ara ternuḍ...
languages-customize-add =
    .label = Rnu
    .accesskey = R
messenger-languages-window =
    .title = Iɣewwaṛen n tutlayt n { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } Ad d-yesken tutlayt tamezwarut d tutlayt-ik tamezwert, daɣen ad d-yesken tutlayin-nniḍen ma yessefk s usmizzwer akken i d-ttbanent.
