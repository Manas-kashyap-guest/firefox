<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!-- rights.locale-direction instead of the usual local.dir entity, so RTL can skip translating page. -->
<!ENTITY rights.locale-direction "ltr">
<!ENTITY rights.title            "Talɣut ɣef izerfan-ik">
<!ENTITY rights.intro-header     "Talɣut ɣef izerfan-ik">
<!ENTITY rights.intro "&brandFullName; d aseɣẓan ilelli yeldin, mahlent yifḍan n imttekkiyen deg umaḍal. Atan wayen issefk ad teẓreḍ:">
<!-- Note on pointa / pointb / pointc form:
     These points each have an embedded link in the HTML, so each point is
     split into chunks for text before the link, the link text, and the text
     after the link. If a localized grammar doesn't need the before or after
     chunk, it can be left blank.

     Also note the leading/trailing whitespace in strings here, which is
     deliberate for formatting around the embedded links. -->

<!ENTITY rights.intro-point1a "&brandShortName; ittumag i kečč ddaw tewtilin n ">
<!ENTITY rights.intro-point1b "Turagt tazayezt n Mozilla">
<!ENTITY rights.intro-point1c ". Ay-agi yebɣa ad yini tmzemreḍ ad tesqedceḍ, ad tesneɣleḍ u ad tezzuzreḍ &brandShortName; i wiyaḍ.  Tzemreḍ diɣen ad tesnifleḍ tangalt aɣbalu n &brandShortName; akken tesriḍ. Turagt tazayezt n Mozilla ak teǧǧ daɣen ad tezzuzreḍ ileqman ittusniflen.">

<!ENTITY rights.intro-point2a "Mozilla ur k-tettmuddu ara izerfan n tecraḍ neɣ iluguten n Mozilla akked Thunderbird. Yezmer ad tefaeḍ ugar n telɣut ɣef ticraḍ ">
<!ENTITY rights.intro-point2b "d-agi">
<!ENTITY rights.intro-point2c ".">
<!-- point 3 text for official branded builds -->

<!ENTITY rights.intro-point3a "Yezmer ad tafeḍ tasertit tabaḍnit n ifarisen n &vendorShortName;">
<!ENTITY rights.intro-point3b "d-agi">
<!ENTITY rights.intro-point3c ".">
<!-- point 3 text for unbranded builds -->

<!ENTITY rights.intro-point3-unbranded "Yal tasertit tabaḍnit yettwasnasen ɣef ufaris-agi ad tettwabder d-agi.">
<!-- point 4 text for official branded builds -->

<!ENTITY rights.intro-point4a "&brandShortName; yettak-d daɣen imeẓla nniḍen, am umeẓlu n uleqqem n izegrar, acu kan ur nezmir ara ad neḍmen d akken d imeɣta i 100&#037; neɣ ulac deg-sen tuccḍiwin. Ugar n telqayt, ula d talɣut ɣef tukksa n urmad n imeẓla, ad tafeḍ di">
<!ENTITY rights.intro-point4b "Tiwtilin n imeẓla">
<!ENTITY rights.intro-point4c ".">
<!-- point 4 text for unbranded builds -->

<!ENTITY rights.intro-point4a-unbranded "Ma yella asɣeẓan-agi isedduy tanfiwin web, issefk akk tisertiyin n useqdec ittusnasen ɣef tanfiwin-agi ad cudden ɣeṛ">
<!ENTITY rights.intro-point4b-unbranded "Imeẓla n usmel Web">
<!ENTITY rights.intro-point4c-unbranded "tigezmi.">

<!ENTITY rights.webservices-header "Imeẓla n usmel Web n &brandFullName; ">
<!-- Note that this paragraph references a couple of entities from
     preferences/security.dtd, so that we can refer to text the user sees in
     the UI, without this page being forgotten every time those strings are
     updated.  -->
<!-- intro paragraph for branded builds -->

<!ENTITY rights.webservices-a "&brandFullName; yettmuddu-d imeẓla ifrayanen (&quot;Services&quot;), am umeẓlu n uleqqem n izegrar, i d-ittuheggan i useqdec-inek di lqem-agi imsin n &brandShortName; akken d yettwaglem daw-a. M yella ur tebɣiḍ ad tesqedceḍ imeẓla neɣ  tiwtilin-agi ur k-ɛǧibent ara, tzemreḍ ad tekkseḍ armad n umeẓlu n uleqqem n izegrar s tulya n ismenyaf, s wefran n">
<!ENTITY rights.webservices-b "Talqayt">
<!ENTITY rights.webservices-c "  tigezmi, iḍefr-it ">
<!ENTITY rights.webservices-d "Leqqem">
<!ENTITY rights.webservices-e " iccer, sakin kkes rrcem n isefran i &quot;Senqed ileqman s wudem awurman i&quot;.">
<!-- intro paragraph for unbranded builds -->

<!ENTITY rights.webservices-unbranded "Taskant n tenfiwin n yesmal web yeddan deg useɣẓan, id d-yeglan s tinaḍin ɣef wamek ad tekkseḍ armad, ma yebbwi-d waddad, issefk ad yettwamel d-agi.">
<!-- point 1 text for unbranded builds -->

<!ENTITY rights.webservices-term1-unbranded "Yal tasertit tabaḍnit yettwasnasen ɣef ufaris-agi ad tettwabder d-agi.">
<!-- points 1-7 text for branded builds -->

<!ENTITY rights.webservices-term1 "&vendorShortName; izmer ad yeḥbes neɣ ad isnifel tanfiwin s tuffra.">
<!ENTITY rights.webservices-term2 "Ansuf yis-k ɣeṛ useqdec n imeẓla-yagi s i d-yessegla lqem n &brandShortName;, u ɣur-k akk izerfan i tesriḍ. &vendorShortName; akked imuragen ines ḥeṛṛen akk izerfan nniḍen deg iméẓla. Tiwtilin agi ur llint ara akken ad arrent tilisa i yizerfan d-ittunefken daw lewnaya n tugragin n useɣẓan ilelli yettwasnasen ɣef &brandShortName; neɣ ɣef tengalt aɣbalu n ileqman n &brandShortName;.">
<!ENTITY rights.webservices-term3 "Imeẓla ttunefken-d &quot;Akken llan.&quot;  &vendorShortName;, imttekkiyen ines, imuragen ines, akked iwzewziyen, ur ṭamanen ara d akken imeẓla, ama d wid d-yettwabedren s wudem aflalay neɣ ala, ay-agai yegber s war talast, ṭmana d akken imeẓla zemren ad ttwazenzen u ad ak-d-awin ayen k-ilaqen. D kečč ara yawin taɛekkemt n ufran n imeẓla i yesran-inek akked uqbel n tɣaṛa akked tmellit n imeẓla agi. Kra n yexxamen n creɛ u tt-sirigen-t ara tawsit-agi n ṭmana, ihi yezmer d akken agatu-yagi ur yettwasnas ara fell-ak.">
<!ENTITY rights.webservices-term4 "Except as required by law, &vendorShortName;, its contributors, licensors, and distributors will not be liable for any indirect, special, incidental, consequential, punitive, or exemplary damages arising out of or in any way relating to the use of &brandShortName; and the Services. The collective liability under these terms will not exceed $500 (five hundred dollars). Some jurisdictions do not allow the exclusion or limitation of certain damages, so this exclusion and limitation may not apply to you.">
<!ENTITY rights.webservices-term5 "&vendorShortName; izmer ad isnifel tiwtlin-agi ticki isra seg akud ɣeṛ wayeḍ. Tiwtilin-agi ur zmirent ara ad ttusniflent s war amtawa n &vendorShortName;.">
<!ENTITY rights.webservices-term6 "These terms are governed by the laws of the state of California, U.S.A., excluding its conflict of law provisions. If any portion of these terms is held to be invalid or unenforceable, the remaining portions will remain in full force and effect. In the event of a conflict between a translated version of these terms and the English language version, the English language version shall control.">
