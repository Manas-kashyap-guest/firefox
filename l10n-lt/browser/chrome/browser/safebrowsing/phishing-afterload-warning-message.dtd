<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Eiti atgal">
<!ENTITY safeb.palm.seedetails.label "Išsamesnė informacija">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Tai nėra apgaulinga svetainė…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "g">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Patarimus teikia <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Lankymasis šioje svetainėje gali pakenkti jūsų kompiuteriui">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "„&brandShortName;“ užblokavo šį tinklalapį, nes jis gali bandyti įdiegti kenkėjiškų programų, skirtų pavogti ar ištrinti jūsų duomenis.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Yra pranešta, jog <span id='malware_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>. Jūs galite <a id='report_detection'>pranešti apie aptikimo klaidą</a> arba <a id='ignore_warning_link'>nepaisyti įspėjimo</a> ir aplankyti šią nesaugią svetainę.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Yra pranešta, jog <span id='malware_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>. Jūs galite <a id='report_detection'>pranešti apie aptikimo klaidą</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Sužinokite daugiau apie kenkėjišką turinį internete, įskaitant virusus ir kitas kenkėjiškas programas, taip pat kaip nuo jų apsaugoti savo kompiuterį, apsilankę <a id='learn_more_link'>StopBadware.org</a>. Sužinokite daugiau apie „&brandShortName;“ apsaugą nuo sukčiavimo ir kenkėjiškų programų apsilankę <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Atveriama svetainė gali būti kenkėjiška">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "„&brandShortName;“ užblokavo šį tinklalapį, nes jis gali bandyti priversti jus įdiegti programų, kurios kenkia naršymui (pvz., pakeičia jūsų pradžios tinklalapį arba rodo daugiau reklamų lankomose svetainėse).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Yra pranešta, jog <span id='unwanted_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>. Jūs galite <a id='ignore_warning_link'>nepaisyti įspėjimo</a> ir aplankyti šią nesaugią svetainę.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Yra pranešta, jog <span id='unwanted_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Sužinokite daugiau apie kenkėjišką ir nepageidaujamą programinę įrangą <a id='learn_more_link'>nepageidaujamos programinės įrangos nuostatuose</a>. Sužinokite daugiau apie „&brandShortName;“ apsaugą nuo sukčiavimo ir kenkėjiškų programų apsilankę <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Atveriama apgaulinga svetainė">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "„&brandShortName;“ užblokavo šį tinklalapį, nes jis gali bandyti priversti jus atlikti kažką pavojingo, pvz., įdiegti programinę įrangą arba atskleisti asmeninę informaciją, kaip slaptažodžius ar banko korteles.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Yra pranešta, jog <span id='phishing_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>. Jūs galite <a id='report_detection'>pranešti apie aptikimo klaidą</a> arba <a id='ignore_warning_link'>nepaisyti įspėjimo</a> ir aplankyti šią nesaugią svetainę.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Yra pranešta, jog <span id='phishing_sitename'/> svetainė <a id='error_desc_link'>yra apgaulinga</a>. Jūs galite <a id='report_detection'>pranešti apie aptikimo klaidą</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Sužinokite daugiau apie apgaulingas svetaines ir sukčiavimą apsilankę <a id='learn_more_link'>www.antiphishing.org</a>. Sužinokite daugiau apie „&brandShortName;“ apsaugą nuo sukčiavimo ir kenkėjiškų programų apsilankę <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Atveriama svetainė gali būti kenkėjiška">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "„&brandShortName;“ užblokavo šį tinklalapį, nes jis gali bandyti įdiegti pavojingų programų, skirtų pavogti ar ištrinti jūsų duomenis (pvz., nuotraukas, slaptažodžius, žinutes, banko korteles).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Yra pranešta, jog <span id='harmful_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>. Galite <a id='ignore_warning_link'>nepaisyti įspėjimo</a> ir aplankyti šią nesaugią svetainę.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Yra pranešta, jog <span id='harmful_sitename'/> svetainė <a id='error_desc_link'>yra kenkėjiška</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Sužinokite daugiau apie „&brandShortName;“ apsaugą nuo sukčiavimo ir kenkėjiškų programų apsilankę <a id='firefox_support'>support.mozilla.org</a>.">
