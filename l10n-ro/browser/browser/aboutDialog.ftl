# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

aboutDialog-title =
    .title = Despre { -brand-full-name }
releaseNotes-link = Ce este nou
update-checkForUpdatesButton =
    .label = Caută actualizări
    .accesskey = C
update-updateButton =
    .label = Repornește pentru a actualiza { -brand-shorter-name }
    .accesskey = R
update-checkingForUpdates = Se caută actualizări…
update-downloading = <img data-l10n-name="icon"/>Se descarcă actualizarea — <label data-l10n-name="download-status"/>
update-applying = Se aplică actualizarea…
update-failed = Actualizare eșuată. <label data-l10n-name="failed-link">Descarcă cea mai recentă versiune</label>
update-failed-main = Actualizare eșuată. <a data-l10n-name="failed-link-main">Descarcă cea mai recentă versiune</a>
update-adminDisabled = Actualizările sunt dezactivate de administratorul de sistem
update-noUpdatesFound = { -brand-short-name } este actualizat
update-otherInstanceHandlingUpdates = { -brand-short-name } este actualizat de o altă instanță
update-manual = Actualizări disponibile la <label data-l10n-name="manual-link"/>
update-unsupported = Nu poți efectua actualizări suplimentare pe acest sistem. <label data-l10n-name="unsupported-link">Află mai multe</label>
update-restarting = Se repornește…
channel-description = Folosești în prezent <label data-l10n-name="current-channel"></label> drept canal de actualizare.{ " " }
warningDesc-version = { -brand-short-name } este experimental și poate fi instabil.
community-exp = <label data-l10n-name="community-exp-mozillaLink">{ -vendor-short-name }</label> este o <label data-l10n-name="community-exp-creditsLink">comunitate globală</label> care lucrează împreună pentru a păstra webul deschis, public și accesibil tuturor.
community-2 = { -brand-short-name } este conceput de <label data-l10n-name="community-mozillaLink">{ -vendor-short-name }</label>, o <label data-l10n-name="community-creditsLink">comunitate globală</label> care lucrează împreună pentru a păstra webul deschis, public și accesibil tuturor.
helpus = Vrei să ajuți? <label data-l10n-name="helpus-donateLink">Fă o donație</label> sau <label data-l10n-name="helpus-getInvolvedLink">implică-te!</label>
bottomLinks-license = Informații privind licențierea
bottomLinks-rights = Drepturile utilizatorului final
bottomLinks-privacy = Politica de confidențialitate
aboutDialog-architecture-sixtyFourBit = 64 de biți
aboutDialog-architecture-thirtyTwoBit = 32 de biți
