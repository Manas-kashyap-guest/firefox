<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Întoarce-te">
<!ENTITY safeb.palm.seedetails.label "Vezi detalii">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Nu este un site înșelător…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Avertismente furnizate de <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Vizitarea acestui site web poate dăuna calculatorului">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; a blocat această pagină deoarece ar putea încerca să instaleze programe rău intenționate care pot fura sau ștarge informații cu caracter personal de pe calculator.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> a fost <a id='error_desc_link'>raportat ca având programe rău intenționate</a>. Poți <a id='report_detection'>raporta o problemă de detectare</a> sau <a id='ignore_warning_link'>ignoră riscul</a> și mergi pe acest site nesigur.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> a fost <a id='error_desc_link'>raportat ca având conținut de programe rău intenționate</a>. Poți <a id='report_detection'>să raportezi o problemă de detectare</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Află mai multe despre conținuturi web dăunătoare, inclusiv viruși și alte programe rău intenționate și despre cum să îți protejezi calculatorul la <a id='learn_more_link'>StopBadware.org</a>. Află mai multe despre protecția împotriva furtului de date prin e-mail și a programelor rău intenționate în &brandShortName; la <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Site-ul pe care urmează să îl accesezi poate conține programe dăunătoare">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; a blocat această pagină deoarece ar putea să încerce să te păcălească să instalezi programe care dăunează experienței de navigare (de exemplu, prin schimbarea paginii de start sau prin afișarea de reclame suplimentare pe site-urile pe care le vizitezi).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> a fost <a id='error_desc_link'>raportat ca având programe dăunătoare</a>. Poți <a id='ignore_warning_link'>ignora riscul</a> și să mergi pe acest site nesigur.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> a fost <a id='error_desc_link'>raportat ca având programe dăunătoare</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Află mai multe despre programele dăunătoare și nedorite la <a id='learn_more_link'>Politica privind programele nedorite</a>. Află mai multe despre protecția împotriva furtului de date prin e-mail și a programelor rău intenționate în &brandShortName; la <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Urmează un site înșelător">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; a blocat această pagină deoarece te-ar putea păcăli să faci ceva periculos, cum ar fi să instalezi programare sau să divulgi informații cu caracter personal precum parole sau carduri de credit.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> a fost <a id='error_desc_link'>raportat ca site înșelător</a>. Poți <a id='report_detection'>raporta o problemă de detectare</a> sau <a id='ignore_warning_link'>ignoră riscul</a> și mergi la site-ul nesigur.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/>a fost <a id='error_desc_link'> raportat ca site înșelător</a>. Poți <a id='report_detection'>raporta o problemă de detectare</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Află mai multe despre site-uri înșelătoare și furtul de date prin e-mail la <a id='learn_more_link'>www.antiphishing.org</a>. Află mai multe despre protecția împotriva furtului de date prin e-mail și a programelor rău intenționate în &brandShortName; la <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Site-ul pe care urmează să îl accesezi poate conține programe rău intenționate">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; a blocat această pagină deoarece ar putea să încerce să instaleze programe periculoase care fură sau șterg informațiile tale (de exemplu, fotografii, parole, mesaje și carduri de credit).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> a fost <a id='error_desc_link'>raportat ca având o aplicație potențial dăunătoare</a>. Poți <a id='ignore_warning_link'>ignora riscul</a> și să mergi pe acest site nesigur.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> a fost <a id='error_desc_link'>raportat ca având o aplicație potențial dăunătoare</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Află mai multe despre protecția împotriva furtului de date prin e-mail și a programelor rău intenționate în &brandShortName; la <a id='firefox_support'>support.mozilla.org</a>.">
