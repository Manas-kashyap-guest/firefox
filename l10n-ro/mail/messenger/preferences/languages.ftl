# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Mai sus
    .accesskey = U
languages-customize-movedown =
    .label = Mai jos
    .accesskey = J
languages-customize-remove =
    .label = Elimină
    .accesskey = E
languages-customize-select-language =
    .placeholder = Alege limba de adăugat...
languages-customize-add =
    .label = Adaugă
    .accesskey = A
messenger-languages-window =
    .title = Setări limbă { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } va afișa prima limbă ca cea implicită și va afișa limbile alternative dacă este necesar în ordinea în care acestea apar.
