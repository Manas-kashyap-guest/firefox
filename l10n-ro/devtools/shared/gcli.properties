# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Web Console
# command line which is available from the Web Developer sub-menu
# -> 'Web Console'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# For each command there are in general two strings. As an example consider
# the 'pref' command.
# commandDesc (e.g. prefDesc for the command 'pref'): this string contains a
# very short description of the command. It's designed to be shown in a menu
# alongside the command name, which is why it should be as short as possible.
# commandManual (e.g. prefManual for the command 'pref'): this string will
# contain a fuller description of the command. It's displayed when the user
# asks for help about a specific command (e.g. 'help pref').

# LOCALIZATION NOTE: This message is used to describe any command or command
# parameter when no description has been provided.
canonDescNone=(Fără descriere)

# LOCALIZATION NOTE: The default name for a group of parameters.
canonDefaultGroupName=Opțiuni

# LOCALIZATION NOTE (canonProxyDesc, canonProxyManual): These commands are
# used to execute commands on a remote system (using a proxy). Parameters: %S
# is the name of the remote system.
canonProxyDesc=Execută o comandă pe %S
canonProxyManual=Un set de comenzi executate pe un sistem la distanță. Sistemul la distanță este accesat prin %S

# LOCALIZATION NOTE: This error message is displayed when we try to add a new
# command (using a proxy) where one already exists with the same name.
canonProxyExists=Deja există o comandă numită „%S”

# LOCALIZATION NOTE: This message describes the '{' command, which allows
# entry of JavaScript like traditional developer tool command lines.
cliEvalJavascript=Introdu direct JavaScript

# LOCALIZATION NOTE: This message is displayed when the command line has more
# arguments than the current command can understand.
cliUnusedArg=Prea multe argumente

# LOCALIZATION NOTE: The title of the dialog which displays the options that
# are available to the current command.
cliOptions=Opțiuni disponibile

# LOCALIZATION NOTE: The error message when the user types a command that
# isn't registered
cliUnknownCommand2=Comandă invalidă: „%1$S”.

# LOCALIZATION NOTE: A parameter should have a value, but doesn't
cliIncompleteParam=Este necesară o valoare pentru „%1$S”.

# LOCALIZATION NOTE: Error message given when a file argument points to a file
# that does not exist, but should (e.g. for use with File->Open) %1$S is a
# filename
fileErrNotExists=„%1$S” nu există

# LOCALIZATION NOTE: Error message given when a file argument points to a file
# that exists, but should not (e.g. for use with File->Save As) %1$S is a
# filename
fileErrExists=„%1$S” deja există

# LOCALIZATION NOTE: Error message given when a file argument points to a
# non-file, when a file is needed. %1$S is a filename
fileErrIsNotFile=„%1$S” nu este fișier

# LOCALIZATION NOTE: Error message given when a file argument points to a
# non-directory, when a directory is needed (e.g. for use with 'cd') %1$S is a
# filename
fileErrIsNotDirectory=„%1$S” nu este director

# LOCALIZATION NOTE: Error message given when a file argument does not match
# the specified regular expression %1$S is a filename %2$S is a regular
# expression
fileErrDoesntMatch=„%1$S” nu corespunde cu „%2$S”

# LOCALIZATION NOTE: When the menu has displayed all the matches that it
# should (i.e. about 10 items) then we display this to alert the user that
# more matches are available.
fieldMenuMore=Mai multe rezultate, continuă să scrii

# LOCALIZATION NOTE: The command line provides completion for JavaScript
# commands, however there are times when the scope of what we're completing
# against can't be used. This error message is displayed when this happens.
jstypeParseScope=Domeniu pierdut

# LOCALIZATION NOTE (jstypeParseMissing, jstypeBeginSyntax,
# jstypeBeginUnterm): These error messages are displayed when the command line
# is doing JavaScript completion and encounters errors.
jstypeParseMissing=Nu se poate găsi proprietatea ‘%S’
jstypeBeginSyntax=Eroare de sintaxă
jstypeBeginUnterm=Șir literal neterminat

# LOCALIZATION NOTE: This message is displayed if the system for providing
# JavaScript completions encounters and error it displays this.
jstypeParseError=Eroare

# LOCALIZATION NOTE (typesNumberNan, typesNumberNotInt2, typesDateNan): These
# error messages are displayed when the command line is passed a variable
# which has the wrong format and can't be converted. Parameters: %S is the
# passed variable.
typesNumberNan=Nu se poate converti „%S” într-un număr.
typesNumberNotInt2=Nu se poate converti „%S” într-un integer.
typesDateNan=Nu se poate converti „%S” într-o dată.

# LOCALIZATION NOTE (typesNumberMax, typesNumberMin, typesDateMax,
# typesDateMin): These error messages are displayed when the command line is
# passed a variable which has a value out of range (number or date).
# Parameters: %1$S is the passed variable, %2$S is the limit value.
typesNumberMax=%1$S este mai mare decât maximul permis: %2$S.
typesNumberMin=%1$S este mai mic decât minimul permis: %2$S.
typesDateMax=%1$S este ulterior maximului permis: %2$S.
typesDateMin=%1$S este anterior minimului permis: %2$S.

# LOCALIZATION NOTE: This error message is displayed when the command line is
# passed an option with a limited number of correct values, but the passed
# value is not one of them.
typesSelectionNomatch=Nu se poate folosi „%S”.

# LOCALIZATION NOTE: This error message is displayed when the command line is
# expecting a CSS query string, however the passed string is not valid.
nodeParseSyntax=Eroare de sintaxă în interogarea CSS

# LOCALIZATION NOTE (nodeParseMultiple, nodeParseNone): These error messages
# are displayed when the command line is expecting a CSS string that matches a
# single node, but more nodes (or none) match.
nodeParseMultiple=Prea multe rezultate (%S)
nodeParseNone=Niciun rezultat

# LOCALIZATION NOTE (helpDesc, helpManual, helpSearchDesc, helpSearchManual3):
# These strings describe the "help" command, used to display a description of
# a command (e.g. "help pref"), and its parameter 'search'.
helpDesc=Obține ajutor pe comenzile disponibile
helpManual=Asigură ajutor fie pe o comandă specifică (dacă se dă șirul de căutare și s-a găsit o corespondență exactă), fie pe comenzile disponibile (dacă nu se dă un șir de căutare sau dacă nu se găsește o corespondență exactă).
helpSearchDesc=Șir de căutare
helpSearchManual3=șir de căutare de utilizat la restrângerea comenzilor afișate. Expresiile regulate nu sunt permise.

# LOCALIZATION NOTE: These strings are displayed in the help page for a
# command in the console.
helpManSynopsis=Rezumat

# LOCALIZATION NOTE: This message is displayed in the help page if the command
# has no parameters.
helpManNone=Niciunul

# LOCALIZATION NOTE: This message is displayed in response to the 'help'
# command when used without a filter, just above the list of known commands.
helpListAll=Comenzi disponibile:

# LOCALIZATION NOTE (helpListPrefix, helpListNone): These messages are
# displayed in response to the 'help <search>' command (i.e. with a search
# string), just above the list of matching commands. Parameters: %S is the
# search string.
helpListPrefix=Comenzi care încep cu „%S”:
helpListNone=Nicio comandă nu începe cu „%S”

# LOCALIZATION NOTE (helpManRequired, helpManOptional, helpManDefault): When
# the 'help x' command wants to show the manual for the 'x' command, it needs
# to be able to describe the parameters as either required or optional, or if
# they have a default value.
helpManRequired=necesare
helpManOptional=opțională
helpManDefault=opțional, implicit=%S

# LOCALIZATION NOTE: This forms part of the output from the 'help' command.
# 'GCLI' is a project name and should be left untranslated.
helpIntro=GCLI este un experiment de creat o linie de comandă pentru dezvoltatorii web foarte ușor de utilizat.

# LOCALIZATION NOTE: Text shown as part of the output of the 'help' command
# when the command in question has sub-commands, before a list of the matching
# sub-commands.
subCommands=Subcomenzi

# LOCALIZATION NOTE: This error message is displayed when the command line is
# cannot find a match for the parse types.
commandParseError=Eroare de analiză sintactică a liniei de comandă

# LOCALIZATION NOTE (contextDesc, contextManual, contextPrefixDesc): These
# strings are used to describe the 'context' command and its 'prefix'
# parameter. See localization comment for 'connect' for an explanation about
# 'prefix'.
contextDesc=Concentrează pe un grup de comenzi
contextManual=Configurează un prefix implicit pentru comenzi viitoare. De exemplu, „context git” ar permite să scrii „commit” în loc de „git commit”.
contextPrefixDesc=Prefixul de comandă

# LOCALIZATION NOTE: This message message displayed during the processing of
# the 'context' command, when the found command is not a parent command.
contextNotParentError=Nu se poate utiliza „%S” ca prefix de comandă pentru că nu este comandă-părinte.

# LOCALIZATION NOTE (contextReply, contextEmptyReply): These messages are
# displayed during the processing of the 'context' command, to indicate
# success or that there is no command prefix.
contextReply=Se folosește %S ca prefix de comandă
contextEmptyReply=Prefixul comenzii nu este setat

# LOCALIZATION NOTE (connectDesc, connectManual, connectPrefixDesc,
# connectMethodDesc, connectUrlDesc, connectDupReply): These strings describe
# the 'connect' command and all its available parameters. A 'prefix' is an
# alias for the remote server (think of it as a "connection name"), and it
# allows to identify a specific server when connected to multiple remote
# servers.
connectDesc=Comenzi proxy către server
connectManual=Conectează-te la server, creând versiuni locale ale comenzilor de pe server. Comenzile la distanță au inițial un prefix care să le diferențieze de comenzile locale (dar vezi comanda contextuală ca să ocolești utilizarea prefixului)
connectPrefixDesc=Prefix-părinte pentru comenzi importate
connectMethodDesc=Metoda de conectare
connectUrlDesc=URL la care să te conectezi
connectDupReply=Conexiunea numită %S există deja.

# LOCALIZATION NOTE: The output of the 'connect' command, telling the user
# what it has done. Parameters: %S is the prefix command. See localization
# comment for 'connect' for an explanation about 'prefix'.
connectReply=Comenzi %S adăugate.

# LOCALIZATION NOTE (disconnectDesc2, disconnectManual2,
# disconnectPrefixDesc): These strings describe the 'disconnect' command and
# all its available parameters. See localization comment for 'connect' for an
# explanation about 'prefix'.
disconnectDesc2=Deconectează de la server
disconnectManual2=Deconectează de la un server conectat la momentul respectiv pentru executarea comenzilor la distanță
disconnectPrefixDesc=Prefix-părinte pentru comenzi importate

# LOCALIZATION NOTE: This is the output of the 'disconnect' command,
# explaining the user what has been done. Parameters: %S is the number of
# commands removed.
disconnectReply=Comenzi %S eliminate.

# LOCALIZATION NOTE: These strings describe the 'clear' command
clearDesc=Golește zona de ieșire

# LOCALIZATION NOTE (prefDesc, prefManual, prefListDesc, prefListManual,
# prefListSearchDesc, prefListSearchManual, prefShowDesc, prefShowManual,
# prefShowSettingDesc, prefShowSettingManual): These strings describe the
# 'pref' command and all its available sub-commands and parameters.
prefDesc=Comenzi pentru controlarea setărilor
prefManual=Comenzi pentru afișarea și modificarea preferințelor, atât pentru GCLI, cât și pentru mediul înconjurător
prefListDesc=Afișează setările disponibile
prefListManual=Afișează o listă de preferințe, opțional filtrate, când se folosește parametrul „search”
prefListSearchDesc=Filtrează lista de setări afișate
prefListSearchManual=Caută șirul dat în lista de preferințe disponibile
prefShowDesc=Afișează valoarea setării
prefShowManual=Afișează valoarea unei preferințe date
prefShowSettingDesc=Setare de afișat
prefShowSettingManual=Numele setării de afișat

# LOCALIZATION NOTE: This message is used to show the preference name and the
# associated preference value. Parameters: %1$S is the preference name, %2$S
# is the preference value.
prefShowSettingValue=%1$S: %2$S

# LOCALIZATION NOTE (prefSetDesc, prefSetManual, prefSetSettingDesc,
# prefSetSettingManual, prefSetValueDesc, prefSetValueManual): These strings
# describe the 'pref set' command and all its parameters.
prefSetDesc=Modifică o setare
prefSetManual=Modifică preferințe definite de mediu
prefSetSettingDesc=Setare de modificat
prefSetSettingManual=Numele setării de modificat.
prefSetValueDesc=Valoare nouă pentru setare
prefSetValueManual=Valoarea nouă pentru setarea specificată

# LOCALIZATION NOTE (prefResetDesc, prefResetManual, prefResetSettingDesc,
# prefResetSettingManual): These strings describe the 'pref reset' command and
# all its parameters.
prefResetDesc=Resetează o setare
prefResetManual=Resetează valoarea unei setări la valorile implicite ale sistemului
prefResetSettingDesc=Setare de resetat
prefResetSettingManual=Numele setării de resetat la valoarea implicită a sistemului

# LOCALIZATION NOTE: This string is displayed in the output from the 'pref
# list' command as a label to an input element that allows the user to filter
# the results.
prefOutputFilter=Filtrează

# LOCALIZATION NOTE (prefOutputName, prefOutputValue): These strings are
# displayed in the output from the 'pref list' command as table headings.
prefOutputName=Nume
prefOutputValue=Valoare

# LOCALIZATION NOTE (introTextOpening3, introTextCommands, introTextKeys2,
# introTextF1Escape, introTextGo): These strings are displayed when the user
# first opens the developer toolbar to explain the command line, and is shown
# each time it is opened until the user clicks the 'Got it!' button.
introTextOpening3=GCLI este un experiment de creat o linie de comandă pentru dezvoltatorii web ușor de utilizat.
introTextCommands=Pentru o listă de comenzi, tastează
introTextKeys2=, sau, pentru afișarea/ascunderea sugestiilor de comenzi, apasă
introTextF1Escape=F1/Escape
introTextGo=Am înțeles!

# LOCALIZATION NOTE: This is a short description of the 'hideIntro' setting.
hideIntroDesc=Afișează mesajul inițial de întâmpinare

# LOCALIZATION NOTE: This is a description of the 'eagerHelper' setting. It's
# displayed when the user asks for help on the settings. eagerHelper allows
# users to select between showing no tooltips, permanent tooltips, and only
# important tooltips.
eagerHelperDesc=Cât de rapide sunt sugestiile pentru unelte
