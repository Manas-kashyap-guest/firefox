<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Conectează-te la &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Pentru a activa noul dispozitiv, selectează „Configurează &syncBrand.shortName.label;” pe dispozitiv.'>
<!ENTITY sync.subtitle.pair.label 'Pentru activare, selectează „Asociază un dispozitiv” de pe celălalt dispozitiv.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Nu am dispozitivul cu mine…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Autentificări'>
<!ENTITY sync.configure.engines.title.history 'Istoric'>
<!ENTITY sync.configure.engines.title.tabs 'File'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; pe &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Meniu de marcaje'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etichete'>
<!ENTITY bookmarks.folder.toolbar.label 'Bara de marcaje'>
<!ENTITY bookmarks.folder.other.label 'Alte marcaje'>
<!ENTITY bookmarks.folder.desktop.label 'Marcaje de pe desktop'>
<!ENTITY bookmarks.folder.mobile.label 'Marcaje mobile'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fixat'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Înapoi la navigare'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Bun venit la &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Autentifică-te pentru a sincroniza filele, marcajele, parolele și altele.'>
<!ENTITY fxaccount_getting_started_get_started 'Începe'>
<!ENTITY fxaccount_getting_started_old_firefox 'Folosești o versiune mai veche de &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Serverul contului'>
<!ENTITY fxaccount_status_sync_now 'Sincronizează acum'>
<!ENTITY fxaccount_status_syncing2 'Se sincronizează…'>
<!ENTITY fxaccount_status_device_name 'Numele dispozitivului'>
<!ENTITY fxaccount_status_sync_server 'Serverul de sincronizare'>
<!ENTITY fxaccount_status_needs_verification2 'Contul trebuie să fie verificat. Atinge pentru a retrimite e-mailul de verificare.'>
<!ENTITY fxaccount_status_needs_credentials 'Nu se poate conecta. Atinge pentru a te autentifica.'>
<!ENTITY fxaccount_status_needs_upgrade 'Trebuie să actualizezi &brandShortName; pentru a te autentifica.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; este configurat, însă nu se sincronizează automat. Comută „Sincronizați automat datele” din setările Android &gt; Utilizare date.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; este configurat, însă nu se sincronizează automat. Comută „Sincronizați automat datele” din setările Android &gt; Conturi.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Atinge pentru a te autentifica în contul nou Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Alege ce să sincronizezi'>
<!ENTITY fxaccount_status_bookmarks 'Marcaje'>
<!ENTITY fxaccount_status_history 'Istoric'>
<!ENTITY fxaccount_status_passwords2 'Autentificări'>
<!ENTITY fxaccount_status_tabs 'File deschise'>
<!ENTITY fxaccount_status_additional_settings 'Setări adiționale'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sincronizează doar prin Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Împiedică &brandShortName; să se sincronizeze printr-o rețea celulară sau contorizată'>
<!ENTITY fxaccount_status_legal 'Mențiuni legale' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Termenii de utilizare a serviciului'>
<!ENTITY fxaccount_status_linkprivacy2 'Politica de confidențialitate'>
<!ENTITY fxaccount_remove_account 'Deconectează-te&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Te deconectezi de la Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Datele de navigare vor rămâne pe acest dispozitv, însă nu se vor mai sincroniza cu contul tău.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Contul Firefox &formatS; deconectat.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Deconectează-te'>

<!ENTITY fxaccount_enable_debug_mode 'Activează modul de depanare'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Opțiuni &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Configurează &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; nu este conectat'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Atinge ca să te autentifici ca &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Ai terminat actualizarea &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Atinge ca să te autentifici ca &formatS;'>
