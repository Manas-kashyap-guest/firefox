# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-heading = शिफारस केलेले विस्तार
cfr-doorhanger-extension-sumo-link =
    .tooltiptext = मला हे का दिसत आहे
cfr-doorhanger-extension-cancel-button = आत्ता नाही
    .accesskey = N
cfr-doorhanger-extension-ok-button = आत्ताच जोडा
    .accesskey = A
cfr-doorhanger-extension-manage-settings-button = शिफारशी सेटिंग्ज व्यवस्थापित करा
    .accesskey = M
cfr-doorhanger-extension-never-show-recommendation = मला ही शिफारस दर्शवू नका
    .accesskey = S
cfr-doorhanger-extension-learn-more-link = अधिक जाणा
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } द्वारा
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = शिफारस

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

