# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

history-title = इतिहास सुधारा
history-intro = पुढील अद्ययावते स्थापन झालेले आहेत
close-button-label =
    .buttonlabelcancel = बंद करा
    .title = इतिहास सुधारा
no-updates-label = काही सुधारणा स्थापीत झाल्या नाहीत
name-header = नावात वाढ करा
date-header = दिनांक स्थापीत करा
type-header = प्रकार
state-header = राज्य
# Used to display update history
#
# Variables:
#   $name (String): name of the update
#   $buildID (String): build identifier from the local updates.xml
update-full-name =
    .name = { $name } ({ $buildID })
