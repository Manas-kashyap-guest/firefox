<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Повернутись назад">
<!ENTITY safeb.palm.seedetails.label "Показати деталі">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Це не шахрайський сайт…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "н">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Інформація отримана від <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Відвідування цього сайту може зашкодити вашому комп’ютеру">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; заблокував цю сторінку, бо вона може намагатися встановити зловмисні програми, що можуть викрадати або знищувати дані на вашому комп'ютері.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> відомий <a id='error_desc_link'>як такий, що містить зловмисні програми</a>. Ви можете <a id='report_detection'>повідомити про невідповідність цієї інформації</a>, або <a id='ignore_warning_link'>ігнорувати ризик</a> і перейти на цей небезпечний сайт.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> відомий <a id='error_desc_link'>як такий, що містить зловмисні програми</a>. Ви можете <a id='report_detection'>повідомити про невідповідність цієї інформації</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Дізнайтеся більше про небезпечний веб-вміст, включаючи віруси та інші зловмисні програми, а також про те, як захистити свій комп'ютер, на сайті <a id='learn_more_link'>StopBadware.org</a>. Дізнайтеся більше про те, як працює захист від зловмисного вмісту в &brandShortName; на сайті <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Цей сайт може містити небезпечні програми">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; заблокував цю сторінку, тому що вона може спробувати змусити вас встановити програми, які будуть заважати роботі в браузері (наприклад, змінити домівку або показувати зайву рекламу на відвідуваних вами сайтах).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> відомий <a id='error_desc_link'>як такий, що містить зловмисні програми</a>. Ви можете <a id='ignore_warning_link'>ігнорувати ризик</a> і перейти на цей небезпечний сайт.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> відомий <a id='error_desc_link'>як такий, що містить зловмисні програми</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Дізнайтеся більше про небезпечне й небажане програмне забезпечення на сайті <a id='learn_more_link'>Правила щодо небажаного ПЗ</a>. Дізнайтеся більше про те, як працює захист від шахрайських та зловмисних програм в &brandShortName; на сайті <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Це шахрайський сайт">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; заблокував цю сторінку, тому що вона може спробувати змусити вас зробити щось небезпечне, наприклад встановити програму або розкрити особисту інформацію, таку як паролі або реквізити банківських карт.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> відомий <a id='error_desc_link'>шахрайський сайт</a>. Ви можете <a id='report_detection'>повідомити про невідповідність цієї інформації</a>, або <a id='ignore_warning_link'>ігнорувати ризик</a> і перейти на цей небезпечний сайт.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> відомий <a id='error_desc_link'>шахрайський сайт</a>. Ви можете <a id='report_detection'>повідомити про невідповідність цієї інформації </a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Дізнайтеся більше про шахрайські сайти та шахрайство на сайті <a id='learn_more_link'>www.antiphishing.org</a>. Дізнайтеся більше про те, як працює захист від шахрайських та зловмисних програм в &brandShortName; на сайті <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Цей сайт може містити зловмисне програмне забезпечення">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; заблокував цю сторінку, тому що вона може намагатися встановити небезпечне програмне забезпечення, що викрадає чи видаляє вашу інформацію (наприклад, фотографії, паролі, повідомлення й кредитні картки).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> відомий <a id='error_desc_link'>як такий, що містить потенційно зловмисну програму</a>. Ви можете <a id='ignore_warning_link'>ігнорувати ризик</a> і перейти на цей небезпечний сайт.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> відомий <a id='error_desc_link'>як такий, що містить потенційно зловмисну програму</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Дізнайтеся більше про те, як працює захист від шахрайства та зловмисного програмного забезпечення в &brandShortName; на сайті <a id='firefox_support'>support.mozilla.org</a>.">
