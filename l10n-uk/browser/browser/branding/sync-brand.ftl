# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

-sync-brand-short-name =
    {
       *[nom] Синхронізація
        [nom-lower] синхронізація
        [gen] Синхронізації
        [gen-lower] синхронізації
        [dat] Синхронізації
        [dat-lower] синхронізації
        [acc] Синхронізацію
        [acc-lower] синхронізацію
        [abl] Синхронізацією
        [abl-lower] синхронізацією
    }
# “Sync” can be localized, “Firefox” must be treated as a brand,
# and kept in English.
-sync-brand-name =
    {
       *[nom] Синхронізація Firefox
        [nom-lower] синхронізація Firefox
        [gen] Синхронізації Firefox
        [gen-lower] синхронізації Firefox
        [dat] Синхронізації Firefox
        [dat-lower] синхронізації Firefox
        [acc] Синхронізацію Firefox
        [acc-lower] синхронізацію Firefox
        [abl] Синхронізацією Firefox
        [abl-lower] синхронізацією Firefox
    }
# “Account” can be localized, “Firefox” must be treated as a brand,
# and kept in English.
-fxaccount-brand-name =
    {
       *[nom] Обліковий запис Firefox
        [nom-lower] обліковий запис Firefox
        [gen] Облікового запису Firefox
        [gen-lower] облікового запису Firefox
        [dat] Обліковому записі Firefox
        [dat-lower] обліковому записі Firefox
        [acc] Обліковий запис Firefox
        [acc-lower] обліковий запис Firefox
        [abl] Обліковим записом Firefox
        [abl-lower] обліковим записом Firefox
    }
