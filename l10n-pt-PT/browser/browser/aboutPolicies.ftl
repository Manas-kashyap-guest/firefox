# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Políticas empresariais
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Ativas
errors-tab = Erros
documentation-tab = Documentação
policy-name = Nome da política
policy-value = Valor da política
policy-errors = Erros de políticas
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = Ao utilizar a Política de grupo, esta política apenas pode ser definida ao nível do computador.
