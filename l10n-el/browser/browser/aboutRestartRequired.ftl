# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Απαιτείται Επανεκκίνηση
restart-required-header = Συγγνώμη. Θα πρέπει να κάνουμε μια μικρή εργασία για να συνεχίσουμε.
restart-required-description = Θα επαναφέρουμε όλες τις σελίδες, τα παράθυρα και τις καρτέλες σας αμέσως μετά. Έτσι, θα μπορέσετε να συνεχίσετε γρήγορα.
restart-button-label = Επανεκκίνηση του { -brand-short-name }
