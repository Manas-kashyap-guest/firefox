# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Page title
about-performance-title = Διαχείριση εργασιών

## Column headers

column-type = Τύπος
column-energy-impact = Αντίκτυπο ενέργειας

## Special values for the Name column

ghost-windows = Καρτέλες που έκλεισαν πρόσφατα
# Variables:
#   $title (String) - the title of the preloaded page, typically 'New Tab'
preloaded-tab = Προφορτωμένη: { $title }

## Values for the Type column

type-tab = Καρτέλα

## Values for the Energy Impact column
##
## Variables:
##   $value (Number) - Value of the energy impact, eg. 0.25 (low),
##                     5.38 (medium), 105.38 (high)


## Tooltips for the action buttons

