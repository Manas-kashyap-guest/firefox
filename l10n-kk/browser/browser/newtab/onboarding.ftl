# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Оны қазір көру
onboarding-button-label-get-started = Бастау
onboarding-welcome-header = { -brand-short-name } өніміне қош келдіңіз
onboarding-start-browsing-button-label = Шолуды бастау

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Жекелік шолу режимі
onboarding-private-browsing-text = Өздігінен шолыңыз. Құраманы бұғаттауы бар жекелік шолу режимі интернетте сізді бақылайтын трекерлерді бұғаттайды.
onboarding-screenshots-title = Скриншоттар
onboarding-screenshots-text = Скриншоттарды түсіру, сақтау және олармен бөлісу - тура { -brand-short-name } ішінен. Шолу кезінде аймақты немесе толық парақты түсіріп алыңыз. Одан кейін, жеңіл қатынау және бөлісу үшін оны интернетте сақтаңыз.
onboarding-addons-title = Қосымшалар
onboarding-addons-text = Сіз үшін { -brand-short-name } күштірек жұмыс жасау үшін, оған мүмкіндіктерді қосыңыз. Бағаларды салыстыру, ауа райын тексеру немесе таңдауыңызша теманы орнатуға болады.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Мазаңызды алатын жарнаманы бұғаттай алатын Ghostery сияқты кеңейтулермен жылдам, ақылды және қауіпсіз түрде шолыңыз.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Синхрондау
onboarding-fxa-text = { -fxaccount-brand-name } тіркелгісін жасап, өзіңіз { -brand-short-name } қолданатын әр жерде бетбелгілер, парольдер және ашық беттерді синхрондаңыз.
