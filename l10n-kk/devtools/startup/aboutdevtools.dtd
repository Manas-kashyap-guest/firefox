<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Әзірлеуші құралдары жөнінде">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Firefox әзірлеуші құралдарын іске қосу">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Элементті бақылаушыны қолдану үшін Firefox Әзірлеуші құралдарын іске қосыңыз">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Әзірлеуші құралдарының Бақылаушысы көмегімен HTML мен CSS зерттеу және түзету.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Firefox Әзірлеуші құралдары көмегімен WebExtensions, Web workers, Service workers және көптеген т.б. әзірлеу және жөндеу.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Сіз Әзірлеуші құралдар жарлығын белсендірдіңіз. Бұл әрекет қате болса, бұл бетті жаба аласыз.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage
          "Бақылаушы және Жөндеуші сияқты құралдармен HTML, CSS және JavaScript қарау, түзету және жөндеу.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Бақылаушы және Жөндеуші сияқты құралдармен веб-сайтыңыздың HTML, CSS және JavaScript кодтарын жақсартыңыз.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Firefox Әзірлеуші құралдары үнсіз келісім бойынша сөндірілген, ол сізге браузерді жақсырақ басқаруға мүмкіндік береді.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Әзірлеуші құралдары туралы көбірек білу">

<!ENTITY  aboutDevtools.enable.enableButton "Әзірлеуші құралдарын іске қосу">
<!ENTITY  aboutDevtools.enable.closeButton "Бұл бетті жабу">

<!ENTITY  aboutDevtools.enable.closeButton2 "Бұл бетті жабу">

<!ENTITY  aboutDevtools.welcome.title "Firefox әзірлеуші құралдарына қош келдіңіз!">

<!ENTITY  aboutDevtools.newsletter.title "Әзірлеушілерге арналған Mozilla жаңалықтар таратуы">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Әзірлеушілер жаңалықтары, кеңестері және ресурстарды тікелей кіріс бумаңызға алыңыз.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "Эл. пошта">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Мен Mozilla-ның менің ақпаратымды мына <a class='external' href='https://www.mozilla.org/privacy/'>Құпиялылық саясатында</a> көрсетілгендей өңдейтінімен келісемін.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Жазылу">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Рахмет!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Mozilla-ға байланысты жаңалықтар таспасына жазылуды осыған дейін растамаған болсаңыз, оны жасауыңыз керек болуы мүмкін. Поштаңыздың кіріс және спам бумаларынан бізден хатты іздеңіз.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Әзірлеуші құралдарынан көбірек бірнәрсе іздедіңіз бе? Әзірлеушілер және заманауи жұмыс тәртібі үшін арнайы жасалған Firefox браузерін қолданып көріңіз.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Көбірек білу">

