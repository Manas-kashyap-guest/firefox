# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Permet de definir una URL de mesa a jorn personalizada per l’aplicacion.
policy-BlockAboutConfig = Blòca l’accès a la pagina about:config.
policy-BlockAboutProfiles = Blòca l’accès a la pagina about:profiles.
policy-BlockAboutSupport = Blòca l’accès a la pagina about:support.
policy-CertificatesDescription = Apondre de certificats o utilizar de certificats predefinits.
policy-Cookies = Permet o defend als sites de definir de cookies.
policy-DisableAppUpdate = Empacha lo navigador de se metre a jorn.
policy-DisableBuiltinPDFViewer = Desactiva PDF.js, lo visionador integrat de PDF dins { -brand-short-name }.
policy-DisableDeveloperTools = Blòca l’accès a las aisinas de desvolopament.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Desactivar la foncionalitat de presa de capture Firefox Screenshots.
policy-DisableFirefoxStudies = Empacha { -brand-short-name } d’executar d’estudis.
policy-DisableForgetButton = Empacha l’accès al boton d’escafament de las donadas.
policy-DisableFormHistory = Conservar pas d’istoric de las recèrcas e dels formularis.
policy-DisableMasterPasswordCreation = Se activat, serà impossible de crear un senhal principal.
policy-DisablePocket = Desactiva la foncionalitat d’enregistrament de paginas web dins Pocket.
policy-DisablePrivateBrowsing = Desactiva la navigacion privada.
policy-DisableTelemetry = Desactiva la telemetria.
policy-DisplayBookmarksToolbar = Aficha la barra personala per defaut.
policy-DisplayMenuBar = Aficha la barra de menús per defaut.
policy-DNSOverHTTPS = Permet de configurar lo DNS over HTTPS.
policy-FlashPlugin = Autoriza o pas l’utilizacion del plugin Flash.
policy-HardwareAcceleration = Se fals, desactiva l’acceleracion materiala.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = Definís la pagina d’acuèlh e prepausa de la verrolhar.
policy-InstallAddonsPermission = Autoriza certans sites web d'installar d'extensions.
policy-Permissions = Configurar las autorizacions per la camèra, lo microfòn, lo localizacion e las notificacions.
policy-Proxy = Configura los paramètres del servidor mandatari.
policy-SanitizeOnShutdown = Suprimís totas las donadas de navigacion a la tampadura.
# For more information, see https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/PKCS11/Module_Installation
policy-SecurityDevices = Permet d’installar de moduls PKCS #11.
