# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = پیدا کردن رخداد بعدی عبارت
findbar-previous =
    .tooltiptext = پیدا کردن رخداد قبلی عبارت
findbar-find-button-close =
    .tooltiptext = بستن نوار پیدا کردن
findbar-highlight-all =
    .label = پررنگ‌سازی همه
    .accesskey = ه
    .tooltiptext = پررنگ‌سازی همهٔ رخدادهای عبارت
findbar-case-sensitive =
    .label = تطابق بزرگی و کوچکی حروف
    .accesskey = ط
    .tooltiptext = جست‌وجو با تطبیق بزرگی و کوچکی حروف
findbar-entire-word =
    .label = تمام کلمات
    .accesskey = ت
    .tooltiptext = فقط جست‌وجو کلمه‌هایِ کامل
