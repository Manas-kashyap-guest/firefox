# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = Tysýi jejoko
    .style = width: 55em
blocklist-desc = Nde ikatu eiporavo { -brand-short-name } rysýi eipurútava ejoko hag̃ua Ñanduti mba'epuru ohapykuehokuaáva nekundaha rembiapo.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = Tysýi
blocklist-button-cancel =
    .label = Heja
    .accesskey = C
blocklist-button-ok =
    .label = Moambue ñongatu
    .accesskey = S
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = Ñemo'ã tuicha'ỹva Disconnect.me (Je'epyréva).
blocklist-item-moz-std-desc = Omoneĩ heta tapykueriguávape ñanduti renda omba'apóvo hekoitépe.
blocklist-item-moz-full-name = Disconnect.me ñemo'ã ha'etéva.
blocklist-item-moz-full-desc = Tapykuerigua eikuaáva jejoko. Heta tenda ikatu nomba'apoporãmbái.
