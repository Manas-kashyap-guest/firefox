# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ਹੁਣੇ ਕੋਸ਼ਿਸ਼ ਕਰੋ
onboarding-button-label-get-started = ਸ਼ੁਰੂ ਕਰੀਏ
onboarding-welcome-header = { -brand-short-name } ਵਲੋਂ ਜੀ ਆਇਆਂ ਨੂੰ
onboarding-start-browsing-button-label = ਬਰਾਊਜ਼ ਕਰਨਾ ਸ਼ੁਰੂ ਕਰੋ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = ਪ੍ਰਾਈਵੇਟ ਬਰਾਊਜ਼ਿੰਗ
onboarding-screenshots-title = ਸਕਰੀਨਸ਼ਾਟ
onboarding-addons-title = ਐਡ-ਆਨ
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = ਸਿੰਕ ਕਰੋ
