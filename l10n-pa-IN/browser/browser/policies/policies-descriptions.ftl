# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-BlockAboutConfig = about:config ਸਫ਼ੇ ਲਈ ਪਹੁੰਚ ਤੇ ਪਾਬੰਦੀ ਲਗਾਓ।
policy-BlockAboutProfiles = about:profiles ਸਫ਼ੇ ਲਈ ਪਹੁੰਚ ਤੇ ਪਾਬੰਦੀ ਲਗਾਓ।
policy-CertificatesDescription = ਸਰਟੀਫਿਕੇਟ ਜੋੜੋ ਜਾਂ ਵਿਚੇ ਮੌਜੂਦ ਸਰਟੀਫਿਕੇਟ ਵਰਤੋਂ।
policy-Cookies = ਵੈੱਬਸਾਈਟਾਂ ਨੂੰ ਕੂਕੀਜ਼ ਸੈੱਟ ਕਰਨ ਦੀ ਇਜਾਜ਼ਤ ਦਿਓ ਜਾਂ ਪਾਬੰਦੀ ਲਗਾਓ।
policy-DisableAppUpdate = ਬਰਾਊਜ਼ਰ ਨੂੰ ਅੱਪਡੇਟ ਹੋਣ ਤੋਂ ਰੋਕੋ।
policy-DisableBuiltinPDFViewer = PDF.js, { -brand-short-name } ਵਿੱਚ ਮੌਜੂਦ PDF ਦਰਸ਼ਕ, ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ।
policy-DisableDeveloperTools = ਡਿਵੈਲਪਰ ਟੂਲਾਂ ਲਈ ਪਹੁੰਚ ਵਾਸਤੇ ਪਾਬੰਦੀ ਲਗਾਓ।
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = ਫਾਇਰਫਾਕਸ ਸਕਰੀਨਸ਼ਾਟ ਫੀਚਰ ਨੂੰ ਅਸਮਰੱਥ ਕਰੋ।
policy-DisableFormHistory = ਖੋਜ ਅਤੇ ਫਾਰਮ ਅਤੀਤ ਨੂੰ ਯਾਦ ਨਾ ਰੱਖੋ।
policy-DisableMasterPasswordCreation = ਜੇ ਸੱਚ ਹੈ ਤਾਂ ਮਾਸਟਰ ਪਾਸਵਰਡ ਨਹੀਂ ਬਣਾਇਆ ਜਾ ਸਕਦਾ ਹੈ।
policy-DisableTelemetry = ਟੈਲੀਮੈਂਟਰੀ ਬੰਦ ਕਰੋ।
policy-FlashPlugin = ਫਲੈਸ਼ ਪਲੱਗਇਨ ਦੀ ਵਰਤੋਂ ਦੀ ਇਜਾਜ਼ਤ ਦਿਓ ਜਾਂ ਪਾਬੰਦੀ ਲਗਾਓ।
