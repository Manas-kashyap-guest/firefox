# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = ਮੁੜ-ਸ਼ੁਰੂ ਕਰਨਾ ਜਰੂਰੀ ਹੈ
restart-required-header = ਅਫ਼ਸੋਸ ਹੈ। ਸਾਨੂੰ ਜਾਰੀ ਰੱਖਣ ਵਾਸਤੇ ਇੱਕ ਨਿੱਕਾ ਜਿਹਾ ਕੰਮ ਕਰਨਾ ਪਵੇਗਾ।
restart-button-label = { -brand-short-name } ਮੁੜ-ਚਾਲੂ ਕਰੋ
