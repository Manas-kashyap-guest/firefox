<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : FILE This file contains the Toolbox strings -->
<!-- LOCALIZATION NOTE : FILE Do not translate key -->


<!-- LOCALIZATION NOTE (browserToolboxErrorMessage): This is the label
  -  shown next to error details when the Browser Toolbox is unable to open. -->

<!-- LOCALIZATION NOTE (browserToolboxStatusMessage): This is the label
  -  shown next to status details when the Browser Toolbox fails to connect or
  -  appears to be taking a while to do so. -->

<!-- LOCALIZATION NOTE (options.context.advancedSettings): This is the label for
  -  the heading of the advanced settings group in the options panel. -->

<!-- LOCALIZATION NOTE (options.context.inspector): This is the label for
  -  the heading of the Inspector group in the options panel. -->

<!-- LOCALIZATION NOTE (options.showUserAgentStyles.label): This is the label
  -  for the checkbox option to show user agent styles in the Inspector
  -  panel. -->

<!-- LOCALIZATION NOTE (options.collapseAttrs.label): This is the label
  -  for the checkbox option to enable collapse attributes in the Inspector
  -  panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.label): This is the label for a
  -  dropdown list that controls the default color unit used in the inspector.
  -  This label is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.accesskey): This is the access
  -  key for a dropdown list that controls the default color unit used in the
  -  inspector. This is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.authored): This is used in the
  -  'Default color unit' dropdown list and is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.hex): This is used in the
  -  'Default color unit' dropdown list and is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.hsl): This is used in the
  -  'Default color unit' dropdown list and is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.rgb): This is used in the
  -  'Default color unit' dropdown list and is visible in the options panel. -->

<!-- LOCALIZATION NOTE (options.defaultColorUnit.name): This is used in
  -  the 'Default color unit' dropdown list and is visible in the options panel.
  -  -->

<!-- LOCALIZATION NOTE (options.context.triggersPageRefresh): This is the
  -  triggers page refresh footnote under the advanced settings group in the
  -  options panel and is used for settings that trigger page reload. -->

<!-- LOCALIZATION NOTE (options.enableChrome.label5): This is the label for the
  -  checkbox that toggles chrome debugging, i.e. devtools.chrome.enabled
  -  boolean preference in about:config, in the options panel. -->

<!-- LOCALIZATION NOTE (options.enableRemote.label3): This is the label for the
  -  checkbox that toggles remote debugging, i.e. devtools.debugger.remote-enabled
  -  boolean preference in about:config, in the options panel. -->

<!-- LOCALIZATION NOTE (options.disableJavaScript.label,
  -  options.disableJavaScript.tooltip): This is the options panel label and
  -  tooltip for the checkbox that toggles JavaScript on or off. -->

<!-- LOCALIZATION NOTE (options.disableHTTPCache.label,
  -  options.disableHTTPCache.tooltip): This is the options panel label and
  -  tooltip for the checkbox that toggles the HTTP cache on or off. -->

<!-- LOCALIZATION NOTE (options.enableServiceWorkersHTTP.label,
  -  options.enableServiceWorkersHTTP.tooltip): This is the options panel label and
  -  tooltip for the checkbox that toggles the service workers testing features on or off. This option enables service workers over HTTP. -->

<!-- LOCALIZATION NOTE (options.selectDefaultTools.label2): This is the label for
  -  the heading of group of checkboxes corresponding to the default developer
  -  tools. -->

<!-- LOCALIZATION NOTE (options.selectAdditionalTools.label): This is the label for
  -  the heading of group of checkboxes corresponding to the developer tools
  -  added by add-ons. This heading is hidden when there is no developer tool
  -  installed by add-ons. -->

<!-- LOCALIZATION NOTE (options.selectEnabledToolboxButtons.label): This is the label for
  -  the heading of group of checkboxes corresponding to the default developer
  -  tool buttons. -->

<!-- LOCALIZATION NOTE (options.toolNotSupported.label): This is the label for
  -  the explanation of the * marker on a tool which is currently not supported
  -  for the target of the toolbox. -->

<!-- LOCALIZATION NOTE (options.selectDevToolsTheme.label2): This is the label for
  -  the heading of the radiobox corresponding to the theme of the developer
  -  tools. -->

<!-- LOCALIZATION NOTE (options.usedeveditiontheme.*) Options under the
  -  toolbox for enabling and disabling the Developer Edition browser theme. -->

<!-- LOCALIZATION NOTE (options.webconsole.label): This is the label for the
  -  heading of the group of Web Console preferences in the options panel. -->

<!-- LOCALIZATION NOTE (options.timestampMessages.label): This is the
   - label for the checkbox that toggles timestamps in the Web Console -->

<!-- LOCALIZATION NOTE (options.debugger.label): This is the label for the
  -  heading of the group of Debugger preferences in the options panel. -->

<!-- LOCALIZATION NOTE (options.sourceMaps.label): This is the
   - label for the checkbox that toggles source maps in all tools -->

<!-- LOCALIZATION NOTE (options.styleeditor.label): This is the label for the
  -  heading of the group of Style Editor preferences in the options
  -  panel. -->

<!-- LOCALIZATION NOTE (options.stylesheetAutocompletion.label): This is the
   - label for the checkbox that toggles autocompletion of css in the Style Editor -->

<!-- LOCALIZATION NOTE (options.screenshot.label): This is the label for the
   -  heading of the group of Screenshot preferences in the options
   -  panel. -->
<!ENTITY options.screenshot.label            "ਸਕਰੀਨਸ਼ਾਟ ਰਵੱਈਆ">

<!-- LOCALIZATION NOTE (options.screenshot.clipboard.label): This is the
   - label for the checkbox that toggles screenshot to clipboard feature. -->
<!ENTITY options.screenshot.clipboard.label      "ਸਕਰੀਨਸ਼ਾਟ ਕਲਿੱਪਬੋਰਡ 'ਚ ਲਿਆ">
<!ENTITY options.screenshot.clipboard.tooltip    "ਸਕਰੀਨਸ਼ਾਟ ਨੂੰ ਸਿੱਧੇ ਕਲਿੱਪਬੋਰਡ 'ਚ ਸੰਭਾਲਦਾ ਹੈ">

<!-- LOCALIZATION NOTE (options.screenshot.audio.label): This is the
   - label for the checkbox that toggles the camera shutter audio for screenshot tool -->
<!ENTITY options.screenshot.audio.label      "ਕੈਮਰਾ ਸ਼ਟਰ ਆਵਾਜ਼ ਚਲਾਓ">
<!ENTITY options.screenshot.audio.tooltip    "ਜਦੋਂ ਸਕਰੀਨਸ਼ਾਟ ਲੈਣਾ ਹੋਵੇ ਤਾਂ ਕੈਮਰਾ ਆਡੀਓ ਆਵਾਜ਼ ਸਮਰੱਥ ਕਰਦਾ ਹੈ">

<!-- LOCALIZATION NOTE (options.showPlatformData.label): This is the
  -  label for the checkbox that toggles the display of the platform data in the,
  -  Profiler i.e. devtools.profiler.ui.show-platform-data a boolean preference
  -  in about:config, in the options panel. -->

<!-- LOCALIZATION NOTE (options.sourceeditor.*): Options under the editor
  -  section. -->

