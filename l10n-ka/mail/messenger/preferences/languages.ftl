# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = აწევა
    .accesskey = ა
languages-customize-movedown =
    .label = ჩამოწევა
    .accesskey = მ
languages-customize-remove =
    .label = მოცილება
    .accesskey = მ
languages-customize-select-language =
    .placeholder = ენის არჩევა დასამატებლად...
languages-customize-add =
    .label = დამატება
    .accesskey = დ
messenger-languages-window =
    .title = { -brand-short-name }-ის ენის პარამეტრები
    .style = width: 40em
messenger-languages-description = { -brand-short-name } აჩვენებს თქვენ მიერ მითითებულ პირველ ენას ნაგულისხმევად, ხოლო საჭიროების შემთხვევაში დამატებითს, რიგითობის შესაბამისად.
