# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Neustart erforderlich
restart-required-header = Entschuldigung für die Störung
restart-required-intro = Wir haben gerade ein Update im Hintergrund installiert. Klicken Sie auf { -brand-short-name } neu starten, um es abzuschließen.
restart-required-description = Wir werden danach all Ihre Seiten, Fenster und Tabs wiederherstellen, damit Sie schnell weitermachen können.
restart-button-label = { -brand-short-name } neu starten
