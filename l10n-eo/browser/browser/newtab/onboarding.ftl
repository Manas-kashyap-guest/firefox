# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Provu ĝin nun
onboarding-button-label-get-started = Unuaj paŝoj
onboarding-welcome-header = Bonvenon al { -brand-short-name }
onboarding-start-browsing-button-label = Komenci retumi

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Privata retumo
onboarding-private-browsing-text = Retumu sola. Privata retumo kun blokado de enhavo estas trajto, kiu blokas retajn spurilojn, kiuj sekvas vin tra la reto.
onboarding-screenshots-title = Ekrankopioj
onboarding-screenshots-text = Faru, konservu kaj dividu ekrankopiojn sen forlasi { -brand-short-name }. Dum vi retumas, kaptu parton aŭ tutan paĝon. Poste konservu ĝin en la reto por facila aliro kaj divido.
onboarding-addons-title = Aldonaĵoj
onboarding-addons-text = Aldonu eĉ pli da trajtoj, kiuj igas { -brand-short-name } labori pli por vi. Komparu prezojn, kontrolu la veterprognozon aŭ esprimu vin per personecigita etoso.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Retumu pli rapide, pli inteligente aŭ pli sekure per etendaĵoj kiel Ghostery, kiu permesas al vi bloki ĝenajn reklamojn.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Spegulado
onboarding-fxa-text = Registriĝu ĉe { -fxaccount-brand-name } por speguli viajn legosignojn, pasvortojn kaj malfermitajn langetojn ĉie, kie vi uzas { -brand-short-name }.
