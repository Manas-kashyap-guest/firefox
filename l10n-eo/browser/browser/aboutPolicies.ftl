# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Entreprenaj politikoj
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktiva
errors-tab = Eraroj
documentation-tab = Dokumentaro
policy-name = Nomo de politiko
policy-value = Valoro de politiko
policy-errors = Eraroj de politiko
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = Se vi uzas grupan politikon, tiu ĉi politiko nur povas esti difinita je la nivelo de la komputilo.
