# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = ആഡ്-ഓണ്‍ മാനേജര്‍
search-header-shortcut =
    .key = f
loading-label =
    .value = ലഭ്യമാക്കുന്നു…
list-empty-installed =
    .value = ഈ രീതിയിലുള്ള ആഡ്-ഓണുകള്‍ നിങ്ങള്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്തിട്ടില്ല
list-empty-available-updates =
    .value = പരിഷ്കരണങ്ങള്‍ ലഭ്യമല്ല
list-empty-recent-updates =
    .value = അടുത്തിടയ്ക്കു് ആഡ്-ഓണുകള്‍ പരിഷ്കരിച്ചിട്ടില്ല
list-empty-find-updates =
    .label = പരിഷ്കരണങ്ങള്‍ക്കായി പരിശോധിയ്ക്കുക
list-empty-button =
    .label = ആഡ്-ഓണുകളെപ്പറ്റി കൂടുതല്‍ അറിയുക
install-addon-from-file =
    .label = ഫയലില്‍ നിന്നും ആഡ്-ഓണ്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്യുക…
    .accesskey = I
tools-menu =
    .tooltiptext = എല്ലാ ആഡ്-ഓണുകള്‍ക്കുമുള്ള പ്രയോഗങ്ങള്‍
show-unsigned-extensions-button =
    .label = ചില എക്റ്റെന്‍ഷനുകള്‍ പരിശോധിക്കാന്‍ പറ്റുന്നില്ല
show-all-extensions-button =
    .label = എല്ലാ എക്സ്റ്റെന്‍ഷനും കാണിക്കുക
debug-addons =
    .label = ആഡ്-ഓണുകള്‍ ഡീബഗ് ചെയ്യുക
    .accesskey = B
cmd-show-details =
    .label = കൂടുതല്‍ വിവരങ്ങള്‍ കാണിയ്ക്കുക
    .accesskey = S
cmd-find-updates =
    .label = പരിഷ്കരണങ്ങള്‍ കണ്ടെത്തുക
    .accesskey = F
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] ഐച്ഛികങ്ങള്‍
           *[other] മുന്‍ഗണനകള്‍
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
cmd-enable-theme =
    .label = ഥീം സ്വീകരിയ്ക്കുക
    .accesskey = W
cmd-disable-theme =
    .label = ഥീം ഉപയോഗം നിര്‍ത്തുക
    .accesskey = W
cmd-install-addon =
    .label = ഇന്‍സ്റ്റോള്‍ ചെയ്യുക
    .accesskey = I
cmd-contribute =
    .label = സംഭാവന ചെയ്യുക
    .accesskey = C
    .tooltiptext = ഈ ആഡ്-ഓണിന്റെ പുരോഗതിയ്ക്കായി സംഭാവന ചെയ്യുക
discover-title = എന്താണു് ആഡ്-ഓണുകള്‍?
discover-description = നിങ്ങളുടെ ഇഷ്ടാനുസൃതം { -brand-short-name } -നു് കൂടുതല്‍ പ്രവര്‍ത്തനശേഷിയും ശൈലിയും ലഭ്യമാക്കുന്ന പ്രയോഗങ്ങളാണു് ആഡ്-ഓണുകള്‍. { -brand-short-name } നിങ്ങളുടെ ഇഷ്ടമുള്ളത് പോലെ ആക്കാന്‍ ഒരു കാലാവസ്ഥാ അറിയിപ്പോ, തീം മാറ്റുകയോ ചെയ്ത് നോക്കു.
discover-footer = ഇന്റര്‍നെറ്റ് കണക്ഷനുള്ളപ്പോള്‍, ഈ പെയിനില്‍ ഏറ്റവും  ഉത്തമമായ ആഡ്-ഓണുകള്‍ നിങ്ങള്‍ക്കു് കാണാം.
detail-version =
    .label = പതിപ്പു്
detail-last-updated =
    .label = ഏറ്റവും ഒടുവില്‍ പുതുക്കിയതു്
detail-contributions-description = ചെറിയ സംഭാവനകള്‍ നല്‍കി ഈ ആഡ്-ഓണിനുള്ള പുരോഗമനത്തിനായി സഹായിയ്ക്കുവാന്‍ ഡവലപ്പര്‍ അഭ്യര്‍ത്ഥിയ്ക്കുന്നു.
detail-update-type =
    .value = ഓട്ടോമാറ്റിക്ക് പരിഷ്കരണങ്ങള്‍
detail-update-default =
    .label = സ്വതവേ
    .tooltiptext = സ്വതവേയുള്ളതാണെങ്കില്‍ മാത്രം പരിഷ്കരണങ്ങള്‍ ഓട്ടോമാറ്റിക്കായി ഇന്‍സ്റ്റോള്‍ ചെയ്യുക
detail-update-automatic =
    .label = ഓണ്‍
    .tooltiptext = പരിഷ്കരണങ്ങള്‍ ഓട്ടോമാറ്റിക്കായി ഇന്‍സ്റ്റോള്‍ ചെയ്യുക
detail-update-manual =
    .label = ഓഫ്
    .tooltiptext = പരിഷ്കരണങ്ങള്‍ ഓട്ടോമാറ്റിക്കായി ഇന്‍സ്റ്റോള്‍ ചെയ്യരുതു്
detail-home =
    .label = ആസ്ഥാനതാള്‍
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = ആഡ്-ഓണ്‍ പ്രൊഫൈല്‍
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = പരിഷ്കരണങ്ങള്‍ക്കായി പരിശോധിയ്ക്കുക
    .accesskey = f
    .tooltiptext = ഈ ആഡ്-ഓണിനുള്ള പരിഷ്കരണങ്ങള്‍ക്കായി പരിശോധിയ്ക്കുക
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] ഐച്ഛികങ്ങള്‍
           *[other] മുന്‍ഗണനകള്‍
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] ഈ ആഡ്-ഓണിനുള്ള ഐച്ഛികങ്ങള്‍ മാറ്റുക
           *[other] ഈ ആഡ്-ഓണിനുള്ള മുന്‍ഗണനകള്‍ മാറ്റുക
        }
detail-rating =
    .value = റേറ്റിങ്
addon-restart-now =
    .label = ഉടന്‍ വീണ്ടും ആരംഭിയ്ക്കുക
disabled-unsigned-heading =
    .value = ചില ആഡോണുകള്‍ പ്രവര്‍ത്തനരഹിതമാക്കി
disabled-unsigned-description = ഈ ആഡോണുകൾ { -brand-short-name } ൽ ഉപയോഗിക്കാമെന്ന് ഇതുവരെ ഉറപ്പുവരുത്തിയിട്ടില്ല. നിങ്ങൾക്ക് <label data-l10n-name="find-addons">പകരം വെക്കാനാവുന്നവ കണ്ടെത്താം</label> അല്ലെങ്കിൽ ഡെവലപ്പറോട് അവ വെരിഫൈ ചെയ്യാൻ ആവശ്യപ്പെടാം.
disabled-unsigned-learn-more = ഓൺലൈനിൽ താങ്കൾ കൂടുതൽ സുരക്ഷിതനായിരിക്കാൻ സഹായിക്കുന്നതിനായുള്ള ഞങ്ങളുടെ ശ്രമങ്ങളെക്കുറിച്ച് കൂടുതൽ മനസിലാക്കുക.
disabled-unsigned-devinfo = തങ്ങളുടെ ആഡോണുകൾ വെരിഫൈ ചെയ്യുന്നതിൽ തല്പരരായ ഡെവലപ്പർമാർക്ക് ഞങ്ങളുടെ<label data-l10n-name="learn-more">മാന്വൽ വായിക്കുന്നത് തുടരാവുന്നതാണ്</label>.
plugin-deprecation-description = എന്തെങ്കിലും നഷ്ടമായോ? ചില പ്ലഗിന്നുകൾ ഇനിമുതൽ { -brand-short-name } പിന്തുണയ്ക്കില്ല. <label data-l10n-name="learn-more">കൂടുതല്‍ അറിയുക.</label>
legacy-warning-show-legacy = പഴയ എക്സ്റ്റെന്‍ഷനും കാണിക്കുക
legacy-extensions-description = ഈ എക്സ്റ്റൻഷെനുകൾ നിലവിലെ { -brand-short-name } മാനദണ്ഡങ്ങൾ പാലിക്കുന്നുല്ല, ആയതിനാൽ അവ നിർജ്ജീവമാക്കിയിട്ടുണ്ട്. <label data-l10n-name="legacy-learn-more">ആഡ്-ഓണുകളിലെ മാറ്റങ്ങളെക്കുറിച്ച് അറിയുക</label>
extensions-view-discover =
    .name = ആഡ്-ഓണുകള്‍ ലഭ്യമാകുക
    .tooltiptext = { extensions-view-discover.name }
extensions-view-recent-updates =
    .name = ഏറ്റവും പുതിയ പരിഷ്കരണങ്ങള്‍
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = ലഭ്യമായ പരിഷ്കരണങ്ങള്‍
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = എല്ലാ ആഡ്-ഓണുകളും സേഫ് മോഡില്‍ പ്രവര്‍ത്തന രഹിതമാക്കിയിരിയ്ക്കുന്നു.
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = ആഡ്-ഓണ്‍ പൊരുത്തം പരിശോധന പ്രവര്‍ത്തന രഹിതമാക്കിയിരിയ്ക്കുന്നു. പൊരുത്തപ്പെടാത്ത ആഡ്-ഓണുകളുണ്ടാവാം.
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = പ്രവര്‍ത്തന സജ്ജം
    .tooltiptext = ആഡ്-ഓണ്‍ പൊരുത്തം പരിശോധന പ്രവര്‍ത്തന സജ്ജമാക്കുക
extensions-warning-update-security-label =
    .value = ആഡ്-ഓണ്‍ പരിഷ്കരണ സുരക്ഷാ പരിശോധന പ്രവര്‍ത്തന രഹിതമാക്കിയിരിയ്ക്കുന്നു. ഉചിതമായ പരിഷ്കരണങ്ങള്‍ ലഭ്യമായില്ല.
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = പ്രവര്‍ത്തന സജ്ജം
    .tooltiptext = ആഡ്-ഓണ്‍ പരിഷ്കരണ സുരക്ഷാ പരിശോധന പ്രവര്‍ത്തന സജ്ജമാക്കുക

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = പരിഷ്കരണങ്ങള്‍ക്കായി പരിശോധിയ്ക്കുക
    .accesskey = C
extensions-updates-view-updates =
    .label = ഏറ്റവും പുതിയ പരിഷ്കരങ്ങള്‍ കാണുക
    .accesskey = V

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = ഓട്ടോമാറ്റിക്കായി ആഡ്-ഓണുകള്‍ പരിഷ്കരിയ്ക്കുക
    .accesskey = A

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = ഓട്ടോമാറ്റിക്കായി പരിഷ്കരിയ്ക്കുന്നതിനു് എല്ലാ ആഡ്-ഓണുകളും വീണ്ടും സജ്ജമാക്കുക
    .accesskey = R
extensions-updates-reset-updates-to-manual =
    .label = മാനുവലായി പരിഷ്കരിയ്ക്കുന്നതിനായി എല്ലാ ആഡ്-ഓണുകളും വീണ്ടും സജ്ജമാക്കുക
    .accesskey = R

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = ആഡ്-ഓണുകള്‍ പരിഷ്കരിയ്ക്കുന്നു
extensions-updates-installed =
    .value = നിങ്ങളുടെ ആഡ്-ഓണുകള്‍ പരിഷ്കരിച്ചിരിയ്ക്കുന്നു.
extensions-updates-downloaded =
    .value = നിങ്ങളുടെ ആഡ്-ഓണുകള്‍ക്കുള്ള പരിഷ്കരണം ഡൌണ്‍ലോഡ് ചെയ്തിരിയ്ക്കുന്നു.
extensions-updates-restart =
    .label = ഇന്‍സ്റ്റലേഷന്‍ പൂര്‍ത്തിയാക്കുന്നതിനായി വീണ്ടും ആരംഭിയ്ക്കുക
extensions-updates-none-found =
    .value = പരിഷ്കരണങ്ങള്‍ ലഭ്യമല്ല
extensions-updates-manual-updates-found =
    .label = ലഭ്യമായ പരിഷ്കരണങ്ങള്‍ കാണുക
extensions-updates-update-selected =
    .label = പരിഷ്കരണങ്ങള്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്യുക
    .tooltiptext = പട്ടികയില്‍ ലഭ്യമായ പരിഷ്കരണങ്ങള്‍ ഇന്‍സ്റ്റോള്‍ ചെയ്യുക
