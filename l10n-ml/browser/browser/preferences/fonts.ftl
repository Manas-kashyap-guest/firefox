# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fonts-window =
    .title = ഫോണ്ടുകള്‍
fonts-window-close =
    .key = w

## Font groups by language

fonts-langgroup-header = നുള്ള ഫോണ്ടുകൾ
    .accesskey = F
fonts-langgroup-arabic =
    .label = അറബിക്‌
fonts-langgroup-armenian =
    .label = അര്‍മെനിയന്‍
fonts-langgroup-bengali =
    .label = ബംഗാളി
fonts-langgroup-simpl-chinese =
    .label = ലളിതമായ ചൈനീസ്‌
fonts-langgroup-trad-chinese-hk =
    .label = സമ്പ്രദായ ചൈനീസ്‌ (ഹോങ് കോങ്)
fonts-langgroup-trad-chinese =
    .label = സമ്പ്രദായ ചൈനീസ്‌ (തായ്‌വാന്‍)
fonts-langgroup-cyrillic =
    .label = സിറില്ലിക്ക്‌
fonts-langgroup-devanagari =
    .label = ദേവനഗരി
fonts-langgroup-ethiopic =
    .label = ഇത്യോപ്പിക്ക്‌
fonts-langgroup-georgian =
    .label = ജോര്‍ജ്ജിയന്‍
fonts-langgroup-el =
    .label = ഗ്രീക്ക്‌
fonts-langgroup-gujarati =
    .label = ഗുജറാത്തി
fonts-langgroup-gurmukhi =
    .label = ഗുര്‍മുഖി
fonts-langgroup-japanese =
    .label = ജാപ്പനീസ്‌
fonts-langgroup-hebrew =
    .label = ഹീബ്രു
fonts-langgroup-kannada =
    .label = കന്നഡാ
fonts-langgroup-khmer =
    .label = ഖെമെര്‍
fonts-langgroup-korean =
    .label = കൊറിയന്‍
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = ലാറ്റിന്‍
fonts-langgroup-malayalam =
    .label = മലയാളം
fonts-langgroup-math =
    .label = ഗണിതം
fonts-langgroup-odia =
    .label = ഒഡിയ
fonts-langgroup-sinhala =
    .label = സിന്‍ഹാലാ
fonts-langgroup-tamil =
    .label = തമിഴ്‌
fonts-langgroup-telugu =
    .label = തെലുങ്കു
fonts-langgroup-thai =
    .label = തായ്‌
fonts-langgroup-tibetan =
    .label = ടിബറ്റന്‍
fonts-langgroup-canadian =
    .label = യൂണിഫൈഡ്‌ കാനേഡിയന്‍ സില്ലബറി
fonts-langgroup-other =
    .label = മറ്റ് എഴുത്തുരീതികൾ

## Default fonts and their sizes

fonts-proportional-header = ആനുപാതികം
    .accesskey = P
fonts-default-serif =
    .label = സെറിഫ്
fonts-default-sans-serif =
    .label = സാന്‍സ് സെറിഫ്
fonts-proportional-size = വലുപ്പം
    .accesskey = z
fonts-serif = സെരിഫ്
    .accesskey = S
fonts-sans-serif = സാന്‍സ്-സെരിഫ്
    .accesskey = n
fonts-monospace = മോണോസ്പേസ്
    .accesskey = M
fonts-monospace-size = വലുപ്പം
    .accesskey = e
fonts-minsize = ഏറ്റവും കുറഞ്ഞ ഫോണ്ട് വലുപ്പം
    .accesskey = o
fonts-minsize-none =
    .label = ഒന്നുമില്ല
fonts-allow-own =
    .label = നിങ്ങളുടെ തിരഞ്ഞെടുക്കലിനു പകരം, സ്വന്തം ഫോണ്ടുകൾ തിരഞ്ഞെടുക്കാൻ പേജുകളെ അനുവദിക്കുക
    .accesskey = A

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-header = പരമ്പരാഗത ഉള്ളടക്കത്തിനുള്ള ടെക്സ്റ്റ്‌ എന്‍കോഡിങ്
fonts-languages-fallback-desc = എൻകോഡിങ് രേഖപ്പെടുത്തിയിട്ടില്ലാത്ത പഴയ ഉള്ളടക്കങ്ങൾക്ക് ഈ എൻകോഡിങ് ഉപയോഗിക്കുന്നു.
fonts-languages-fallback-label = ഫോൾബാക്ക് ടെക്സ്റ്റ് എൻകോഡിംഗ്
    .accesskey = T
fonts-languages-fallback-name-auto =
    .label = നിലവിലുള്ള ലോക്കേലിനു് സ്വതവേയുള്ളതു്
fonts-languages-fallback-name-arabic =
    .label = അറബിക്‌
fonts-languages-fallback-name-baltic =
    .label = ബാള്‍ട്ടിക്ക്‌
fonts-languages-fallback-name-ceiso =
    .label = മദ്ധ്യ യൂറോപ്പിയന്‍, ഐഎസ്ഒ
fonts-languages-fallback-name-cewindows =
    .label = മദ്ധ്യ യൂറോപ്പിയന്‍, മൈക്രോസോഫ്ട്
fonts-languages-fallback-name-simplified =
    .label = ചൈനീസ്, ലളിതമായ
fonts-languages-fallback-name-traditional =
    .label = ചൈനീസ്, പരമ്പരാഗത
fonts-languages-fallback-name-cyrillic =
    .label = സിറില്ലിക്ക്‌
fonts-languages-fallback-name-greek =
    .label = ഗ്രീക്ക്‌
fonts-languages-fallback-name-hebrew =
    .label = ഹീബ്രു
fonts-languages-fallback-name-japanese =
    .label = ജാപ്പനീസ്‌
fonts-languages-fallback-name-korean =
    .label = കൊറിയന്‍
fonts-languages-fallback-name-thai =
    .label = തായ്‌
fonts-languages-fallback-name-turkish =
    .label = തുര്‍ക്കിഷ്‌
fonts-languages-fallback-name-vietnamese =
    .label = വിയറ്റ്നമീസ്‌
fonts-languages-fallback-name-other =
    .label = മറ്റുള്ളവ (പടിഞ്ഞാറന്‍ യൂറോപ്പിയന്‍ ഉള്‍പ്പടെ)
fonts-very-large-warning-title = വലിയ മിനിമം ഫോണ്ട് സൈസ്
fonts-very-large-warning-message = നിങ്ങള്‍ വളരെ കൂടിയ ഫോണ്ട് സൈസ് ആണ്ഏറ്റവും ചെറിയതായി തെരഞ്ഞെടുത്തിരിയ്ക്കുന്നത് (24 പിക്സലുകളേക്കാള്‍ കൂടുതല്‍). അവ ഇത് പോലെ ചില പ്രധാന ക്രമീകരണ താളുകളില്‍ ഉപയോഗിയ്ക്കുവാന്‍ വളരെ ബുദ്ധിമുട്ടുണ്ടാക്കുന്നു.
fonts-very-large-warning-accept = എന്റെ മാറ്റങ്ങള്‍ എന്തായാലും വെച്ചേക്കു
# Variables:
#   $name {string, "Arial"} - Name of the default font
fonts-label-default =
    .label = ഡിഫോള്‍ട്ട്‌ ({ $name })
fonts-label-default-unnamed =
    .label = ഡിഫള്‍ട്ട്
