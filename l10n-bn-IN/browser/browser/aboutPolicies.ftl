# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = এন্টারপ্রাইজ পলিসি
# 'Active' is used to describe the policies that are currently active
active-policies-tab = সক্রিয়
errors-tab = ত্রুটি
documentation-tab = নিবন্ধীকরণ
policy-name = পলিসির নাম
policy-value = পলিসির মূল্য
policy-errors = পলিসির ত্রুটি
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = যখন দলের পলিসির ব্যবহার করা হবে, তখন এই নীতিটি শুধুমাত্র কম্পিউটার পর্যায়ে সেট করা যেতে পারে।
