# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fonts-window =
    .title = ফন্ট
fonts-window-close =
    .key = w

## Font groups by language

fonts-langgroup-header = ফন্ট হল
    .accesskey = F
fonts-langgroup-arabic =
    .label = আরবি
fonts-langgroup-armenian =
    .label = আর্মেনিয়ান
fonts-langgroup-bengali =
    .label = বাংলা
fonts-langgroup-simpl-chinese =
    .label = সরলীকৃত চীনা
fonts-langgroup-trad-chinese-hk =
    .label = পারম্পরিক চীনা (হং কং)
fonts-langgroup-trad-chinese =
    .label = পারম্পরিক চীনা (তাইওয়ান)
fonts-langgroup-cyrillic =
    .label = সিরিলিক
fonts-langgroup-devanagari =
    .label = দেবনাগরী
fonts-langgroup-ethiopic =
    .label = ইথিওপিক
fonts-langgroup-georgian =
    .label = জর্জিয়ান
fonts-langgroup-el =
    .label = গ্রিক
fonts-langgroup-gujarati =
    .label = গুজরাতি
fonts-langgroup-gurmukhi =
    .label = গুরুমুখি
fonts-langgroup-japanese =
    .label = জাপানি
fonts-langgroup-hebrew =
    .label = হিব্রু
fonts-langgroup-kannada =
    .label = কন্নড়
fonts-langgroup-khmer =
    .label = খমের
fonts-langgroup-korean =
    .label = কোরিয়ান
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = লাতিন
fonts-langgroup-malayalam =
    .label = মালায়ালম
fonts-langgroup-math =
    .label = গণিত
fonts-langgroup-odia =
    .label = ওড়িয়া
fonts-langgroup-sinhala =
    .label = সিংহালা
fonts-langgroup-tamil =
    .label = তামিল
fonts-langgroup-telugu =
    .label = তেলুগু
fonts-langgroup-thai =
    .label = থাই
fonts-langgroup-tibetan =
    .label = তিব্বতি
fonts-langgroup-canadian =
    .label = ইউনিফায়েড ক্যানাডিয়ান সিলেবারি
fonts-langgroup-other =
    .label = অন্যান্য লেখার সিস্টেম

## Default fonts and their sizes

fonts-proportional-header = আনুপাতিক
    .accesskey = P
fonts-default-serif =
    .label = সেরিফ (Serif)
fonts-default-sans-serif =
    .label = সান্স-সেরিফ (Sans Serif)
fonts-proportional-size = আকার
    .accesskey = z
fonts-serif = সেরিফ
    .accesskey = S
fonts-sans-serif = সান্স-সেরিফ
    .accesskey = n
fonts-monospace = মোনোস্পেস
    .accesskey = M
fonts-monospace-size = আকার
    .accesskey = e
fonts-minsize = ফন্টের সর্বনিম্ন আকার
    .accesskey = o
fonts-minsize-none =
    .label = শূণ্য
fonts-allow-own =
    .label = উপরে আপনার উল্লিখিত নির্বাচন উপেক্ষা করে পেজগুলির দ্বারা ব্যবহারযোগ্য ফন্ট নির্বাচনের অনুমতি প্রদান করা হবে
    .accesskey = A

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-header = লিগ্যাসি সামগ্রীর জন্য ক্যারেক্টার এনকোডিং
fonts-languages-fallback-desc = এই অক্ষর এনকোডিং সেই সমস্ত লিগ্যাসি বিষয়বস্তুর জন্য ব্যবহার করা হয় যেগুলি নিজেদের এনকোডিং সম্বন্ধিয় তথ্য ঘোষণা করতে ব্যর্থ হয়।
fonts-languages-fallback-label = ফলব্যাক টেক্সট এনকোডিং
    .accesskey = T
fonts-languages-fallback-name-auto =
    .label = বর্তমান ডিফল্ট লোকেল
fonts-languages-fallback-name-arabic =
    .label = আরবি
fonts-languages-fallback-name-baltic =
    .label = বল্টিক
fonts-languages-fallback-name-ceiso =
    .label = মধ্য ইউরোপিয়, ISO
fonts-languages-fallback-name-cewindows =
    .label = মধ্য ইউরোপিয়, Microsoft
fonts-languages-fallback-name-simplified =
    .label = সরলিকৃত চীনা
fonts-languages-fallback-name-traditional =
    .label = পারম্পরিক চীনা
fonts-languages-fallback-name-cyrillic =
    .label = সিরিলিক
fonts-languages-fallback-name-greek =
    .label = গ্রিক
fonts-languages-fallback-name-hebrew =
    .label = হিব্রু
fonts-languages-fallback-name-japanese =
    .label = জাপানি
fonts-languages-fallback-name-korean =
    .label = কোরিয়ান
fonts-languages-fallback-name-thai =
    .label = থাই
fonts-languages-fallback-name-turkish =
    .label = তুর্কি
fonts-languages-fallback-name-vietnamese =
    .label = ভিয়েতনামিস
fonts-languages-fallback-name-other =
    .label = অন্যান্য (incl. পশ্চিম ইউরোপীয়)
fonts-very-large-warning-title = বড় নূন্যতম ফন্টের পরিমাপ
fonts-very-large-warning-message = আপনি খুব বড়ো ফন্ট সাইজ নির্বাচন করেছেন (24 পিক্সেলের বেশি)। এটা হয়তো এই পেজের কিছু গুরুত্বপূর্ণ কনফিগারেশনকে অনেক কঠিন করে দিতে পারে।
fonts-very-large-warning-accept = আমার পরিবর্তনগুলো যেরম ভাবে হোক রাখুন
# Variables:
#   $name {string, "Arial"} - Name of the default font
fonts-label-default =
    .label = ডিফল্ট ({ $name })
fonts-label-default-unnamed =
    .label = ডিফল্ট
