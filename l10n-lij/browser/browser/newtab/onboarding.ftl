# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Preuvilo oua
onboarding-welcome-header = Benvegnuo in { -brand-short-name }
onboarding-start-browsing-button-label = Iniçia a navegâ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Navegaçion Privâ
onboarding-private-browsing-text = Navega pe i fæti teu. A Navegaçion privâ co- o blòcco di contegnui che tracian quello che ti fæ inta ræ.
onboarding-screenshots-title = Föto do schermo
onboarding-screenshots-text = Fanni, sarva e condividdi e föto do schermo - sensa sciortî da { -brand-short-name }. Catua 'na region ò 'na pagina intrega comme ti a veddi. Pöi sarvila inta ræ pe acedighe e condividila façilmente.
onboarding-addons-title = Conponenti azonti
onboarding-addons-text = Ancon ciù carateristiche che fan travagiâ { -brand-short-name } ancon de ciù pe ti. Confront prexi, contròlla o tenpo ò esprimi  teu personalitæ co 'n tema personalizou.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Navega veloce e in mòddo ciù furbo ò seguo con estenscioin comme Ghostery ch'o te blòcca e publicitæ che te ronpan.
