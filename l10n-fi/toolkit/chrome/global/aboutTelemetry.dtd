<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutTelemetry.pingDataSource "
Ping-datan lähde:
">
<!ENTITY aboutTelemetry.showCurrentPingData "
Tämänhetkinen ping-data
">
<!ENTITY aboutTelemetry.showArchivedPingData "
Arkistoitu ping-data
">
<!ENTITY aboutTelemetry.showSubsessionData "
Näytä ali-istuntodata
">
<!ENTITY aboutTelemetry.choosePing "
Valitse ping:
">
<!ENTITY aboutTelemetry.archivePingType "
Pingin tyyppi
">
<!ENTITY aboutTelemetry.archivePingHeader "
Ping
">
<!ENTITY aboutTelemetry.optionGroupToday "
Tänään
">
<!ENTITY aboutTelemetry.optionGroupYesterday "
Eilen
">
<!ENTITY aboutTelemetry.optionGroupOlder "
Vanhemmat
">
<!ENTITY aboutTelemetry.payloadChoiceHeader "
  Tiedot
">
<!-- LOCALIZATION NOTE(aboutTelemetry.previousPing, aboutTelemetry.nextPing):
	These strings are displayed when selecting Archived pings, and they’re
	used to move to the next or previous ping. -->
<!ENTITY aboutTelemetry.previousPing "&lt;&lt;">
<!ENTITY aboutTelemetry.nextPing "&gt;&gt;">

<!ENTITY aboutTelemetry.pageTitle "Kaukomittaustiedot">
<!ENTITY aboutTelemetry.moreInformations "
Etsitkö lisätietoa?
">
<!ENTITY aboutTelemetry.firefoxDataDoc "<a>Firefox Data Documentation</a> sisältää englanniksi oppaita datatyökalujen käytöstä.">
<!ENTITY aboutTelemetry.telemetryClientDoc "<a>Firefox Telemetry client documentation</a> sisältää englanniksi käsitteiden määritelmät, API-dokumentaation ja dataviittaukset.">
<!ENTITY aboutTelemetry.telemetryDashboard "<a>Kaukomittaustietojen kojelautojen</a> avulla voi visualisoida dataa, jota Mozilla vastaanottaa kaukomittauksen avulla.">

<!ENTITY aboutTelemetry.showInFirefoxJsonViewer "
Avaa JSON-katselimessa
">

<!ENTITY aboutTelemetry.homeSection "Etusivu">
<!ENTITY aboutTelemetry.generalDataSection "
  Yleistiedot
">
<!ENTITY aboutTelemetry.environmentDataSection "
  Ympäristön tiedot
">
<!ENTITY aboutTelemetry.sessionInfoSection "
  Istunnon tiedot
">
<!ENTITY aboutTelemetry.scalarsSection "
  Skaalariarvot
">
<!ENTITY aboutTelemetry.keyedScalarsSection "
  Avaimelliset skalaariarvot
">
<!ENTITY aboutTelemetry.histogramsSection "
  Histogrammit
">
<!ENTITY aboutTelemetry.keyedHistogramsSection "
  Histogrammit merkinnöistä
">
<!ENTITY aboutTelemetry.eventsSection "
  Tapahtumat
">
<!ENTITY aboutTelemetry.simpleMeasurementsSection "
  Yksinkertaiset mittaukset
">
<!ENTITY aboutTelemetry.telemetryLogSection "
  Kaukomittausloki
">
<!ENTITY aboutTelemetry.slowSqlSection "
  Hitaat SQL-lauseet
">
<!ENTITY aboutTelemetry.chromeHangsSection "
  Selaimen jäätymiset
">
<!ENTITY aboutTelemetry.addonDetailsSection "
  Lisäosien tiedot
">
<!ENTITY aboutTelemetry.capturedStacksSection "
  Kaapatut pinot
">
<!ENTITY aboutTelemetry.lateWritesSection "
  Myöhästyneet kirjoitukset
">
<!ENTITY aboutTelemetry.rawPayloadSection "
Raakatiedot
">
<!ENTITY aboutTelemetry.raw "
Muotoilematon JSON
">

<!ENTITY aboutTelemetry.fullSqlWarning "
  Huom.: Hidas SQL-virheenetsintä on päällä. Täydelliset SQL-lausekkeet saattavat näkyä alla, mutta niitä ei lähetetä mittaustietona.
">
<!ENTITY aboutTelemetry.fetchStackSymbols "
  Nouda funktioiden nimet pinoihin
">
<!ENTITY aboutTelemetry.hideStackSymbols "
  Näytä muotoilematon pinodata
">
