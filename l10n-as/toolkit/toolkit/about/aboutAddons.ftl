# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = এড-অনসমূহৰ ব্যৱস্থাপক
search-header-shortcut =
    .key = f
loading-label =
    .value = ল'ড কৰা হৈছে…
list-empty-installed =
    .value = ইনস্টল হোৱা এই ধৰণৰ বাবে আপোনাৰ কোনো এড-অন নাই
list-empty-available-updates =
    .value = কোনো আপডেইট পোৱা নগল
list-empty-recent-updates =
    .value = আপুনি শেহতীয়াভাৱে কোনো এড-অন আপডেইট কৰা নাই
list-empty-find-updates =
    .label = আপডেইটসমূহৰ কাৰণে নীৰিক্ষণ কৰক
list-empty-button =
    .label = এড-অন সমূহুৰ বিষয়ে অধিক জানক
install-addon-from-file =
    .label = ফাইলৰ পৰা এড-অন ইনস্টল কৰক…
    .accesskey = I
tools-menu =
    .tooltiptext = সকলো এড-অনৰ পৰা সঁজুলিসমূহ
cmd-show-details =
    .label = অধিক তথ্য দেখুৱাওক
    .accesskey = S
cmd-find-updates =
    .label = আপডেইটসমূহ বিচাৰক
    .accesskey = F
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] বিকল্পসমূহ
           *[other] পছন্দসমূহ
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
cmd-enable-theme =
    .label = থীম পিন্ধাওক
    .accesskey = W
cmd-disable-theme =
    .label = থীম পিন্ধোৱা বন্ধ কৰক
    .accesskey = W
cmd-install-addon =
    .label = ইনস্টল কৰক
    .accesskey = I
cmd-contribute =
    .label = অৱদান দিয়ক
    .accesskey = C
    .tooltiptext = এই এড-অনৰ উন্নয়নৰ বাবে অৱদান কৰক
discover-title = এড-অনসমূহ কি?
discover-description = এড-অনসমূহ হল কিছুমান এপ্লিকেচন যিবোৰে আপোনাক { -brand-short-name } ক অতিৰিক্ত কাৰ্য্যকৰীতা অথবা শৈলীৰ হৈতে ব্যক্তিগত ৰূপ দিয়াৰ অনুমতি দিয়ে। { -brand-short-name } ক আপোনাৰ নিজাববিয়া বনাবলে এটা সময়-সঞ্চয়ী কাষবাৰ, বতৰ সংকেতক অথবা এটা থীম ৰূপ প্ৰয়োগ কৰি চাওক।
discover-footer = যেতিয়া আপুনি ইন্টাৰনেটৰ লগত সংযোগিত থাকিব, এই পেইনে আপুনি প্ৰয়োগ কৰিবলে কিছুমান শ্ৰেষ্ট আৰু লোকপ্ৰিয় এড-অন দেখাব।
detail-version =
    .label = সংস্কৰণ
detail-last-updated =
    .label = সৰবশেষ আপডেইট কৰা হৈছিল
detail-contributions-description = এই এড-অনৰ উন্নয়কে বিচাৰে যে আপুনি সৰু এটা বৰঙনি আগবঢ়াই ইয়াৰ উন্নয়ন কাৰ্য্য সমৰ্থন কৰে।
detail-update-type =
    .value = স্বচালিত আপডেইটসমূহ
detail-update-default =
    .label = অবিকল্পিত
    .tooltiptext = আপডেইটসমূহ স্বচালিতভাৱে ইনস্টল কৰিব যদিহে সেইটো অবিকল্পিত হয়
detail-update-automatic =
    .label = অন
    .tooltiptext = আপডেইটসমূহ স্বচালিতভাৱে ইনস্টল কৰক
detail-update-manual =
    .label = অফ
    .tooltiptext = আপডেইটসমূহ স্বচালিতভাৱে ইনস্টল নকৰিব
detail-home =
    .label = ঘৰপৃষ্ঠা
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = এড-অন আলেখ্যন
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = আপডেইটসমূহৰ কাৰণে নীৰিক্ষণ কৰক
    .accesskey = f
    .tooltiptext = এই এড-অনৰ কাৰণে আপডেইটসমূহ নীৰিক্ষণ কৰক
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] বিকল্পসমূহ
           *[other] পছন্দসমূহ
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] এই এড-অনৰ বিকল্পসমূহ পৰিৱৰ্তন কৰক
           *[other] এই এড-অনৰ পছন্দসমূহ পৰিৱৰ্তন কৰক
        }
detail-rating =
    .value = হাৰাংক
addon-restart-now =
    .label = এতিয়া পুনৰাম্ভ কৰক
extensions-view-discover =
    .name = এড-অনসমূহ প্ৰাপ্ত কৰক
    .tooltiptext = { extensions-view-discover.name }
extensions-view-recent-updates =
    .name = শেহতীয়া আপডেইটসমূহ
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = উপলব্ধ আপডেইটসমূহ
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = সকলো এড-অন সুৰক্ষিত অৱস্থা দ্বাৰা অসামৰ্থবান
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = এড-অন খাপ খোৱা নীৰিক্ষণ অসামৰ্থবান কৰা আছে। আপোনাৰ খুব সম্ভব খাপ নোখোৱা এড-অনসমূহ আছে
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = সামৰ্থবান কৰক
    .tooltiptext = এড-অন খাপ খোৱা নীৰিক্ষণ সামৰ্থবান কৰক
extensions-warning-update-security-label =
    .value = এড-অন আপডেইট সুৰক্ষা নীৰিক্ষণ অসামৰ্থবান কৰা আছে। আপুনি আপডেইটসমূহৰে আপোচ কৰিব লগিয়া হব পাৰে।
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = সামৰ্থবান কৰক
    .tooltiptext = এড-অন আপডেইট সুৰক্ষা নীৰিক্ষণ সামৰ্থবান কৰক

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = আপডেইটসমূহৰ বাবে নীৰিক্ষণ কৰক
    .accesskey = C
extensions-updates-view-updates =
    .label = শেহতীয়া আপডেইটসমূহ দৰ্শন কৰক
    .accesskey = V

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = স্বচালিতভাৱে এড-অনসমূহ আপডেইট কৰক
    .accesskey = A

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = সকলো এড-অন স্বচালিতভাৱে আপডেইট হবলে পুনৰ সংহতি কৰক
    .accesskey = R
extensions-updates-reset-updates-to-manual =
    .label = সকলো এড-অন হস্তচালিতভাৱে আপডেইট হবলে পুনৰ সংহতি কৰক
    .accesskey = R

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = এড-অনসমূহ আপডেইট কৰা হৈছে
extensions-updates-installed =
    .value = আপোনাৰ এড-অনসমূহ আপডেইট কৰা হল।
extensions-updates-downloaded =
    .value = আপোনাৰ এড-অন আপডেইটসমূহ ডাউনল'ড কৰা হল।
extensions-updates-restart =
    .label = ইনস্টল সম্পূৰ্ণ কৰিবলে পুনৰাম্ভ কৰক
extensions-updates-none-found =
    .value = কোনো আপডেইট পোৱা নগল
extensions-updates-manual-updates-found =
    .label = উপলব্ধ আপডেইটসমূহ দৰ্শন কৰক
extensions-updates-update-selected =
    .label = আপডেইটসমূহ ইনস্টল কৰক
    .tooltiptext = এই তালিকাত থকা উপলব্ধ আপডেইটসমূহ ইনস্টল কৰক
