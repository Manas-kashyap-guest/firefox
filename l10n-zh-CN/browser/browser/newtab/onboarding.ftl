# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = 立即尝鲜
onboarding-button-label-get-started = 开始使用
onboarding-welcome-header = 欢迎使用 { -brand-short-name }
onboarding-start-browsing-button-label = 开始浏览

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = 隐私浏览
onboarding-private-browsing-text = 上网更私密。使用配有内容拦截功能的隐私浏览，帮助您拦截网络上跟踪您的在线跟踪器。
onboarding-screenshots-title = 屏幕截图
onboarding-screenshots-text = 不用离开 { -brand-short-name } 即可截取、保存以及分享屏幕截图。可直接截取整个页面或选定区域，然后自动上传到网络上，方便访问分享。
onboarding-addons-title = 附加组件
onboarding-addons-text = 附加组件能扩展 { -brand-short-name } 的内置功能，使浏览器更满足您的需求。您可以用附加组件来比价格、查天气，或是选用主题来彰显您的个性。
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = 使用 Ghostery 等扩展拦截恼人的广告，让您上网更快、更智能、更安全。
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = 同步
onboarding-fxa-text = 注册 { -fxaccount-brand-name } 之后，您可在任何使用 { -brand-short-name } 的任何地方同步您的书签、密码和打开的标签页。
