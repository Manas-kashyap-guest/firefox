# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Ngjite Sipër
    .accesskey = S
languages-customize-movedown =
    .label = Zbrite Poshtë
    .accesskey = P
languages-customize-remove =
    .label = Hiqe
    .accesskey = H
languages-customize-select-language =
    .placeholder = Përzgjidhni gjuhë për t’u shtuar…
languages-customize-add =
    .label = Shtoje
    .accesskey = S
messenger-languages-window =
    .title = Rregullime Gjuhe { -brand-short-name }-i
    .style = width: 40em
messenger-languages-description = { -brand-short-name }-i do të shfaqë gjuhën e parë si parazgjedhjen tuaj dhe, në qoftë e nevojshme,  do të shfaqë gjuhë alternativee languages sipas radhës që shfaqe.
