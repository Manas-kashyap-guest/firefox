# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Górjej
    .accesskey = G
languages-customize-movedown =
    .label = Dołoj
    .accesskey = D
languages-customize-remove =
    .label = Wótwónoźeś
    .accesskey = W
languages-customize-select-language =
    .placeholder = Wubjeŕśo rěc, aby ju pśidał…
languages-customize-add =
    .label = Pśidaś
    .accesskey = P
messenger-languages-window =
    .title = Rěcne nastajenja { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } pokažo prědnu rěc ako waš standard a dalšne rěcy, jolic trjeba, w pórěźe, w kótaremž se zjewijo.
