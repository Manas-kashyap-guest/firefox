<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY aboutSupport.pageTitle "ట్రబుల్‌షూటింగ్ సమాచారము">

<!-- LOCALIZATION NOTE (aboutSupport.pageSubtitle): don't change the 'supportLink' id. -->
<!ENTITY aboutSupport.pageSubtitle "  మీరు ఒక సమస్యను పరిష్కరించుటకు ప్రయత్నించునప్పుడు మీకు ఉపయోగవంతంగా వుండగల సాంకేతిక సమాచారమును ఈ పేజీ కలిగివుంటుంది. మీరు &brandShortName; గురించిన వుమ్మడి ప్రశ్నలకు సమాధానముల కొరకు చూస్తుంటే, మా <a id='supportLink'>తోడ్పాటు వెబ్ సైట్</a> చూడండి.">

<!ENTITY aboutSupport.crashes.title "క్రాష్ నివేదికలు">
<!-- LOCALIZATION NOTE (aboutSupport.crashes.id):
This is likely the same like id.heading in crashes.dtd. -->
<!ENTITY aboutSupport.crashes.id "నివేదిక ID">
<!ENTITY aboutSupport.crashes.sendDate "సమర్పించినది">
<!ENTITY aboutSupport.crashes.allReports "అన్ని క్రాష్ నివేదికలు">
<!ENTITY aboutSupport.crashes.noConfig "క్రాష్ నివేదికలను ప్రదర్శించుటకు ఈ అనువర్తనం ఆకృతీకరించలేదు.">

<!ENTITY aboutSupport.extensionsTitle "పొడిగింపులు">
<!ENTITY aboutSupport.extensionName "పేరు">
<!ENTITY aboutSupport.extensionEnabled "చేతనం">
<!ENTITY aboutSupport.extensionVersion "వెర్షను">
<!ENTITY aboutSupport.extensionId "ID">

<!ENTITY aboutSupport.securitySoftwareTitle "భద్రతా సాఫ్ట్‌వేరు">
<!ENTITY aboutSupport.securitySoftwareType "రకం">
<!ENTITY aboutSupport.securitySoftwareName "పేరు">
<!ENTITY aboutSupport.securitySoftwareAntivirus "యాంటీవైరస్">
<!ENTITY aboutSupport.securitySoftwareAntiSpyware "యాంటీస్పైవేర్">
<!ENTITY aboutSupport.securitySoftwareFirewall "ఫైర్వాల్">

<!ENTITY aboutSupport.featuresTitle "&brandShortName; విశేషాలు">
<!ENTITY aboutSupport.featureName "పేరు">
<!ENTITY aboutSupport.featureVersion "వెర్షను">
<!ENTITY aboutSupport.featureId "ID">

<!ENTITY aboutSupport.appBasicsTitle "అనువర్తనం ప్రాధమికాలు">
<!ENTITY aboutSupport.appBasicsName "పేరు">
<!ENTITY aboutSupport.appBasicsVersion "వెర్షను">
<!ENTITY aboutSupport.appBasicsBuildID "బిల్డ్ ID">

<!-- LOCALIZATION NOTE (aboutSupport.appBasicsUpdateChannel, aboutSupport.appBasicsUpdateHistory, aboutSupport.appBasicsShowUpdateHistory):
"Update" is a noun here, not a verb. -->
<!ENTITY aboutSupport.appBasicsUpdateChannel "నవీకరణ ఛానల్">
<!ENTITY aboutSupport.appBasicsUpdateHistory "తాజాకరణ చరిత్ర">
<!ENTITY aboutSupport.appBasicsShowUpdateHistory "తాజాకరణ చరిత్రను చూపించు">

<!ENTITY aboutSupport.appBasicsProfileDir "ప్రొఫైల్ డైరెక్టరీ">
<!-- LOCALIZATION NOTE (aboutSupport.appBasicsProfileDirWinMac):
This is the Windows- and Mac-specific variant of aboutSupport.appBasicsProfileDir.
Windows/Mac use the term "Folder" instead of "Directory" -->
<!ENTITY aboutSupport.appBasicsProfileDirWinMac "ప్రొఫైల్ సంచయం">

<!ENTITY aboutSupport.appBasicsEnabledPlugins "చేతనపరచిన చొప్పింతలు">
<!ENTITY aboutSupport.appBasicsBuildConfig "నిర్మామ ఆకృతీకరణ">
<!ENTITY aboutSupport.appBasicsUserAgent "వాడుకరి ప్రతినిధి">
<!ENTITY aboutSupport.appBasicsOS "OS">
<!ENTITY aboutSupport.appBasicsMemoryUse "మెమొరీ వినియోగం">
<!ENTITY aboutSupport.appBasicsPerformance "పనితనం">

<!-- LOCALIZATION NOTE the term "Service Workers" should not be translated. -->
<!ENTITY aboutSupport.appBasicsServiceWorkers "నమోదైన సర్వీస్ వర్కర్స్">

<!ENTITY aboutSupport.appBasicsProfiles "ప్రొఫైల్స్">

<!ENTITY aboutSupport.appBasicsMultiProcessSupport "మల్టీప్రోసెస్ విండోలు">


<!ENTITY aboutSupport.enterprisePolicies "ఎంటర్‌ప్రైజ్ విధానాలు">

<!ENTITY aboutSupport.appBasicsKeyGoogle "గూగుల్ కీ">
<!ENTITY aboutSupport.appBasicsKeyMozilla "మొజిల్లా స్థాన సేవ కీ">

<!ENTITY aboutSupport.appBasicsSafeMode "సురక్షిత రీతి">

<!ENTITY aboutSupport.showDir.label "నిఘంటువు తెరువు">
<!-- LOCALIZATION NOTE (aboutSupport.showMac.label): This is the Mac-specific
variant of aboutSupport.showDir.label.  This allows us to use the preferred
"Finder" terminology on Mac. -->
<!ENTITY aboutSupport.showMac.label "ఫైండర్ నందు తెరువు">
<!-- LOCALIZATION NOTE (aboutSupport.showWin2.label): This is the Windows-specific
variant of aboutSupport.showDir.label. -->
<!ENTITY aboutSupport.showWin2.label "సంచయాన్ని తెరువు">

<!ENTITY aboutSupport.modifiedKeyPrefsTitle "సవరించిన అభిరుచులలో ముఖ్యమైనవి">
<!ENTITY aboutSupport.modifiedPrefsName "పేరు">
<!ENTITY aboutSupport.modifiedPrefsValue "విలువ">

<!-- LOCALIZATION NOTE (aboutSupport.userJSTitle, aboutSupport.userJSDescription): user.js is the name of the preference override file being checked. -->
<!ENTITY aboutSupport.userJSTitle "user.js అభిరుచులు">
<!ENTITY aboutSupport.userJSDescription "మీ ప్రొఫైల్ ఫోల్డర్ ఒక <a id='prefs-user-js-link'>user.js ఫైలు</a> కలిగివుంది, అది &brandShortName; చే సృష్టించబడని అభీష్టాలను కలిగివుంటుంది.">

<!ENTITY aboutSupport.lockedKeyPrefsTitle "తాళంవేసిన అభిరుచులలో ముఖ్యమైనవి">
<!ENTITY aboutSupport.lockedPrefsName "పేరు">
<!ENTITY aboutSupport.lockedPrefsValue "విలువ">

<!ENTITY aboutSupport.graphicsTitle "గ్రాఫిక్స్">

<!ENTITY aboutSupport.placeDatabaseTitle "స్థలాలు డేటాబేస్">
<!ENTITY aboutSupport.placeDatabaseIntegrity "సమగ్రత">
<!ENTITY aboutSupport.placeDatabaseVerifyIntegrity "ఇంటెగ్రిటీ నిర్ధారించండి">

<!ENTITY aboutSupport.jsTitle "జావాస్క్రిప్ట్">
<!ENTITY aboutSupport.jsIncrementalGC "ప్రవర్ధమాన GC">

<!ENTITY aboutSupport.a11yTitle "సులభత">
<!ENTITY aboutSupport.a11yActivated "క్రియాశీలమైంది">
<!ENTITY aboutSupport.a11yForceDisabled "సులభత నిరోధించు">
<!ENTITY aboutSupport.a11yHandlerUsed "ప్రాప్యత చేయగల హ్యాండ్లర్ వాడినది">
<!ENTITY aboutSupport.a11yInstantiator "ప్రాప్యత తక్షణం">

<!ENTITY aboutSupport.libraryVersionsTitle "లైబ్రరీ వర్షన్స్">

<!ENTITY aboutSupport.installationHistoryTitle "స్థాపనా చరిత్ర">
<!ENTITY aboutSupport.updateHistoryTitle "తాజాకరణ చరిత్ర">

<!ENTITY aboutSupport.copyTextToClipboard.label "పాఠ్యాన్ని క్లిప్‌బోర్డుకి కాపీచేయి">
<!ENTITY aboutSupport.copyRawDataToClipboard.label "ముడి దత్తాంశాన్ని క్లిప్‌బోర్డుకి కాపీచేయి">

<!ENTITY aboutSupport.sandboxTitle "శాండ్ బాక్స్">
<!ENTITY aboutSupport.sandboxSyscallLogTitle "తిరసృత సిస్టమ్ కాల్స్">
<!ENTITY aboutSupport.sandboxSyscallIndex "#">
<!ENTITY aboutSupport.sandboxSyscallAge "క్షణాల క్రితం">
<!ENTITY aboutSupport.sandboxSyscallPID "PID">
<!ENTITY aboutSupport.sandboxSyscallTID "TID">
<!ENTITY aboutSupport.sandboxSyscallProcType "ప్రాసెస్ రకం">
<!ENTITY aboutSupport.sandboxSyscallNumber "సిస్‌కాల్">
<!ENTITY aboutSupport.sandboxSyscallArgs "చర్చలు">

<!ENTITY aboutSupport.safeModeTitle "సేఫ్ మోడ్ ప్రయత్నించండి">
<!ENTITY aboutSupport.restartInSafeMode.label "పొడిగింతలను అచేతనించి పునఃప్రారంభించు…">

<!ENTITY aboutSupport.graphicsFeaturesTitle "ఫీచర్‌లు">
<!ENTITY aboutSupport.graphicsDiagnosticsTitle "డయాగ్నస్టిక్స్">
<!ENTITY aboutSupport.graphicsFailureLogTitle "వైఫల్యమైన లాగ్">
<!ENTITY aboutSupport.graphicsGPU1Title "GPU #1">
<!ENTITY aboutSupport.graphicsGPU2Title "GPU #2">
<!ENTITY aboutSupport.graphicsDecisionLogTitle "డెసిషన్ లాగ్">
<!ENTITY aboutSupport.graphicsCrashGuardsTitle "క్రాష్ గార్డ్ యొక్క నిలిపివేసిన ఫీచర్స్">
<!ENTITY aboutSupport.graphicsWorkaroundsTitle "పరిష్కారాలను">

<!ENTITY aboutSupport.mediaTitle "మాధ్యమం">
<!ENTITY aboutSupport.mediaOutputDevicesTitle "ఔట్‌పుట్ పరికరాలు">
<!ENTITY aboutSupport.mediaInputDevicesTitle "ఇన్‌పుట్ పరికరాలు">
<!ENTITY aboutSupport.mediaDeviceName "పేరు">
<!ENTITY aboutSupport.mediaDeviceGroup "సమూహం">
<!ENTITY aboutSupport.mediaDeviceVendor "అమ్మకందారు">
<!ENTITY aboutSupport.mediaDeviceState "స్థితి">
<!ENTITY aboutSupport.mediaDevicePreferred "ప్రాధాన్యత">
<!ENTITY aboutSupport.mediaDeviceFormat "రూపం">
<!ENTITY aboutSupport.mediaDeviceChannels "వాహికలు">
<!ENTITY aboutSupport.mediaDeviceRate "రేటు">
<!ENTITY aboutSupport.mediaDeviceLatency "Latency">

<!ENTITY aboutSupport.intlTitle "అంతర్జాతీయీకరణ &amp; స్థానికీకరణ">
<!ENTITY aboutSupport.intlAppTitle "అనువర్తన అమరికలు">
<!ENTITY aboutSupport.intlLocalesRequested "అభ్యర్థించిన భాషలు">
<!ENTITY aboutSupport.intlLocalesAvailable "అందుబాటులోని భాషలు">
<!ENTITY aboutSupport.intlLocalesDefault "అప్రమేయ లొకేల్">
<!ENTITY aboutSupport.intlOSTitle "నిర్వాహక వ్యవస్థ">
<!ENTITY aboutSupport.intlRegionalPrefs "ప్రాంతీయ అభిరుచులు">
