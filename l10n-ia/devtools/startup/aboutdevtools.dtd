<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- LOCALIZATION NOTE : This file contains the strings used in aboutdevtools.xhtml,
  -  displayed when going to about:devtools. UI depends on the value of the preference
  -  "devtools.enabled".
  -
  -  "aboutDevtools.enable.*" and "aboutDevtools.newsletter.*" keys are used when DevTools
     are disabled
  -  "aboutDevtools.welcome.*" keys are used when DevTools are enabled
  - -->

<!-- LOCALIZATION NOTE (aboutDevtools.headTitle): Text of the title tag for about:devtools -->
<!ENTITY  aboutDevtools.headTitle "Circa le utensiles de disveloppamento">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.title): Title of the top about:devtools
  -  section displayed when DevTools are disabled. -->
<!ENTITY  aboutDevtools.enable.title "Activar le Utensiles de disveloppamento de Firefox">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementTitle): Title of the top
  -  section displayed when devtools are disabled and the user triggered DevTools by using
  -  the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementTitle "Activar le Utensiles de disveloppamento de Firefox pro usar Inspicer elemento">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.inspectElementMessage): Message displayed
  -  when users come from using the Inspect Element menu item. -->
<!ENTITY  aboutDevtools.enable.inspectElementMessage
          "Examina e redige HTML e CSS le Inspector del utensiles de disveloppamento.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.aboutDebuggingMessage): Message displayed
  -  when users come from about:debugging. -->
<!ENTITY  aboutDevtools.enable.aboutDebuggingMessage
          "Disveloppa e depura WebExtensions, web workers, service workers e plus per le Utensiles de disveloppamento de Firefox.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.keyShortcutMessage): Message displayed when
  -  users pressed a DevTools key shortcut. -->
<!ENTITY  aboutDevtools.enable.keyShortcutMessage
          "Tu ha activate un accesso directe al instrumentos de developpator. Si isto esseva un error, tu pote clauder iste scheda.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.menuMessage2): Message displayed when users
  -  clicked on a "Enable Developer Tools" menu item. -->
<!ENTITY  aboutDevtools.enable.menuMessage2
          "Perfectiona le HTML, CSS, e JavaScript de tu sito web per utensiles como Inspector e Depurator.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.commonMessage): Generic message displayed for
  -  all possible entry points (keyshortcut, menu item etc…). -->
<!ENTITY  aboutDevtools.enable.commonMessage
          "Le Utensiles de disveloppamento de Firefox es disactivate normalmente pro dar te major controlo de tu navigator.">

<!-- LOCALIZATION NOTE (aboutDevtools.enable.learnMoreLink): Text for the link to
  -  https://developer.mozilla.org/docs/Tools displayed in the top section when DevTools
  -  are disabled. -->
<!ENTITY  aboutDevtools.enable.learnMoreLink "Apprende plus re le Utensiles del disveloppamento">

<!ENTITY  aboutDevtools.enable.enableButton "Activar le Utensiles de disveloppamento">
<!ENTITY  aboutDevtools.enable.closeButton2 "Clauder iste scheda">

<!ENTITY  aboutDevtools.welcome.title "Benvenite al Utensiles de disveloppamento de Firefox!">

<!ENTITY  aboutDevtools.newsletter.title "Littera del novas de disveloppamento de Mozilla">
<!-- LOCALIZATION NOTE (aboutDevtools.newsletter.message): Subscribe form message.
  -  The newsletter is only available in english at the moment.
  -  See Bug 1415273 for support of additional languages.-->
<!ENTITY  aboutDevtools.newsletter.message "Recipe novas, artificios e ressources de disveloppamento inviate directemente a tu cassa de entrata.">
<!ENTITY  aboutDevtools.newsletter.email.placeholder "Email">
<!ENTITY  aboutDevtools.newsletter.privacy.label "Io consenti con le tractamento per Mozilla de mi informationes como explicate in iste <a class='external' href='https://www.mozilla.org/privacy/'>Politica de confidentialitate</a>.">
<!ENTITY  aboutDevtools.newsletter.subscribeButton "Subscriber">
<!ENTITY  aboutDevtools.newsletter.thanks.title "Gratias!">
<!ENTITY  aboutDevtools.newsletter.thanks.message "Si tu non habeva antea confirmate un subscription a un newsletter re Mozilla tu pote facer lo. Per favor controla tu cassa de entrata o tu filtro del spam pro un e-posta ab nos.">

<!ENTITY  aboutDevtools.footer.title "Firefox Developer Edition">
<!ENTITY  aboutDevtools.footer.message "Cerca tu plus que justo Utensiles de disveloppamento? Controla le navigator Firefox create specificamente pro le disveloppatores e le fluxos de labor moderne.">

<!-- LOCALIZATION NOTE (aboutDevtools.footer.learnMoreLink): Text for the link to
  -  https://www.mozilla.org/firefox/developer/ displayed in the footer. -->
<!ENTITY  aboutDevtools.footer.learnMoreLink "Saper plus">

