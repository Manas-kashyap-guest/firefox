# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = အက်ပ် မွမ်းမံချက် URL ကို ပြောင်းလဲသတ်မှတ်ရန်။
policy-Authentication = တစ်ပါတည်းဖြစ်သော အထောက်အထား စစ်ဆေးခြင်းကို ထောက်ပံ့သော ဝဘ်ဆိုက်များအတွက် ၎င်းလုပ်ဆောင်ချက်ကို ပြင်ဆင်သတ်မှတ်ရန်။
policy-BlockAboutAddons = အတ်အွန်များ စီမံနေရာ အသုံးပြုခြင်းကို ပိတ်ရန် (about:addons)။
policy-BlockAboutConfig = about:config စာမျက်နှာ အသုံးပြုခြင်းကို ပိတ်ရန်။
policy-BlockAboutProfiles = about:profile စာမျက်နှာ အသုံးပြုခြင်းကို ပိတ်ရန်။
policy-BlockAboutSupport = about:support စာမျက်နှာ အသုံးပြုခြင်းကို ပိတ်ရန်။
policy-CertificatesDescription = သက်သေခံလက်မှတ် ထည့်ပါ သို့မဟုတ် ပါဝင်ပြီး သက်သေခံလက်မှတ်ကို အသုံးပြုပါ။
policy-Cookies = ကွတ်ကီးအသုံးပြုခြင်းကို ခွင့်ပြုရန် သို့မဟုတ် တားမြစ်ရန်။
policy-DisableAppUpdate = ဘရောင်ဇာကို နောက်ဆုံးပေါ် ကို ဆွဲယူမွမ်းမံခြင်းမှ တားဆီးရန်။
policy-DisableBuiltinPDFViewer = { -brand-short-name } တွင် တစ်ခါတည်း ပါလာသော PDF viewer PDF.js ကို ပိတ်ရန်။
policy-DisableFormHistory = ရှာဖွေမှု ဖြည့်စွက်မှု သမိုင်းများကို မမှတ်ထားပါ
policy-DisableMasterPasswordCreation = ဒါမှန်တယ်ဆိုပါက မာစတာစကားဝှက်ကို မဖန်တီးနိုင်ပါ
policy-DisablePrivateBrowsing = သီးသန့်ရှာဖွေမှုကို ပိတ်ပါ
policy-Proxy = ပရောက်ဆီ စနစ်များ နေရာချပါ
