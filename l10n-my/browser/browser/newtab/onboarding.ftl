# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ယခုပင် စမ်းကြည့်ပါ
onboarding-button-label-get-started = စတင်ပါ
onboarding-welcome-header = { -brand-short-name } မှကြိုဆိုပါတယ်
onboarding-start-browsing-button-label = ရှာဖွေမှုစမည်

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = လုံခြုံစွာရှာဖွေခြင်း
onboarding-private-browsing-text = မိမိကိုယ်တိုင် ရှာဖွေနိုင်သည်။ အကြောင်းအရာ စစ်ထုတ် တားဆီးခြင်းနှင့် လုံခြုံစွာရှာဖွေခြင်းသည် ဝဘ်တွင်ရှိတိုင်း နောက်ယောင်ခံလိုက်နေမည့် အွန်လိုင်းခြေရာခံကိရိယာများကို တားဆီးပါမည်။
onboarding-screenshots-title = မျက်နှာပြင်ပုံရိပ်ဖမ်းချက်
onboarding-screenshots-text = { -brand-short-name } ထံမှ ထွက်ခွာစရာ မလိုဘဲ မျက်နှာပြင် ပုံဖမ်းချက်များကို ရိုက်ကူးပါ၊ သိမ်းဆည်းပါ၊ ပြီးနောက် မျှဝေပါ။ နေရာတစ်ခု သို့မဟုတ် စာမျက်နှာတစ်ခုလုံးကို ဖမ်းယူပါ။ ထို့နောက် အလွယ်တကူ အသုံးပြုနိုင်ရန်နှင့် မျှဝေနိုင်ရန် ဝဘ်တွင် သိမ်းဆည်းပါ။
onboarding-addons-title = အတ်အွန်များ
onboarding-ghostery-title = Ghostery
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = ထပ်တူပြုရန်
