# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-heading = အကြံပြုထားသော တိုးချဲ့ချက်
cfr-doorhanger-extension-sumo-link =
    .tooltiptext = ဘာကြောင့် မြင်ရပါသနည်း
cfr-doorhanger-extension-cancel-button = ယခု မဟုတ်သေးပါ
    .accesskey = N
cfr-doorhanger-extension-ok-button = ယခုပင်ထည့်ပါ
    .accesskey = A
cfr-doorhanger-extension-manage-settings-button = အကြံပြုချက်နှင့်ဆိုင်သည့်အပြင်အဆင်များစီမံပါ
    .accesskey = M
cfr-doorhanger-extension-never-show-recommendation = ဒီအကြံပြုချက်ကိုမပြပါနှင့်
    .accesskey = S
cfr-doorhanger-extension-learn-more-link = ပိုမိုလေ့လာရန်
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = { $name } အားဖြင့်
# This is a notification displayed in the address bar.
# When clicked it opens a panel with a message for the user.
cfr-doorhanger-extension-notification = အကြံပြုချက်

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

# Variables:
#   $total (Number) - The rating of the add-on from 1 to 5
cfr-doorhanger-extension-rating =
    .tooltiptext =
        { $total ->
           *[other] ကြယ် { $total } ပွင့်
        }
# Variables:
#   $total (Number) - The total number of users using the add-on
cfr-doorhanger-extension-total-users =
    { $total ->
       *[other] သုံးစွဲသူ { $total } ယောက်
    }
