# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Stratégies d’entreprise
# 'Active' is used to describe the policies that are currently active
active-policies-tab = En fonction
errors-tab = Erreurs
documentation-tab = Documentation
policy-name = Nom de la stratégie
policy-value = Contenu de la stratégie
policy-errors = Erreurs de stratégie
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = Lors de l’utilisation de stratégies de groupe, cette politique peut uniquement être définie au niveau de l’ordinateur.
