# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Monter
    .accesskey = r
languages-customize-movedown =
    .label = Descendre
    .accesskey = D
languages-customize-remove =
    .label = Supprimer
    .accesskey = S
languages-customize-select-language =
    .placeholder = Choisir une langue à ajouter…
languages-customize-add =
    .label = Ajouter
    .accesskey = A
messenger-languages-window =
    .title = Paramètres de langue de { -brand-short-name }
    .style = width: 40em
messenger-languages-description = { -brand-short-name } affichera la première langue par défaut puis les autres langues si nécessaire dans leur ordre d’apparition.
