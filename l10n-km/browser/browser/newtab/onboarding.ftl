# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = សាកល្បង​វា​ឥឡូវ​នេះ
onboarding-button-label-get-started = ត្រូវ​បាន​ចាប់​ផ្ដើម​
onboarding-welcome-header = សូម​ស្វាគមន៍​មក​កាន់ { -brand-short-name }
onboarding-start-browsing-button-label = ចាប់​ផ្ដើម​រុករក

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = ការ​រក​មើល​ឯកជន
onboarding-screenshots-title = រូប​ថត​​អេក្រង់
onboarding-addons-title = កម្មវិធី​បន្ថែម
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = ធ្វើ​សមកាលកម្ម
onboarding-fxa-text = ចុះ​ឈ្មោះ​សម្រាប់ { -fxaccount-brand-name } និង​ធ្វើ​សមកាលកម្ម​ចំណាំ, ពាក្យសម្ងាត់​និង​ផ្ទាំង​បើក​របស់​អ្នក​គ្រប់​ទី​កន្លែង​ដែល​អ្នក​ប្រើ { -brand-short-name } ។
