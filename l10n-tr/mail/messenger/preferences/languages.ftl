# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Yukarı taşı
    .accesskey = Y
languages-customize-movedown =
    .label = Aşağı taşı
    .accesskey = A
languages-customize-remove =
    .label = Kaldır
    .accesskey = K
languages-customize-select-language =
    .placeholder = Eklenecek bir dil seçin…
languages-customize-add =
    .label = Ekle
    .accesskey = E
messenger-languages-window =
    .title = { -brand-short-name } Dil Ayarları
    .style = width: 40em
messenger-languages-description = { -brand-short-name } ilk dili varsayılan olarak kullanacak, gerekirse alternatif dilleri göründükleri sırayla kullanacaktır.
