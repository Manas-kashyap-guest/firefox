/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "nsISupports.idl"

/**
 * nsIWebContentHandlerRegistrar
 * 
 * Applications wishing to use web content handlers need to implement this
 * interface. Typically they will prompt the user to confirm adding an entry
 * to the local list. 
 *
 * The component must have the contract id defined below so that nsNavigator
 * can invoke it. 
 */

[scriptable, uuid(65a3fafd-0e4a-4b06-8b4e-6a611da63d98)]
interface nsIWebContentHandlerRegistrar : nsISupports
{
  /**
   * See documentation in Navigator.webidl
   * The additional contentWindow param for this method represents the dom
   * content window from which the method has been called.
   */
   void registerProtocolHandler(in AString protocol,
                                in AString uri,
                                in AString title,
                                in nsISupports windowOrBrowser);
  /**
   * Removes a registered protocol handler
   *
   * While registerProtocolHandler is exposed on Navigator, unregistering
   * is exposed through the UI code.
   * @param   protocol
   *          The protocol scheme to remove a service handler for
   * @param   uri
   *          The uri of the service handler to remove
   */
  void removeProtocolHandler(in AString protocol, in AString uri);
};

%{ C++

#define NS_WEBCONTENTHANDLERREGISTRAR_CONTRACTID "@mozilla.org/embeddor.implemented/web-content-handler-registrar;1"
%}
