# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Proveu-ho ara
onboarding-welcome-header = Us donem la benvinguda al { -brand-short-name }
onboarding-start-browsing-button-label = Comença a navegar

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = Navegació privada
onboarding-private-browsing-text = Navegueu tot sol. La navegació privada amb bloqueig de contingut bloca els elements que us fan el seguiment mentre navegueu.
onboarding-screenshots-title = Captures de pantalla
onboarding-screenshots-text = Feu captures de pantalla, deseu-les i compartiu-les sense sortir del { -brand-short-name }. Captureu una regió o una pagina sencera mentre navegueu. Llavors, deseu-la al web per accedir-hi i compartir-la fàcilment.
onboarding-addons-title = Complements
onboarding-addons-text = Afegiu més funcions al { -brand-short-name } per tal que encara faci més coses. Podeu comparar preus, veure quin temps farà o expressar la vostra personalitat amb un tema personalitzat.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Les extensions com el Ghostery, que bloqueja els anuncis molestos, permeten una navegació més ràpida, més intel·ligent i més segura.
