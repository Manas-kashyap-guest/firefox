# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Puja
    .accesskey = u
languages-customize-movedown =
    .label = Baixa
    .accesskey = B
languages-customize-remove =
    .label = Elimina
    .accesskey = m
languages-customize-select-language =
    .placeholder = Seleccioneu la llengua que voleu afegir…
languages-customize-add =
    .label = Afegeix
    .accesskey = A
messenger-languages-window =
    .title = Paràmetres de llengua del { -brand-short-name }
    .style = width: 40em
messenger-languages-description = El { -brand-short-name } utilitzarà la primera llengua per defecte i, si cal, utilitzarà les altres llengües en l'ordre en què apareixen.
