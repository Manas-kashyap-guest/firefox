# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-window =
    .title = Luengas
    .style = width: 30em
languages-close-key =
    .key = w
languages-description = Bellas pachinas web puede ofrir-se en mas d'una luenga. Triga los idiomas en que quiers que s'amuestren estas pachinas webs, en orden de preferencia
languages-customize-spoof-english =
    .label = Demandar versions en anglés de pachinas web pa amillorar la privacidat
languages-customize-moveup =
    .label = Puyar
    .accesskey = u
languages-customize-movedown =
    .label = Baixar
    .accesskey = B
languages-customize-remove =
    .label = Eliminar
    .accesskey = r
languages-customize-select-language =
    .placeholder = Trigue un idioma a adhibir…
languages-customize-add =
    .label = Adhibir
    .accesskey = A
# The pattern used to generate strings presented to the user in the
# locale selection list.
#
# Example:
#   Icelandic [is]
#   Spanish (Chile) [es-CL]
#
# Variables:
#   $locale (String) - A name of the locale (for example: "Icelandic", "Spanish (Chile)")
#   $code (String) - Locale code of the locale (for example: "is", "es-CL")
languages-code-format =
    .label = { $locale }  [{ $code }]
