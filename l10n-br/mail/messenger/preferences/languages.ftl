# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = Davet ar c'hrec'h
    .accesskey = D
languages-customize-movedown =
    .label = Davet an traoñ
    .accesskey = t
languages-customize-remove =
    .label = Dilemel
    .accesskey = D
languages-customize-select-language =
    .placeholder = Diuzit ur yezh da ouzhpennañ...
languages-customize-add =
    .label = Ouzhpennañ
    .accesskey = O
messenger-languages-window =
    .title = { -brand-short-name } Arventennoù Yezh
    .style = width: 40em
messenger-languages-description = { -brand-short-name } a ziskouezo ar yezh kentañ evel ho yezh dre diouer ha diskouez a raio ar yezhoù all m'eo dleet, en urzh m'int diskouezet.
