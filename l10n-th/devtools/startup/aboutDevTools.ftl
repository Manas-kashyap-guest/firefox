# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

head-title = เกี่ยวกับเครื่องมือนักพัฒนา
enable-inspect-element-title = เปิดใช้งานเครื่องมือนักพัฒนา Firefox เพื่อใช้การตรวจสอบองค์ประกอบ
enable-learn-more-link = เรียนรู้เพิ่มเติมเกี่ยวกับเครื่องมือนักพัฒนา
enable-enable-button = เปิดใช้งานเครื่องมือนักพัฒนา
enable-close-button = ปิดแท็บนี้
newsletter-email-placeholder =
    .placeholder = อีเมล
newsletter-subscribe-button = บอกรับ
newsletter-thanks-title = ขอบคุณ!
footer-learn-more-link = เรียนรู้เพิ่มเติม
features-learn-more = เรียนรู้เพิ่มเติม
features-inspector-title = ตัวตรวจสอบ
features-console-title = คอนโซล
features-debugger-title = ตัวดีบั๊ก
features-network-title = เครือข่าย
features-storage-title = ที่เก็บข้อมูล
features-responsive-title = โหมดการออกแบบเชิงตอบสนอง
features-performance-title = ประสิทธิภาพ
features-memory-title = หน่วยความจำ
