# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = ลองเลยตอนนี้
onboarding-button-label-get-started = เริ่มต้นใช้งาน
onboarding-welcome-header = ยินดีต้อนรับสู่ { -brand-short-name }
onboarding-start-browsing-button-label = เริ่มการท่องเว็บ

## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = การท่องเว็บแบบส่วนตัว
onboarding-private-browsing-text = ท่องเว็บด้วยตัวคุณเอง การท่องเว็บแบบส่วนตัวพร้อมการปิดกั้นเนื้อหาปิดกั้นตัวติดตามออนไลน์ที่ติดตามคุณในเว็บต่าง ๆ
onboarding-screenshots-title = ภาพหน้าจอ
onboarding-screenshots-text = จับ บันทึก และแบ่งปันภาพหน้าจอ - โดยไม่ต้องออกจาก { -brand-short-name } จับภาพเป็นบริเวณหรือทั้งหน้าตามที่คุณท่องเว็บ จากนั้นบันทึกไปยังเว็บเพื่อความง่ายในการเข้าถึงและแบ่งปัน
onboarding-addons-title = ส่วนเสริม
onboarding-addons-text = เพิ่มคุณลักษณะเพิ่มเติมที่ทำให้ { -brand-short-name } ทำงานได้มากขึ้นสำหรับคุณ เปรียบเทียบราคา ตรวจสอบสภาพอากาศ หรือแสดงบุคลิกภาพของคุณด้วยชุดตกแต่งที่กำหนดเอง
onboarding-ghostery-title = Ghostery
