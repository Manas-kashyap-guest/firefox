# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = ต้องเริ่มการทำงานใหม่
restart-required-header = ขออภัย เราจำเป็นต้องทำสิ่งเล็ก ๆ อีกเพียงหนึ่งอย่างเพื่อดำเนินการต่อ
restart-button-label = เริ่มการทำงาน { -brand-short-name } ใหม่
