<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Вярнуцца">
<!ENTITY safeb.palm.seedetails.label "Паказаць падрабязнасці">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Гэта не падманлівы сайт…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "в">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Парады ад <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Наведванне гэтага сайта можа нанесці шкоду вашаму камп'ютару">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; заблакаваў гэту старонку, таму што яна можа спрабаваць усталяваць шкодныя праграмы, якія могуць скрасці або сцерці персанальныя звесткі на вашым камп'ютары.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "Пра <span id='malware_sitename'/> ёсць звесткі, што на ім <a id='error_desc_link'>утрымліваюцца шкодныя праграмы</a>. Вы можаце <a id='report_detection'>паведаміць аб памылцы ў гэтых звестках</a> ці <a id='ignore_warning_link'>ігнараваць рызыку</a> і перайсці на гэты небяспечны сайт.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "Пра <span id='malware_sitename'/> ёсць звесткі, што на ім <a id='error_desc_link'>утрымліваюцца шкодныя праграмы</a>. Вы можаце <a id='report_detection'>паведаміць аб памылковасці гэтых звестак</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Даведайцеся больш аб непажаданым сеціўным змесціве, у тым ліку вірусах і іншых шкодных праграмах, і аб тым, як абараніць свой камп'ютар, на сайце <a id='learn_more_link'>StopBadware.org</a>. Падрабязней пра Абарону ад фішынгу і шкодных праграм у &brandShortName; на пляцоўцы <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Гэты сайт можа ўтрымліваць зламысныя праграмы">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; заблакаваў гэту старонку, таму што яна можа спрабаваць падманам схіліць вас да ўсталявання праграм, якія будуць перашкаджаць вашай працы ў браўзеры (напрыклад, мяняць хатнюю старонку, або паказваць дадатковую рэкламу на сайтах).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "Пра <span id='unwanted_sitename'/> вядома, што на ім <a id='error_desc_link'>утрымліваюцца шкодныя праграмы</a>. Вы можаце <a id='ignore_warning_link'>ігнараваць рызыку</a> і перайсці на гэты небяспечны сайт.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "Пра <span id='unwanted_sitename'/> вядома, што на ім <a id='error_desc_link'>утрымліваюцца шкодныя праграмы</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Даведайцеся больш аб непажаданым сеціўным змесціве на сайце <a id='learn_more_link'>Палітыкі ў адносінах непажаданых праграм</a>. Падрабязней пра Абарону ад фішынгу і шкодных праграм у &brandShortName; на пляцоўцы <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Падроблены сайт наперадзе">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; заблакаваў гэту старонку, таму што яна можа спрабаваць падманам схіліць вас зрабіць што-небудзь небяспечнае, напрыклад, усталяваць праграмы, або раскрыць персанальную інфармацыю, як то паролі і крэдытныя карты.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "Пра <span id='phishing_sitename'/> вядома, што ён <a id='error_desc_link'>падроблены сайт</a>. Вы можаце <a id='report_detection'>паведаміць аб памылковасці гэтых звестак</a> ці <a id='ignore_warning_link'>ігнараваць рызыку</a> і перайсці на гэты небяспечны сайт.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "Пра <span id='phishing_sitename'/> вядома, што ён <a id='error_desc_link'>падроблены сайт</a>. Вы можаце <a id='report_detection'>паведаміць аб памылковасці гэтых звестак</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Даведайцеся больш аб падробленых сайтах і фішынгу на сайце <a id='learn_more_link'>www.antiphishing.org</a>. Падрабязней пра Абарону ад фішынгу і шкодных праграм у &brandShortName; на пляцоўцы <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Гэты сайт можа ўтрымліваць зламысныя праграмы">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; заблакаваў доступ да гэтай старонкі, бо яна можа зрабіць спробу ўсталяваць небяспечныя праграмы, якія выкрадуць або выдаляць вашу інфармацыю (напрыклад, фатаграфіі, паролі, паведамленні і нумары банкаўскіх карт).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "Пра <span id='harmful_sitename'/> вядома, што ён <a id='error_desc_link'>утрымлівае патэнцыяльна шкодную праграму</a>. Вы можаце <a id='ignore_warning_link'>ігнараваць рызыку</a> і перайсці на небяспечны сайт.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "Пра <span id='harmful_sitename'/> вядома, што ён <a id='error_desc_link'>утрымлівае патэнцыяльна шкодную праграму</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Падрабязней пра Абарону ад фішынгу і шкодных праграм у &brandShortName; на пляцоўцы <a id='firefox_support'>support.mozilla.org</a>.">
