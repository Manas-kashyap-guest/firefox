# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Firefox Brand
##
## Firefox must be treated as a brand, and kept in English.
## It cannot be:
## - Declined to adapt to grammatical case.
## - Transliterated.
## - Translated.
##
## Reference: https://www.mozilla.org/styleguide/communications/translation/

-brand-short-name = Firefox
-vendor-short-name = Mozilla
-brand-full-name = Mozilla Firefox
-brand-shorter-name = Firefox
trademarkInfo = Firefox 및 Firefox 로고는 미국 Mozilla Foundation의 소유이며, 미국 및 그 외의 나라에 등록된 상표입니다.
