# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

languages-customize-moveup =
    .label = 위로 이동
    .accesskey = U
languages-customize-movedown =
    .label = 아래로 이동
    .accesskey = D
languages-customize-remove =
    .label = 삭제
    .accesskey = R
languages-customize-select-language =
    .placeholder = 추가할 언어 선택…
languages-customize-add =
    .label = 추가
    .accesskey = A
messenger-languages-window =
    .title = { -brand-short-name } 언어 설정
    .style = width: 40em
messenger-languages-description = { -brand-short-name }가 첫번째 언어를 기본 언어로 표시하고 필요한 경우 순서대로 대체 언어를 표시 합니다.
