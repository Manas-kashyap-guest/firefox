<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Ferbine mei &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Selektearje ‘&syncBrand.shortName.label; ynstelle’ op jo nije apparaat om it te aktivearjen.'>
<!ENTITY sync.subtitle.pair.label 'Selektearje ‘In apparaat keppelje’ op jo oare apparaat om it te aktivearjen.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Ik haw it apparaat net by my…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Oanmeldingen'>
<!ENTITY sync.configure.engines.title.history 'Skiednis'>
<!ENTITY sync.configure.engines.title.tabs 'Ljepblêden'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; op &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Blêdwizermenu'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Labels'>
<!ENTITY bookmarks.folder.toolbar.label 'Blêdwizerarkbalke'>
<!ENTITY bookmarks.folder.other.label 'Oare blêdwizers'>
<!ENTITY bookmarks.folder.desktop.label 'Buro-PC-blêdwizers'>
<!ENTITY bookmarks.folder.mobile.label 'Mobyl-blêdwizers'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fêstmakke'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Tebek nei navigearjen'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Wolkom by &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Meld jo oan om jo ljepblêden, blêdwizers, oanmeldingen en mear te syngronisearjen.'>
<!ENTITY fxaccount_getting_started_get_started 'Begjinne'>
<!ENTITY fxaccount_getting_started_old_firefox 'Brûke jo in âldere ferzje fan &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Accountserver'>
<!ENTITY fxaccount_status_sync_now 'No syngronisearje'>
<!ENTITY fxaccount_status_syncing2 'Syngronisearje…'>
<!ENTITY fxaccount_status_device_name 'Apparaatnamme'>
<!ENTITY fxaccount_status_sync_server 'Syncserver'>
<!ENTITY fxaccount_status_needs_verification2 'Jo account moat ferifiearre wurde. Tik om de ferifikaasje-e-mail opnij te ferstjoeren.'>
<!ENTITY fxaccount_status_needs_credentials 'Kin net ferbine. Tik om jo oan te melden.'>
<!ENTITY fxaccount_status_needs_upgrade 'Jo moatte &brandShortName; fernije om jo oan te melden.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; is ynsteld, mar syngronisearret net automatysk. Skeakel ‘Automatysk syngronisearje’ om yn Android-instellingen &gt; Gegevensgebruik.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; is ynsteld, mar syngronisearret net automatysk. Skeakel ‘Automatysk syngronisearje’ om yn it menu fan Android-instellingen &gt; Accounts.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tik om jo oan te melden by jo nije Firefox-account.'>
<!ENTITY fxaccount_status_choose_what 'Kies wat jo syngronisearje wolle'>
<!ENTITY fxaccount_status_bookmarks 'Blêdwizers'>
<!ENTITY fxaccount_status_history 'Skiednis'>
<!ENTITY fxaccount_status_passwords2 'Oanmeldingen'>
<!ENTITY fxaccount_status_tabs 'Iepen ljepblêden'>
<!ENTITY fxaccount_status_additional_settings 'Ekstra ynstellingen'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Allinnich fia wifi syngronisearje'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Opkeare dat &brandShortName; syngronisearret oer in mobyl of beheind netwurk'>
<!ENTITY fxaccount_status_legal 'Juridysk' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Tsjinstbetingsten (Ing.)'>
<!ENTITY fxaccount_status_linkprivacy2 'Privacyferklearring (Ing.)'>
<!ENTITY fxaccount_remove_account 'Ferbrekke&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Ferbrekke fan Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Jo navigaasjegegevens bliuwe op dizze kompjûter bestean, mar der wurdt net mear mei jo account syngronisearre.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox-account &formatS; ferbrutsen.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Ferbrekke'>

<!ENTITY fxaccount_enable_debug_mode 'Debugmodus ynskeakelje'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label;-opsjes'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; konfigurearje'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; is net ferbûn'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tik om jo oan te melden as &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; fernijen foltôgje?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tik om jo oan te melden as &formatS;'>
