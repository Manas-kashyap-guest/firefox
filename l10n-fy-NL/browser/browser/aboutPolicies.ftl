# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-policies-title = Bedriuwsbelied
# 'Active' is used to describe the policies that are currently active
active-policies-tab = Aktyf
errors-tab = Flaters
documentation-tab = Dokumintaasje
policy-name = Beliedsnamme
policy-value = Beliedswearde
policy-errors = Beliedsflaters
# 'gpo-machine-only' policies are related to the Group Policy features
# on Windows. Please use the same terminology that is used on Windows
# to describe Group Policy.
# These policies can only be set at the computer-level settings, while
# the other policies can also be set at the user-level.
gpo-machine-only =
    .title = By gebrûk fan Groepsbelied kin dizze beliedsregel allinnich op kompjûternivo ynsteld wurde.
