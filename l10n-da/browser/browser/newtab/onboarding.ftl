# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## UI strings for the simplified onboarding modal

onboarding-button-label-try-now = Prøv det
onboarding-button-label-get-started = Kom i gang
onboarding-welcome-header = Velkommen til { -brand-short-name }
onboarding-start-browsing-button-label = Kom i gang

## These strings belong to the individual onboarding messages.

## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section
onboarding-private-browsing-title = Privat browsing
onboarding-private-browsing-text = Beskyt dit privatliv på nettet. Privat browsing med Blokering af indhold blokerer sporings-teknologier, der følger dig rundt på nettet. 

onboarding-screenshots-title = Skærmbilleder
onboarding-screenshots-text = Tag, gem og del skærmbilleder direkte i { -brand-short-name }. Gem en del af siden eller hele siden, mens du browser. Gem så på nettet, så du nemt kan finde og dele dine skærmbilleder.

onboarding-addons-title = Tilføjelser
onboarding-addons-text = Tilføj flere funktioner for at gøre { -brand-short-name } endnu bedre. Sammenlign priser, tjek vejret eller tilpas browserens udseende efter dit humør.

onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = Med tilføjelser som Ghostery slipper du for irriterende reklamer. Samtidig får du en hurtigere, bedre og mere sikker oplevelse på nettet.

# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sync
onboarding-fxa-text = Opret en { -fxaccount-brand-name } og synkroniser dine bogmærker, adgangskoder og åbne faneblade, overalt hvor du bruger { -brand-short-name }.
