# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-search-textbox =
    .placeholder = Cuardaigh suímh
    .accesskey = S
site-data-column-host =
    .label = Suíomh
site-data-column-storage =
    .label = Stóras
site-data-remove-selected =
    .label = Bain na Cinn Roghnaithe
    .accesskey = R
site-data-button-cancel =
    .label = Cealaigh
    .accesskey = C
site-data-button-save =
    .label = Sábháil na hAthruithe
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = Bain Uile
    .accesskey = e
site-data-remove-shown =
    .label = Bain a bhfuil ar taispeáint
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
site-data-removing-dialog =
    .title = { site-data-removing-header }
    .buttonlabelaccept = Bain
