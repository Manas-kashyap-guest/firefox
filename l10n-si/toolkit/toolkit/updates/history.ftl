# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

history-title = යාවත්කාල කිරීම්හී අතීතයන්
close-button-label =
    .buttonlabelcancel = වසන්න
    .title = යාවත්කාල කිරීම්හී අතීතයන්
no-updates-label = තවම යාවත්කාල කිරීම් ස්ථාලනය කර නොමැත
name-header = යාවත්කාල කිරීමේ නම
date-header = ස්ථාපනය කළ දිනය
type-header = වර්ගය
state-header = තත්වය
# Used to display update history
#
# Variables:
#   $name (String): name of the update
#   $buildID (String): build identifier from the local updates.xml
update-full-name =
    .name = { $name } ({ $buildID })
