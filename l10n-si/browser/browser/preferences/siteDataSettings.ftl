# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Settings

site-data-column-host =
    .label = අඩවිය
site-data-column-storage =
    .label = ගබඩා කිරීම
site-data-remove-selected =
    .label = තෝරාගත් එක ඉවත් කරන්න
    .accesskey = r
site-data-button-cancel =
    .label = වලක්වන්න
    .accesskey = C
site-data-button-save =
    .label = වෙනස්කම් සුරකින්න
    .accesskey = a
# Variables:
#   $value (Number) - Value of the unit (for example: 4.6, 500)
#   $unit (String) - Name of the unit (for example: "bytes", "KB")
site-usage-pattern = { $value } { $unit }
site-data-remove-all =
    .label = සියල්ල ඉවත් කරන්න
    .accesskey = e

## Removing

site-data-removing-window =
    .title = { site-data-removing-header }
