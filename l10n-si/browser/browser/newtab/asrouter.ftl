# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

cfr-doorhanger-extension-cancel-button = පසුවට
    .accesskey = N
cfr-doorhanger-extension-ok-button = දැන් එක් කරන්න
    .accesskey = A
cfr-doorhanger-extension-learn-more-link = තවත් දැනගන්න
# This string is used on a new line below the add-on name
# Variables:
#   $name (String) - Add-on author name
cfr-doorhanger-extension-author = නිමැවුම  { $name }

## Add-on statistics
## These strings are used to display the total number of
## users and rating for an add-on. They are shown next to each other.

