# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

permissions-window =
    .title = Esceiciones
    .style = width: 36em
permissions-close-key =
    .key = w
permissions-address = Direición del sitiu web
    .accesskey = D
permissions-block =
    .label = Bloquiar
    .accesskey = B
permissions-session =
    .label = Permitir na sesión
    .accesskey = s
permissions-allow =
    .label = Permitir
    .accesskey = P
permissions-site-name =
    .label = Sitiu web
permissions-status =
    .label = Estáu
permissions-remove =
    .label = Desaniciar sitiu web
    .accesskey = R
permissions-remove-all =
    .label = Desaniciar tolos sitios web
    .accesskey = e
permissions-button-cancel =
    .label = Encaboxar
    .accesskey = E
permissions-button-ok =
    .label = Guardar cambeos
    .accesskey = G
permissions-searchbox =
    .placeholder = Guetar sitiu web
permissions-capabilities-allow =
    .label = Permitir
permissions-capabilities-block =
    .label = Bloquiar
permissions-capabilities-prompt =
    .label = Entrugar siempre
permissions-capabilities-listitem-allow =
    .value = Permitir
permissions-capabilities-listitem-block =
    .value = Bloquiar
permissions-capabilities-listitem-allow-first-party =
    .value = Permitir namái les de primeres partes
permissions-capabilities-listitem-allow-session =
    .value = Permitir pa la sesión

## Invalid Hostname Dialog

permissions-invalid-uri-title = La direición del host nun ye válida
permissions-invalid-uri-label = Por favor, introduz un nome de host válidu

## Exceptions - Tracking Protection

permissions-exceptions-tracking-protection-window =
    .title = Esceiciones - Proteición de rastrexu
    .style = { permissions-window.style }
permissions-exceptions-tracking-protection-desc = Deshabilitasti la proteición de rastrexu nestes webs.

## Exceptions - Cookies


## Exceptions - Pop-ups

permissions-exceptions-popup-window =
    .title = Sitios web permitíos - Ventanos emerxentes
    .style = { permissions-window.style }
permissions-exceptions-popup-desc = Pues especificar qué sitios web puen abrir ventanes emerxentes. Escribi la direición exauta del sitiu que quies permitir y calca Permitir.

## Exceptions - Saved Logins

permissions-exceptions-saved-logins-window =
    .title = Esceiciones - Ingresos guardaos
    .style = { permissions-window.style }
permissions-exceptions-saved-logins-desc = Nun van guardase los anicios de sesión pa los sitios web de darréu

## Exceptions - Add-ons

permissions-exceptions-addons-desc = Pues especificar dende qué sitios web ta permitío instalar complementos. Escribi la direición exauta del sitiu que quies permitir y calca Permitir.

## Exceptions - Autoplay Media


## Site Permissions - Notifications


## Site Permissions - Location


## Site Permissions - Camera


## Site Permissions - Microphone

permissions-site-microphone-desc = Los sitios web de darréu solicitaron l'accesu al micrófonu. Pues especificar los sitios web que tienen permisu p'acceder a elli. Tamién pues bloquiar les solicitúes nueves que pidan l'accesu al micrófonu.
