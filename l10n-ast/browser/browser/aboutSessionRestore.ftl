# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restore-page-tab-title = Restaurar Sesión
# When tabs are distributed across multiple windows, this message is used as a
# header above the group of tabs for each window.
#
# Variables:
#    $windowNumber: Progressive number associated to each window
restore-page-window-label = Ventana { $windowNumber }
restore-page-restore-header =
    .label = Restaurar
restore-page-list-header =
    .label = Ventanes y llingüetes
restore-page-try-again-button =
    .label = Restaurar Sesión
    .accesskey = R
restore-page-close-button =
    .label = Aniciar sesión nueva
    .accesskey = N

## The following strings are used in about:welcomeback

welcome-back-tab-title = ¡Ésitu!
welcome-back-page-title = ¡Ésitu!
welcome-back-page-info = { -brand-short-name } ta preparáu pa siguir.
welcome-back-restore-button =
    .label = ¡Vamos!
    .accesskey = g
welcome-back-restore-all-label = Restaurar toles ventanes y llingüetes
welcome-back-restore-some-label = Restaurar namái les que quies
welcome-back-page-info-link = Desaniciáronse los complementos y personalizaciones amás de reafitar los axustes de restolar. Si esto nun igua'l to problema, <a data-l10n-name="link-more">deprendi más tocante a lo que pues facer.</a>
