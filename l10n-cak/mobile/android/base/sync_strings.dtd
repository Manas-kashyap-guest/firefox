<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Ximojri\&apos;ïl">
<!ENTITY syncBrand.shortName.label "Ximïl">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Tokisäx pa &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Richin natzïj ri k\&apos;ak\&apos;a\&apos; awokisaxel, tacha\&apos; “Rub\&apos;anik runuk\&apos;ulem &syncBrand.shortName.label;” pa ri okisaxel.'>
<!ENTITY sync.subtitle.pair.label 'Richin natzïj. tacha\&apos; “Tixim jun okisaxel” pan ri jun chik awokisaxel.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Man k\&apos;o ta ri woyonib\&apos;äl wik\&apos;in…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Kitikirisaxik taq molojri\&apos;ïl'>
<!ENTITY sync.configure.engines.title.history 'Natab\&apos;äl'>
<!ENTITY sync.configure.engines.title.tabs 'Taq ruwi\&apos;'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; pa ruwi\&apos; &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'K\&apos;utüy kisamaj taq yaketal'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Ruwachib\&apos;al b\&apos;i\&apos;aj'>
<!ENTITY bookmarks.folder.toolbar.label 'Kimolsamajib\&apos;al taq yaketal'>
<!ENTITY bookmarks.folder.other.label 'Ch\&apos;aqa\&apos; chik taq Yaketal'>
<!ENTITY bookmarks.folder.desktop.label 'Kematz\&apos;ib\&apos;ab\&apos;äl taq Yaketal'>
<!ENTITY bookmarks.folder.mobile.label 'Taq ruyaketal oyonib\&apos;äl'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Jikïl'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Tok chik pa k\&apos;amaya\&apos;l'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Ütz apetik pa &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Tatikirisaj molojri\&apos;ïl richin ye\&apos;axïm taq ruwi\&apos;, taq yaketal, ewan taq tzij &amp; ch\&apos;aqa\&apos; chik.'>
<!ENTITY fxaccount_getting_started_get_started 'Nab\&apos;ey taq xak'>
<!ENTITY fxaccount_getting_started_old_firefox '¿La nawokisaj jun ojer ruwäch chi &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Retal rutaqoya\&apos;l ri ruk\&apos;u\&apos;x samaj'>
<!ENTITY fxaccount_status_sync_now 'Tixim wakami'>
<!ENTITY fxaccount_status_syncing2 'Tajin niximon…'>
<!ENTITY fxaccount_status_device_name 'Rub\&apos;i\&apos; ri okisaxel'>
<!ENTITY fxaccount_status_sync_server 'Ruk\&apos;u\&apos;x rusamaj Ximïl'>
<!ENTITY fxaccount_status_needs_verification2 'Rajowaxik nijikib\&apos;äx ri retal ataqoya\&apos;l. Tanaqa\&apos; qa richin nitaq chik ri rutzijol jikib\&apos;axïk.'>
<!ENTITY fxaccount_status_needs_credentials 'Man tikirel ta nok. Tanaqa\&apos; qa richin natikirisaj qa samaj.'>
<!ENTITY fxaccount_status_needs_upgrade 'Rajowaxik nik\&apos;ex ruwäch &brandShortName; richin nitikirisäx qa samaj.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; b\&apos;anon runuk\&apos;ulem, xa xe chi man nuxïm ta ri\&apos; pa ruyonil. Tacha\&apos; “kexim taq tzij pa ruyonil” pa ri runuk\&apos;ulem Android &gt; Kokisaxik taq tzij.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; b\&apos;anon runuk\&apos;ulem, xa xe chi man nuxïm ta ri\&apos; pa ruyonil. Tacha\&apos; “Kexim taq tzij pa ruyonil” pa ri runuk\&apos;ulem Android &gt; Kokisaxik taq tzij.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tapitz\&apos;a\&apos; richin yatok chupam ri k\&apos;ak\&apos;a\&apos; rub\&apos;i\&apos; ataqoya\&apos;l richin Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Tacha\&apos; achike naxïm'>
<!ENTITY fxaccount_status_bookmarks 'Yaketal'>
<!ENTITY fxaccount_status_history 'Natab\&apos;äl'>
<!ENTITY fxaccount_status_passwords2 'Kitikirisaxik taq molojri\&apos;ïl'>
<!ENTITY fxaccount_status_tabs 'Jaqon taq ruwi\&apos;'>
<!ENTITY fxaccount_status_additional_settings 'Kitz\&apos;aqat taq Nuk\&apos;ulem'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sync xa xe pa Mek\&apos;am'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Tiq\&apos;at &brandShortName; ri ximojri\&apos;ïl pan oyonib\&apos;äl o etan k\&apos;amab\&apos;ey'>
<!ENTITY fxaccount_status_legal 'Taqanel tzijol' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Retal rutzijol re samaj'>
<!ENTITY fxaccount_status_linkprivacy2 'Ichinan na\&apos;oj'>
<!ENTITY fxaccount_remove_account 'Tichup&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 '¿La nichup richin ri Ximojri\&apos;ïl?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Ri taq atzij richin okem pa k\&apos;amaya\&apos;l junelïk xkek\&apos;oje\&apos; pa re okisaxel re\&apos;, xa xe chi man xkexim ta chik rik\&apos;in ri rub\&apos;i\&apos; ataqoya\&apos;l.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Yujun rub\&apos;i\&apos; taqoya\&apos;l richin Firefox &formatS; chupun.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Tichup'>

<!ENTITY fxaccount_enable_debug_mode 'Titzïj ri chojmirisaxik rub\&apos;eyal'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; taq rucha\&apos;oj'>
<!ENTITY fxaccount_options_configure_title 'Tab\&apos;ana\&apos; runuk\&apos;ulem &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; man okinäq ta'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tapitz\&apos;a\&apos; richin natikirisaj molojri\&apos;ïl achi\&apos;el &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '¿Tik\&apos;isib\&apos;ëx ri k\&apos;exoj ruwäch&syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tapitz\&apos;a\&apos; richin natikirisaj molojri\&apos;ïl achi\&apos;el &formatS;'>
