<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY restorepage.tabtitle       "Restaurar sesión previa">

<!-- LOCALIZATION NOTE: The title is intended to be apologetic and disarming, expressing dismay
     and regret that we are unable to restore the session for the user -->
<!ENTITY restorepage.errorTitle2    "Disculpe. Tenemos dificultades para recuperar sus páginas.">
<!ENTITY restorepage.problemDesc2   "Hay algunos problemas intentando restaurar la última sesión. Elegí Restaurar sesión para volver a intentarlo.">
<!ENTITY restorepage.tryThis2       "¿Todavía no pudo restaurar la sesión? A veces una pestaña puede causar el problema. Mire las pestañas previas, quite de la lista las pestañas que no necesite, y vuelva a intentarlo.">

<!ENTITY restorepage.hideTabs       "Ocultar pestañas anteriores">
<!ENTITY restorepage.showTabs       "Mostrar pestaña anteriores">

<!ENTITY restorepage.tryagainButton2 "Restaurar sesión">
<!ENTITY restorepage.restore.access2 "R">
<!ENTITY restorepage.closeButton2    "Empezar una sesión nueva">
<!ENTITY restorepage.close.access2   "N">

<!ENTITY restorepage.restoreHeader  "Restaurar">
<!ENTITY restorepage.listHeader     "Ventanas y pestañas">
<!-- LOCALIZATION NOTE: &#37;S will be replaced with a number. -->
<!ENTITY restorepage.windowLabel    "Ventana &#37;S">


<!-- LOCALIZATION NOTE: The following 'welcomeback2' strings are for about:welcomeback,
     not for about:sessionstore -->

<!ENTITY welcomeback2.restoreButton  "¡Vamos!">
<!ENTITY welcomeback2.restoreButton.access "V">

<!ENTITY welcomeback2.tabtitle      "¡Éxito!">

<!ENTITY welcomeback2.pageTitle     "¡Éxito!">
<!ENTITY welcomeback2.pageInfo1     "&brandShortName; está listo.">

<!ENTITY welcomeback2.restoreAll.label  "Restaurar todas las ventanas &amp; pestañas">
<!ENTITY welcomeback2.restoreSome.label "Restaurar solo las que quiere">


<!-- LOCALIZATION NOTE (welcomeback2.beforelink.pageInfo2,
welcomeback2.afterlink.pageInfo2): these two string are used respectively
before and after the the "learn more" link (welcomeback2.link.pageInfo2).
Localizers can use one of them, or both, to better adapt this sentence to
their language.
-->
<!ENTITY welcomeback2.beforelink.pageInfo2  "Los complementos y personalizaciones han sido eliminados y la configuración del navegador ha sido restaurada a sus valores predeterminados. Si esto no corrige el problema, ">
<!ENTITY welcomeback2.afterlink.pageInfo2   "">

<!ENTITY welcomeback2.link.pageInfo2        "conozca más acerca de lo que puede hacer.">

