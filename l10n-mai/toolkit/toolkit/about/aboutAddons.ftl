# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = सहयुक्ति प्रबंधक
search-header-shortcut =
    .key = f
loading-label =
    .value = लोड कए रहल अछि…
list-empty-installed =
    .value = अहाँक एहि प्रकारक कोनो सहयुक्ति संस्थापित नहि अछि
list-empty-available-updates =
    .value = कोनो अद्यतन नहि भेटल
list-empty-recent-updates =
    .value = अहाँ हाल मे कोनो सहयुक्ति अद्यतन नहि कएने छी
list-empty-find-updates =
    .label = अद्यतन क लेल जाँचू
list-empty-button =
    .label = सहयुक्तिक संबंधमे  बेसी जानू
install-addon-from-file =
    .label = फाइल सँ सहयुक्ति संस्थापित करू …
    .accesskey = I
tools-menu =
    .tooltiptext = सभटा सहयुक्ति क लेल अओजार
show-unsigned-extensions-button =
    .label = किछु विस्तार जाँचल नहि जाए सकल
show-all-extensions-button =
    .label = सभ विस्तार देखाबू
cmd-show-details =
    .label = बेसी  सूचना देखाउ
    .accesskey = S
cmd-find-updates =
    .label = अद्यतन ताकू
    .accesskey = F
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] विकल्प
           *[other] वरीयता
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
cmd-enable-theme =
    .label = प्रसंग पहनें
    .accesskey = W
cmd-disable-theme =
    .label = प्रसंग पहिरनाइ बन्न करू
    .accesskey = W
cmd-install-addon =
    .label = संस्थापित  करू
    .accesskey = I
cmd-contribute =
    .label = योगदान करू
    .accesskey = C
    .tooltiptext = ई सहयुक्ति क विकासमे अपन योगदान दिअ
discover-title = सहयुक्ति क अछि?
discover-description = सहयुक्ति एहन अनुप्रयोग अछि जे अहाँकेँ { -brand-short-name } केँ  अतिरिक्त प्रकार्यात्मकता अथवा शैली सँ व्यक्तिगत रूचि क अनुसार ढालबमे मदति करैत अछि. समय बचाबैबला कतकापट्टी, मौसम सूचक अथवा एकटा एहिन प्रसंग जे अहाँक  अपन { -brand-short-name } बनाबैबला प्रसंग आजमाउ.
discover-footer = जखन अहाँ इंटरनेट सँ कनेक्टेड होइछ, ई पट्टी अहाँक लेल सबसँ नीक आओर लोकप्रिय सहयुक्ति प्रस्तुत करैत अछि जकरा अहाँ आजमाए सकू.
detail-version =
    .label = संस्करण
detail-last-updated =
    .label = अंतिम अद्यतन
detail-contributions-description = ई सहयुक्तिक डेवलेपर ने आग्रह कएल अछि जे अहाँ अपन छोट योगदान सँ एकर लगातार विकास मे अपन समर्थन दिअ.
detail-update-type =
    .value = स्वचालित अद्यतन
detail-update-default =
    .label = पूर्वनिर्धारित
    .tooltiptext = स्वतः अद्यतन संस्थापित करैत अछि जँ ओ पूर्वनिर्धारित अछि
detail-update-automatic =
    .label = चालू
    .tooltiptext = स्वतः अद्यतन संस्थापित करैत अछि
detail-update-manual =
    .label = बन्न
    .tooltiptext = स्वतः अद्यतन केँ संस्थापित मत  करू
detail-home =
    .label = होमपेज
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = सहयुक्ति प्रोफाइल
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = अद्यतन क लेल जाँचू
    .accesskey = f
    .tooltiptext = ई सहयुक्ति क लेल अद्यतन क लेल जाँच  करू
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] विकल्प
           *[other] वरीयता
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] ई सहयुक्तिक विकल्पकेँ बदलू
           *[other] ई सहयुक्तिक वरीयता बदलू
        }
detail-rating =
    .value = दर
addon-restart-now =
    .label = आब फेर आरंभ करू
disabled-unsigned-heading =
    .value = किछु सहयुक्ति निष्क्रिय कएल गेल अछि
disabled-unsigned-description = { -brand-short-name } निम्नलिखित एड-ऑन उपयोग मे जाँचल नहि गेल अछि. अहाँ जाँचि सकैत छी<label data-l10n-name="find-addons">प्रतिस्थापन पाबू</label>अथवा डेवलपर सँ कहियो कि ओकरा जाँचि लिअ.
disabled-unsigned-learn-more = अहाँ अपना केँ ऑनलाइन सुरक्षित राखब लेल हमर आओर अनेक प्रयासक बारे मे जानकारी पाउ.
disabled-unsigned-devinfo = डेवलपर रुचि देखाबत हुए अपन एड-ऑन केँ जाँचनाय जारी राखत पढ़ि कए<label data-l10n-name="learn-more">मैनुअल</label>.
extensions-view-discover =
    .name = सहयुक्ति पाउ
    .tooltiptext = { extensions-view-discover.name }
extensions-view-recent-updates =
    .name = हालिया अद्यतन
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = उपलब्ध अद्यतन
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = सभटा सहयुक्ति सुरक्षित विधि क द्वारा निष्क्रिय कएल गेल अछि.
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = सहयुक्ति सुसंगतता जाँच निष्क्रिय अछि. अहाँक पास असंगत सहयुक्ति भए सकैत अछि.
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = सक्रिय  करू
    .tooltiptext = सहयुक्ति सुसंगतता जाँच सक्रिय  करू
extensions-warning-update-security-label =
    .value = सहयुक्ति अद्यतन सुरक्षा जाँच निष्क्रिय अछि. अहाँक सुरक्षा अद्यतनक द्वारा संदिग्ध भए सकैत अछि.
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = सक्रिय  करू
    .tooltiptext = सहयुक्ति अद्यतन सुरक्षा जाँच सक्रिय करू

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = अद्यतनकएल जाएँच  करू
    .accesskey = C
extensions-updates-view-updates =
    .label = हालिया अद्यतन देखू
    .accesskey = V

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = सहयुक्ति स्वतः अद्यतन  करू
    .accesskey = A

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = अद्यतन स्वतः करबाक लेल सबहि सहयुक्ति फेर सेट  करू
    .accesskey = R
extensions-updates-reset-updates-to-manual =
    .label = दस्ती रूप सँ अद्यतन करब क लेल सबहि सहयुक्ति फेर सेट  करू
    .accesskey = R

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = सहयुक्ति अद्यतन कए रहल अछि
extensions-updates-installed =
    .value = अहाँक सहयुक्ति अद्यतन कएल जाए चुकल अछि.
extensions-updates-downloaded =
    .value = अहाँक सहयुक्ति अद्यतन डाउनलोड कएल जाए रही अछि.
extensions-updates-restart =
    .label = संस्थापन पूरा करब क लेल आब फेर आरंभ  करू
extensions-updates-none-found =
    .value = कोनो अद्यतन नहि भेटल
extensions-updates-manual-updates-found =
    .label = उपलब्ध अद्यतन देखू
extensions-updates-update-selected =
    .label = अद्यतन संस्थापित  करू
    .tooltiptext = ई सूची मे उपलब्ध अद्यतन संस्थापित  करू
