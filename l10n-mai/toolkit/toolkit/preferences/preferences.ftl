# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

password-not-set =
    .value = (सेट नहि)
failed-pw-change = मुख्य गुड़किल्ली केँ बदलबा मे असमर्थ.
incorrect-pw = अहाँ सही मुख्य गुड़किल्ली नहि डालल अछि . फेर कोशिश करू .
pw-change-ok = मुख्य गुड़किल्ली सफलतापूर्वक बदलल गेल.
pw-empty-warning = अहाँक भंडारित इमेल गुड़किल्ली, आंकड़ा आओर निज कुँजी सँ सुरक्षित नहि हाएत.
pw-erased-ok = अहाँ मुख्य गुड़किल्ली मेटाय देने अछि .  { pw-empty-warning }
pw-not-wanted = चेतावनी! अहाँ मुख्य गुड़किल्ली नहि प्रयोग करबाक लेल निश्चित कएने छी. { pw-empty-warning }
pw-change2empty-in-fips-mode = अहाँ अखन FIPS मोड मे  अछि. FIPS क लेल  एकटा गैर रिक्त मुख्य गुड़किल्लीक जरूरत अछि.
pw-change-success-title = गुड़किल्ली बदलनाइ संपन्न भेल
pw-change-failed-title = गुड़किल्ली बदलनाइ विफल
pw-remove-button =
    .label = हटाउ
set-password =
    .title = मुख्य गुड़किल्ली बदलू
set-password-old-password = मोजुदा  गुड़किल्ली:
set-password-new-password = नव गुड़किल्ली डालू:
set-password-reenter-password = गुड़किल्ली फेर डालू:
set-password-meter = गुड़किल्ली गुणता मीटर
set-password-meter-loading = लोडिंग
master-password-description = एकटा मुख्य गुड़किल्ली साइट गुड़किल्ली क तरह क संवेदनशील सूचनाक रक्षाक लेल प्रयोग कएल जाएत अछि . जँ अहाँ एकटा मुख्य गुड़किल्ली बनाबैछी अहाँकेँ प्रति सत्र एकटा डालब केँ कहल जएताह जखन { -brand-short-name } प्राप्त करैत अछि  सहेजल सूचना गुड़किल्लीसँ सुरक्षित.
master-password-warning = निश्चित करू जे अहाँकेँ अहाँक द्वारा सेट मुख्य गुड़किल्ली याद अछि.  जँ अहाँ बिसरैछी मुख्य गुड़किल्ली, अहाँ एकरा द्वारा सुरक्षित कोनो सूचना अभिगम मे असमर्थ हाएब.
remove-password =
    .title = मुख्य गुड़किल्ली हटाउ
remove-info =
    .value = अहाँकेँ जरूर अपन मोजुदा गुड़किल्ली डालनाइ चाही:
remove-warning1 = अहाँक मुख्य गुड़किल्ली केँ संवेदनशील सूचनाक सुरक्षा कलेल  प्रयोग कएल जाइत अछि  साइट गुड़किल्लीक तरह.
remove-warning2 = जँ अहाँ अपन मुख्य गुड़किल्ली हटाबैछी अहाँक सूचना सुरक्षित नहि रहत जँ अहाँक कंप्यूटर क सँग छेडछाड़ कएल जाएत अछि .
remove-password-old-password =
    .value = मोजुदा गुड़किल्ली:
